/**
=========================================================
* Material Dashboard 2 React - v2.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

// @mui material components
import Grid from "@mui/material/Grid";
// import Divider from "@mui/material/Divider";

// @mui icons


// Material Dashboard 2 React components
import MDBox from "components/MDBox";
// import MDTypography from "components/MDTypography";

// Material Dashboard 2 React example components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import Footer from "examples/Footer";

import CardDetail from "examples/Cards/StatisticsCards/CardDetail";
import Datareport from "examples/Cards/StatisticsCards/Datareport";

// Overview page components
import Dtail from "layouts/rating/Detail";
import doctor1 from "assets/images/doctor1.jpg";
import user from "assets/images/user.jpg";
import doctor5 from "assets/images/doctor5.jpg";


function Overview() {
  return (
    <DashboardLayout>
      <DashboardNavbar absolute isMini />
      <MDBox mb={2} />
      <Dtail>
        &nbsp;
        <MDBox p={2}>
          <Grid container spacing={6}>
            <Grid item lg={4}>
              <MDBox mb={1.5}>
                <CardDetail
                  image={doctor1}
                  color="dark"
                  name="พญ. สิริน"
                  title="โรคหู คอ จมูก"
                  university="แพทย์ มข."
                  description="โรงพยาบาลขอนแก่น"
                  hospital="โรงพยาบาลขอนแก่น"
                  price="฿200"
                  star="5.0"
                  heart="5.0"
                />
              </MDBox>
            </Grid>
            <Grid item lg={8}>
              <MDBox mb={1.5}>
                <Datareport
                  image={doctor5}
                  color="dark"
                  name="ต่อ"
                  title="โรคหู คอ จมูก"
                  comment="ให้คำแนะนำดีมากครับ"
                  date="18/02/2565"
                />
              </MDBox>
              <MDBox mb={1.5}>
                <Datareport
                  image={user}
                  color="dark"
                  name="แก้ว"
                  title="โรคหู คอ จมูก"
                  comment="ให้คำแนะนำดีมากครับ"
                  date="18/02/2565"
                />
              </MDBox>
              <MDBox mb={1.5}>
                <Datareport
                  image={user}
                  color="dark"
                  name="ขิง"
                  title="โรคหู คอ จมูก"
                  comment="ให้คำแนะนำดีมากครับ"
                  date="18/02/2565"
                />
              </MDBox>
            </Grid>
          </Grid>
        </MDBox>
      </Dtail>
      <Footer />
    </DashboardLayout>
  );
}

export default Overview;
