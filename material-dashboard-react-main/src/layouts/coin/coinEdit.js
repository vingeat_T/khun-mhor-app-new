import React from "react";
import MDInput from "components/MDInput";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
// import MDAvatar from "components/MDAvatar";
import MDButton from "components/MDButton";
import { useNavigate,useSearchParams } from "react-router-dom";
// import team2 from "assets/images/team-2.jpg";
import blank from "assets/images/blankpicture.png";
import { Image } from "semantic-ui-react";
import coin1 from "assets/images/coin1.png";
import Footer from 'examples/Footer'


function EditData() {
  const [searchParams] = useSearchParams();
  const id = searchParams.get("id")
  console.log(id);
  let user = {
    name: "John Smith ",
    email: "john@smith.com",
    select: "null",
    coin: "10000",
    userName: "Vingeat",
    tel: "0895477651",
    image: coin1
  }
  if (id) {
      user = {
        name: null,
        email: null,
        select: null,
        coin: "0",
        userName: null,
        tel: null,
        image: blank

      }
  } 

  const navigate = useNavigate();
  const routeChange = () => {
    const path = "/coin";
    navigate(path);
  };

  return (
    <DashboardLayout>
      <DashboardNavbar />
      <MDBox pb={3}>
        <Grid container spacing={6}>
        <Grid item sm={3} xs={12} />
          <Grid item sm={6} xs={12} mt={3} >
            <Card>
              <MDBox
                mx={0}
                mt={0}
                py={2}
                px={2}
                variant="contained"
                bgColor="light"
                coloredShadow="info"
              >
                <MDTypography variant="h5" color="back">
                  จัดการเหรียญ
                </MDTypography>
              </MDBox>
              <Grid container spacing={-6}>
                <Grid item xs mt={-4}>
                <MDBox style={{ marginLeft: "25%",marginTop: "30%" }}>  
                    <Image src={user.image} alt="profile-image" width="120px" height="120px" />
                </MDBox>
                </Grid>
                <Grid item xs={6} mt={-4}>
                <MDBox pt={10} >
                    <MDInput type="text" label="จำนวนเหรียญ" value={user.name} style={{ width: "99%" }} />
                </MDBox>
                <MDBox pt={3} >
                    <MDInput type="text" label="จำนวนเงิน" value={user.name} style={{ width: "99%" }} />
                </MDBox>
                </Grid>
              </Grid>
              <Grid container my={1} py={1}>
                <Grid item sm={12} xs={12} align="center">
                    <MDButton variant="gradient" color="secondary" onClick={routeChange}>
                    กลับ
                    </MDButton>
                    <MDButton
                    variant="gradient"
                    color="primary"
                    style={{ marginLeft: "2%" }}
                    onClick={routeChange}
                    >
                    บันทึก
                    </MDButton>
                </Grid>
              </Grid>
            </Card>
          </Grid>
        </Grid>
      </MDBox>
      <Footer bottom={0}/>
    </DashboardLayout>
  );
}
export default EditData;
