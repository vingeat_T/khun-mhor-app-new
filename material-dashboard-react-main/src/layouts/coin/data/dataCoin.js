/* eslint-disable react/prop-types */

// Soft UI Dashboard React components
import { useState } from "react";
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
// import MDAvatar from "components/MDAvatar";
// import MDBadge from "components/MDBadge";
import Grid from "@mui/material/Grid";
import MDButton from "components/MDButton";
import Icon from "@mui/material/Icon";
import * as React from "react";
import MDSnackbar from "components/MDSnackbar";
// import Box from "@mui/material/Box";
// import Typography from "@mui/material/Typography";
// import MDInput from "components/MDInput";
// import Card from "@mui/material/Card";
import { useNavigate } from "react-router-dom";
// Images
// import logoXD from "assets/images/small-logos/logo-xd.svg";
// import logoAtlassian from "assets/images/small-logos/logo-atlassian.svg";
// import logoSlack from "assets/images/small-logos/logo-slack.svg";
// import logoSpotify from "assets/images/small-logos/logo-spotify.svg";
// import logoJira from "assets/images/small-logos/logo-jira.svg";
// import logoInvesion from "assets/images/small-logos/logo-invision.svg";
import coin1 from "assets/images/coin1.png";
import coin2 from "assets/images/coin2.png";
import coin3 from "assets/images/coin3.png";
import coin4 from "assets/images/coin4.png";
import coin5 from "assets/images/coin5.png";

import { Image } from "semantic-ui-react";


export default function data() {
//   const Author = ({ image, name, email }) => (
//     <MDBox display="flex" alignItems="center" lineHeight={1}>
//       <MDAvatar src={image} name={name} size="sm" />
//       <MDBox ml={2} lineHeight={1}>
//         <MDTypography display="block" variant="button" fontWeight="medium">
//           {name}
//         </MDTypography>
//         <MDTypography variant="caption">{email}</MDTypography>
//       </MDBox>
//     </MDBox>
//   );

  const [successSB, setSuccessSB] = useState(false);
  const [deleteSB, setDeleteSB] = useState(false);

  // const openSuccessSB = () => setSuccessSB(true);
  const openDelete = () => setDeleteSB(true);
  const closeSuccessSB = () => setSuccessSB(false);
  const closeDeleteSB = () => setDeleteSB(false);

//   const [open, setOpen] = React.useState(false);
//   const handleOpen = () => setOpen(true);
//   const handleClose = () => setOpen(false);

  const renderSuccessSB = (
    <MDSnackbar
      color="warning"
      icon="check"
      title="Console Dashboard"
      content="This is a data demo can't modify"
      dateTime="11 mins ago"
      open={successSB}
      onClose={closeSuccessSB}
      close={closeSuccessSB}
      bgWhite
    />
  );

//   const style = {
//     position: "absolute",
//     top: "50%",
//     left: "50%",
//     transform: "translate(-50%, -50%)",
//     width: "50%",
//     bgcolor: "#f0f2f5",
//     border: "2px solid #000",
//     boxShadow: 24,
//     p: 4,
//   };

  // const style2 = {
  //   position: "absolute",
  //   top: "50%",
  //   left: "50%",
  //   transform: "translate(-50%, -50%)",
  //   width: "50%",
  //   bgcolor: "background.paper",
  //   border: "2px solid #000",
  //   boxShadow: 24,
  //   p: 4,
  // };

  const navigate = useNavigate();
  const routeChange = () => {
    const path = "/editcoin";
    navigate(path);
  };
  const sizeW = "100px"
  const sizeG = 12
  const sizeButtop = 5

  const renderDelete = (
    <MDSnackbar
      color="error"
      icon="check"
      title="Console Dashboard"
      content="This is a data demo can't delete"
      dateTime="11 mins ago"
      open={deleteSB}
      onClose={closeDeleteSB}
      close={closeDeleteSB}
      bgWhite
    />
  );

  const dataColumns = [
    { Header: "รูป", accessor: "author", width: "25%", align: "center"},
    { Header: "จำนวนเหรียญ", accessor: "status", align: "center" },
    { Header: "ราคาเหรียญ", accessor: "skill", align: "center" },
    { Header: "action", accessor: "action", align: "center" },
  ]

  const dataRows = [
    { author: coin1, status: "60 เหรียญ", skill: "35 บาท"},
    { author: coin2, status: "300 เหรียญ", skill: "179 บาท"},
    { author: coin3, status: "990 เหรียญ", skill: "549 บาท"},
    { author: coin4, status: "1980 เหรียญ", skill: "1100 บาท"},
    { author: coin5, status: "3280 เหรียญ", skill: "1800 บาท"}
  ]

  const setRowArray = []
  if (dataRows) {
    dataRows.forEach((val) => {
      const renderRow = {
        author: ( 
          <Grid container >
              <Grid item sm={sizeG} xs={sizeG} align="center">
                  <Image src={val.author} alt="profile-image" width={sizeW} height="100%" />
              </Grid>
          </Grid>
        ),
        status: (
          <MDTypography component="a" href="#" variant="h3" color="warning" fontWeight="medium">
             {val.status}
          </MDTypography>
        ),
        skill: (
          <MDTypography component="a" href="#" variant="h3" color="text" fontWeight="medium">
            {val.skill}
          </MDTypography>
        ),
        action: (
          <MDBox ml={2} pt={0}>
            <Grid container spacing={2}>
            <Grid item sm={sizeButtop} xs={sizeButtop}>
              <MDButton variant="gradient" color="warning" onClick={routeChange}>
                <Icon>edit</Icon>
                แก้ไข
              </MDButton>
            </Grid>
            <Grid item sm={sizeButtop} xs={sizeButtop}>
              <MDButton style={{marginLeft: "1%" }} variant="gradient" color="error" onClick={openDelete}>
                <Icon>delete</Icon>
                ลบ
              </MDButton>
              {renderSuccessSB}
              {renderDelete}
            </Grid>
            </Grid>
          </MDBox>
        ),
      }
      setRowArray.push(renderRow)
    })
  }

//   const Job = ({ title, description }) => (
//     <MDBox lineHeight={1} textAlign="left">
//       <MDTypography display="block" variant="caption" color="text" fontWeight="medium">
//         {title}
//       </MDTypography>
//       <MDTypography variant="caption">{description}</MDTypography>
//     </MDBox>
//   );

  return {

    columns: dataColumns,

    rows: setRowArray

    // columns: [
    //   { Header: "รูป", accessor: "author", width: "25%", align: "center"},
    //   { Header: "จำนวนเหรียญ", accessor: "status", align: "center" },
    //   { Header: "ราคาเหรียญ", accessor: "skill", align: "center" },
    //   { Header: "action", accessor: "action", align: "center" },
      
    // ],

    // rows: [
    //   {
    //     author:( 
    //         <Grid container >
    //             <Grid item sm={sizeG} xs={sizeG} align="center">
    //                 <Image src={coin1} alt="profile-image" width={sizeW} height="100%" />
    //             </Grid>
    //         </Grid>),
    //     status: (
    //       <MDTypography component="a" href="#" variant="h3" color="warning" fontWeight="medium">
    //          60 เหรียญ
    //       </MDTypography>
    //     ),
    //     skill: (
    //       <MDTypography component="a" href="#" variant="h3" color="text" fontWeight="medium">
    //         35 บาท
    //       </MDTypography>
    //     ),
    //     action: (
    //         <MDBox ml={-1}>
    //           <Grid container spacing={2}>
    //           <Grid item sm={sizeButtop} xs={sizeButtop}>
    //             <MDButton variant="gradient" color="warning" onClick={routeChange}>
    //               <Icon>edit</Icon>
    //               แก้ไข
    //             </MDButton>
    //           </Grid>
    //           <Grid item sm={sizeButtop} xs={sizeButtop}>
    //             <MDButton style={{marginLeft: "1%" }} variant="gradient" color="error" onClick={openDelete}>
    //               <Icon>delete</Icon>
    //               ลบ
    //             </MDButton>
    //             {renderSuccessSB}
    //             {renderDelete}
    //           </Grid>
    //           </Grid>
    //         </MDBox>
    //       ),
    //   },
    //   {
    //     author:( 
    //         <Grid container >
    //             <Grid item sm={sizeG} xs={sizeG} align="center">
    //                 <Image src={coin2} alt="profile-image" width={sizeW} height="100%" />
    //             </Grid>
    //         </Grid>),
    //     status: (
    //       <MDTypography component="a" href="#" variant="h3" color="warning" fontWeight="medium">
    //         300  เหรียญ
    //       </MDTypography>
    //     ),
    //     skill: (
    //       <MDTypography component="a" href="#" variant="h3" color="text" fontWeight="medium">
    //         179 บาท
    //       </MDTypography>
    //     ),
    //     action: (
    //         <MDBox ml={-1}>
    //           <Grid container spacing={2}>
    //           <Grid item sm={sizeButtop} xs={sizeButtop}>
    //             <MDButton variant="gradient" color="warning" onClick={routeChange}>
    //               <Icon>edit</Icon>
    //               แก้ไข
    //             </MDButton>
    //           </Grid>
    //           <Grid item sm={sizeButtop} xs={sizeButtop}>
    //             <MDButton style={{marginLeft: "1%" }} variant="gradient" color="error" onClick={openDelete}>
    //               <Icon>delete</Icon>
    //               ลบ
    //             </MDButton>
    //             {renderSuccessSB}
    //             {renderDelete}
    //           </Grid>
    //           </Grid>
    //         </MDBox>
    //       ),
    //   },
    //   {
    //     author:( 
    //         <Grid container >
    //             <Grid item sm={sizeG} xs={sizeG} align="center">
    //                 <Image src={coin3} alt="profile-image" width={sizeW} height="100%" />
    //             </Grid>
    //         </Grid>),
    //     status: (
    //       <MDTypography component="a" href="#" variant="h3" color="warning" fontWeight="medium">
    //          990 เหรียญ
    //       </MDTypography>
    //     ),
    //     skill: (
    //       <MDTypography component="a" href="#" variant="h3" color="text" fontWeight="medium">
    //         549 บาท
    //       </MDTypography>
    //     ),
    //     action: (
    //         <MDBox ml={-1}>
    //           <Grid container spacing={2}>
    //           <Grid item sm={sizeButtop} xs={sizeButtop}>
    //             <MDButton variant="gradient" color="warning" onClick={routeChange}>
    //               <Icon>edit</Icon>
    //               แก้ไข
    //             </MDButton>
    //           </Grid>
    //           <Grid item sm={sizeButtop} xs={sizeButtop}>
    //             <MDButton style={{marginLeft: "1%" }} variant="gradient" color="error" onClick={openDelete}>
    //               <Icon>delete</Icon>
    //               ลบ
    //             </MDButton>
    //             {renderSuccessSB}
    //             {renderDelete}
    //           </Grid>
    //           </Grid>
    //         </MDBox>
    //       ),
    //   },
    //   {
    //     author:( 
    //         <Grid container >
    //             <Grid item sm={sizeG} xs={sizeG} align="center">
    //                 <Image src={coin4} alt="profile-image" width={sizeW} height="100%" />
    //             </Grid>
    //         </Grid>),
    //     status: (
    //       <MDTypography component="a" href="#" variant="h3" color="warning" fontWeight="medium">
    //         1980 เหรียญ
    //       </MDTypography>
    //     ),
    //     skill: (
    //       <MDTypography component="a" href="#" variant="h3" color="text" fontWeight="medium">
    //         1100 บาท
    //       </MDTypography>
    //     ),
    //     action: (
    //         <MDBox ml={-1}>
    //           <Grid container spacing={2}>
    //           <Grid item sm={sizeButtop} xs={sizeButtop}>
    //             <MDButton variant="gradient" color="warning" onClick={routeChange}>
    //               <Icon>edit</Icon>
    //               แก้ไข
    //             </MDButton>
    //           </Grid>
    //           <Grid item sm={sizeButtop} xs={sizeButtop}>
    //             <MDButton style={{marginLeft: "1%" }} variant="gradient" color="error" onClick={openDelete}>
    //               <Icon>delete</Icon>
    //               ลบ
    //             </MDButton>
    //             {renderSuccessSB}
    //             {renderDelete}
    //           </Grid>
    //           </Grid>
    //         </MDBox>
    //       ),
    //   },
    //   {
    //     author:( 
    //         <Grid container >
    //             <Grid item sm={sizeG} xs={sizeG} align="center">
    //                 <Image src={coin5} alt="profile-image" width={sizeW} height="100%" />
    //             </Grid>
    //         </Grid>),
    //     status: (
    //       <MDTypography component="a" href="#" variant="h3" color="warning" fontWeight="medium">
    //         3280 เหรียญ
    //       </MDTypography>
    //     ),
    //     skill: (
    //       <MDTypography component="a" href="#" variant="h3" color="text" fontWeight="medium">
    //         1800 บาท
    //       </MDTypography>
    //     ),
    //     action: (
    //         <MDBox ml={-1}>
    //           <Grid container spacing={2}>
    //           <Grid item sm={sizeButtop} xs={sizeButtop}>
    //             <MDButton variant="gradient" color="warning" onClick={routeChange}>
    //               <Icon>edit</Icon>
    //               แก้ไข
    //             </MDButton>
    //           </Grid>
    //           <Grid item sm={sizeButtop} xs={sizeButtop}>
    //             <MDButton style={{marginLeft: "1%" }} variant="gradient" color="error" onClick={openDelete}>
    //               <Icon>delete</Icon>
    //               ลบ
    //             </MDButton>
    //             {renderSuccessSB}
    //             {renderDelete}
    //           </Grid>
    //           </Grid>
    //         </MDBox>
    //       ),
    //   },
    // ],
  };
}
