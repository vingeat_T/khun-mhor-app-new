import * as React from "react";

import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";

import DropdownExampleSearchSelection from "layouts/dropdown/dropDown";

import { Dropdown, Menu } from "semantic-ui-react";

import Grid from "@mui/material/Grid";
import MDBox from "components/MDBox";

import doc1 from "assets/images/doc1.jpg";
import doc2 from "assets/images/doc2.jpg";
import doc3 from "assets/images/doc3.jpg";
import doc4 from "assets/images/doc4.jpg";
import doc5 from "assets/images/doc5.jpg";
import doc6 from "assets/images/doc6.jpg";
import doc7 from "assets/images/doc7.jpg";
import doc8 from "assets/images/doc8.jpg";
import doc9 from "assets/images/doc9.jpg";

import Header from "layouts/doctorHeader/doctorHeader";
import Complex from "layouts/complex/complex";

import Footer from 'examples/Footer'


function nowAsk() {
  //   const [ask, setask] = React.useState("");
  //   const navigate = useNavigate();
  //   const handleChange = (event) => {
  //     setask(event.target.value);
  //     console.log(event);
  //     let path = "/dashboard";
  //     if (event.target.value === 1) {
  //     //   path = "/waitAsk";
  //     } else if (event.target.value === 2) {
  //       path = "/waitAsk";
  //     } else if (event.target.value === 3) {
  //       path = "/nowAsk";
  //     }
  //     navigate(path);
  //   };

  function redirect(value) {
    if (value) {
      window.location.href = value;
    }
  }

  return (
    <DashboardLayout>
      <DashboardNavbar />
      <Menu vertical>
        <Dropdown item text="ถามหมอ">
          <Dropdown.Menu>
            <Dropdown.Item onClick={() => redirect("freeAsk")}>ถามฟรี</Dropdown.Item>
            <Dropdown.Item onClick={() => redirect("waitAsk")}>ถามรอ</Dropdown.Item>
            <Dropdown.Item onClick={() => redirect("nowAsk")}>ถามทันที</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </Menu>
      <MDBox mx={1}>
      <DropdownExampleSearchSelection />
      </MDBox>
      &nbsp;
      <Header>
        <MDBox mb={2} />
        <MDBox p={2}>
          <Grid container spacing={6} display="flex" justifyContent="space-between"  pt={2} px={5}>
            <Grid item xs={12} md={6} xl={4}>
            <MDBox mb={1.5} shadow="xl">
              <Complex
                image={doc1}
                icon="LocationOn"
                color="dark"
                name="นพ. สิริน จันทะพรหม"
                title="โรคหู คอ จมูก"
                university="แพทย์ มข."
                description="โรงพยาบาลขอนแก่น"
                hospital="โรงพยาบาลขอนแก่น"
                price="฿200"
                action={{
                  type: "internal",
                  route: "/report",
                  color: "dark",
                  label: "รายละเอียด",
                }}
  
              />
              </MDBox>
            </Grid>
            <Grid item xs={12} md={6} xl={4}>
            <MDBox mb={1.5} shadow="xl">
              <Complex
                image={doc2}
                name="นพ. ธนวัฒน์ ไวศิลา"
                title="โรคทางเดินหายใจ"
                university="แพทย์ มข."
                description="โรงพยาบาลขอนแก่น"
                hospital="โรงพยาบาลขอนแก่น"
                price="฿200"
                action={{
                  type: "internal",
                  route: "/report",
                  color: "dark",
                  label: "รายละเอียด",
                }}
  
              />
              </MDBox>
            </Grid>
            <Grid item xs={12} md={6} xl={4}>
            <MDBox mb={1.5} shadow="xl">
              <Complex
                image={doc4}
                name="นพ. เซียว จ้าน"
                title="โรคผิวหนัง"
                university="แพทย์ มข."
                hospital="โรงพยาบาลขอนแก่น"
                price="฿200"
                action={{
                  type: "internal",
                  route: "/report",
                  color: "dark",
                  label: "รายละเอียด",
                }}
              />
              </MDBox>
            </Grid>
            <Grid item xs={12} md={6} xl={4}>
            <MDBox mb={1.5} shadow="xl">
              <Complex
                image={doc3}
                icon="LocationOn"
                color="dark"
                name="พญ. สิริน กิ่งมาลา"
                title="โรคหู คอ จมูก"
                university="แพทย์ มข."
                description="โรงพยาบาลขอนแก่น"
                hospital="โรงพยาบาลขอนแก่น"
                price="฿200"
                action={{
                  type: "internal",
                  route: "/report",
                  color: "dark",
                  label: "รายละเอียด",
                }}
  
              />
              </MDBox>
            </Grid>
            <Grid item xs={12} md={6} xl={4}>
            <MDBox mb={1.5} shadow="xl">
              <Complex
                image={doc5}
                name="พญ. กนกวรรณ ภูมิลา "
                title="โรคทางเดินหายใจ"
                university="แพทย์ มข."
                description="โรงพยาบาลขอนแก่น"
                hospital="โรงพยาบาลขอนแก่น"
                price="฿200"
                action={{
                  type: "internal",
                  route: "/report",
                  color: "dark",
                  label: "รายละเอียด",
                }}
  
              />
              </MDBox>
            </Grid>
            <Grid item xs={12} md={6} xl={4}>
            <MDBox mb={1.5} shadow="xl">
              <Complex
                image={doc6}
                name="นพ. อาทิตย์ วรนาม"
                title="โรคผิวหนัง"
                university="แพทย์ มข."
                hospital="โรงพยาบาลขอนแก่น"
                price="฿200"
                action={{
                  type: "internal",
                  route: "/report",
                  color: "dark",
                  label: "รายละเอียด",
                }}
              />
              </MDBox>
            </Grid>
            <Grid item xs={12} md={6} xl={4}>
            <MDBox mb={1.5} shadow="xl">
              <Complex
                image={doc7}
                icon="LocationOn"
                color="dark"
                name="นพ. หฤทธิ์ ศรีมุกดา"
                title="โรคหู คอ จมูก"
                university="แพทย์ มข."
                description="โรงพยาบาลขอนแก่น"
                hospital="โรงพยาบาลขอนแก่น"
                price="฿200"
                action={{
                  type: "internal",
                  route: "/report",
                  color: "dark",
                  label: "รายละเอียด",
                }}
  
              />
              </MDBox>
            </Grid>
            <Grid item xs={12} md={6} xl={4}>
            <MDBox mb={1.5} shadow="xl">
              <Complex
                image={doc8}
                name="นพ. นิติภูมิ ใยปางแก้ว "
                title="โรคทางเดินหายใจ"
                university="แพทย์ มข."
                description="โรงพยาบาลขอนแก่น"
                hospital="โรงพยาบาลขอนแก่น"
                price="฿200"
                action={{
                  type: "internal",
                  route: "/report",
                  color: "dark",
                  label: "รายละเอียด",
                }}
  
              />
              </MDBox>
            </Grid>
            <Grid item xs={12} md={6} xl={4}>
            <MDBox mb={1.5} shadow="xl">
              <Complex
                image={doc9}
                name="พญ. กนกวรรณ พรหมพร"
                title="โรคผิวหนัง"
                university="แพทย์ มข."
                hospital="โรงพยาบาลขอนแก่น"
                price="฿200"
                action={{
                  type: "internal",
                  route: "/report",
                  color: "dark",
                  label: "รายละเอียด",
                }}
              />
              </MDBox>
            </Grid>
          </Grid>
        </MDBox>
      </Header>
      <Footer />
    </DashboardLayout>
  );
}
export default nowAsk;
