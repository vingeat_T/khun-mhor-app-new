/**
=========================================================
* Material Dashboard 2 React - v2.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

// prop-types is a library for typechecking of props
import PropTypes from "prop-types";

// @mui material components
import Card from "@mui/material/Card";
import Divider from "@mui/material/Divider";
// import Icon from "@mui/material/Icon";

// Material Dashboard 2 React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
// import { Button } from "@mui/material";
import MDAvatar from "components/MDAvatar";

import ManageQueue from "layouts/ManageQueue/ManageQueue";
// import MesSage from "examples/Message/MesSage";
import { Message } from 'semantic-ui-react'
import Grid from "@mui/material/Grid";

function ComplexStatisticsCard({ color, title, count, percentage,detail1,detail2, image ,header,content}) {
  
  return (
    <Card sx={{width: '100%'}}>
      <MDBox display="flex" justifyContent="space-between" pt={1.5} px={1.5}  >
        <MDBox
          variant="gradient"
          color={color === "light" ? "dark" : "white"}
          coloredShadow={color}
          borderRadius="xl"
          display="flex"
          justifyContent="center"
          alignItems="center"
          width="3rem"
          height="3rem"
          mt={-1}
        >
          <Grid item>
            <MDAvatar src={image} alt="profile-image" variant="rounded"  shadow="sm" />
          </Grid>
        </MDBox>

        
        <MDBox textAlign="center" lineHeight={1}>
        <MDTypography variant="button" fontWeight="light" color="text">
              {detail1}
              <MDBox variant="button" fontWeight="light" color="text">
              {detail2}
              </MDBox>
          </MDTypography>
        </MDBox>
        <MDBox textAlign="right" lineHeight={1}>
          <MDTypography variant="button"  color="text">
            {title}
          </MDTypography>
          <MDBox textAlign="right" lineHeight={1.5}>
          <MDTypography variant >{count}</MDTypography>
          </MDBox>
          {/* <MDBox textAlign="right" lineHeight={1.5}>
          <MDTypography variant="" >{detail1}</MDTypography>
          </MDBox> */}
          {/* <MDBox textAlign="right" lineHeight={1.5}>
          <MDTypography variant >{detail2}</MDTypography>
          </MDBox> */}
        </MDBox>
        {/* <MDBox
          variant=""
          borderRadius="xl"
          display="flex"
          justifyContent="center"
          alignItems="center"
          width="-3.2rem"
          height="3rem"
          mt={-2}
          me={-20}
        >
          <Icon fontSize="medium" color="error">
            {iconx}
          </Icon>
        </MDBox> */}
      </MDBox>
      {/* <MDBox display="flex" justifyContent="space-between" pt={1.5} px={1.5}>
      <MesSage
      header="รายละเอียดอาการ"
      content="เท้าเป็นหนึ่งในอวัยวะที่มีความซับซ้อนที่สุดของร่างกาย ประกอบด้วยกระดูก 26 ชิ้น เชื่อมต่อกันด้วยข้อต่อ กล้ามเนื้อ เอ็นกล้ามเนื้อและเอ็นยึด"/>
      </MDBox> */}

      <MDBox display="flex" justifyContent="space-between" pt={1.5} px={2} sx={{height: "10rem"}}>
        <MDBox 
        sx={{
          width: "100%", 
          overflowY: "auto",
        }}>
          <Message
          header={header}
          content={content}
          />
      </MDBox>
      </MDBox>
      <Divider />
      <MDBox pb={2} px={2}>
        <MDTypography component="p" variant="button" color="text" display="flex">
          <MDTypography
            component="span"
            variant="button"
            fontWeight="bold"
            color={percentage.color}
          >
            {percentage.amount}
          </MDTypography>
            {percentage.label}
        </MDTypography>
        {/* <Button variant="text">ระดับความสำคัญ</Button> */}
        {/* <Button variant="text">Text</Button> */}
        <MDBox>
        <ManageQueue/>
        </MDBox>
      </MDBox>
    </Card>
  );
}

// Setting default values for the props of ComplexStatisticsCard
ComplexStatisticsCard.defaultProps = {
  color: "info",
  percentage: {
    color: "success",
    text: "",
    label: "",
  },
};

// Typechecking props for the ComplexStatisticsCard
ComplexStatisticsCard.propTypes = {
  color: PropTypes.oneOf([
    "primary",
    "secondary",
    "info",
    "success",
    "warning",
    "error",
    "light",
    "dark",
    "red",
  ]),
  image: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  detail1: PropTypes.string.isRequired,
  detail2: PropTypes.string.isRequired,
  header: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  count: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  percentage: PropTypes.shape({
    color: PropTypes.oneOf([
      "primary",
      "secondary",
      "info",
      "success",
      "warning",
      "error",
      "dark",
      "white",
      "red",
    ]),
    amount: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    label: PropTypes.string,
  }),
  // iconx: PropTypes.node.isRequired,
};

export default ComplexStatisticsCard;
