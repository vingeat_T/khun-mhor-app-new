/**
=========================================================
* Material Dashboard 2 React - v2.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/
import * as React from 'react';
// @mui material components
import Grid from "@mui/material/Grid";
// import { DataGrid } from '@mui/x-data-grid';
// import Card from "@mui/material/Card";

// Material Dashboard 2 React components
import MDBox from "components/MDBox";
// import Box from '@mui/material/Box';
// import MDTypography from "components/MDTypography";

// Material Dashboard 2 React example components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import Footer from "examples/Footer";
// import MDAvatar from "components/MDAvatar";
// import TextField from '@mui/material/TextField';


// import DataTable from "examples/Tables/DataTable";

// Data
// import authorsTableData from "layouts/tables/data/authorsTableData";
// import Manage from "layouts/managemoney/data/manage"
// import manage from "layouts/managemoney/data/manage";
// import datagrid from "layouts/managemoney/data/datagrid";
// import Project from "layouts/managemoney/project";
// import manage from "assets/images/manage.png";
// import patien from "assets/images/patien.jpeg";
// import system from "assets/images/system.png";

// import DefaultDoughnutChart from "examples/Charts/DoughnutCharts/DefaultDoughnutChart"

import Background from 'layouts/managemoney/copmponent/Background'
import Topper from 'layouts/managemoney/copmponent/Header'
// import InfoCard from 'layouts/managemoney/copmponent/Content/Infocard'
import InfoChart from 'layouts/managemoney/copmponent/Content/Chart'
// import SliderBar from 'layouts/managemoney/copmponent/Slider'

// import { Card } from '@mui/material';
// import doctor2 from "assets/images/doctor2.jpg";

// import MDAvatar from "components/MDAvatar";
// import doctor1 from "assets/images/doctor1.jpg";
// import Manage from "layouts/managemoney/data/manage";
// import doctor3 from "assets/images/doctor3.jpg";

import reportsLineChartData from "layouts/dashboard2/data/reportsLineChartData";


function ManageMoney() {
  // const { columns: pColumns, rows: pRows } = datagrid();
  // const [value, setValue] = React.useState(0);
  // const [val, setVal] = React.useState(0);
  // const [value2, setValue2] = React.useState(0);

  // const handleOnChange = (e) => {
  //   setValue2(100 - e.target.value)
  //   setValue(e.target.value)
  //   setVal(e.target.value)
  // }

  const { tasks, sales } = reportsLineChartData

  return (
    <DashboardLayout>
      <DashboardNavbar />
      {/* <Project> */}
      <MDBox p={2} pb={3}>

        <Background />
      
        <MDBox 
          mt={-24} 
          mx= {3}
          py= {2}
          px= {2}
          shadow="xl"
          bgColor="white" 
          borderRadius="xl"
        >
        
        <Topper />

        <MDBox align="center">
          <Grid container spacing={0} sx={{ justifyContent:"space-around" }}>
              
              {/* <InfoCard img={patien} type="คนไข้" />
              <InfoCard img={manage} type="หมอ" />
              <InfoCard img={system} type="ระบบ" />
              
              <SliderBar /> */}

          </Grid>

              {/* Chart */}
          <Grid>
            <InfoChart data={tasks} data2={sales} head="รายได้ระบบ" color="dark" />
          </Grid>

          
        </MDBox>
        </MDBox>

        </MDBox>
      {/* </Project> */}
      <Footer />
    </DashboardLayout>
  );
}

export default ManageMoney;
