import React from 'react'
import backgroundImage from "assets/images/bg-profile.jpeg";

// Material Dashboard 2 React components
import MDBox from "components/MDBox";

function Background() {
  return (
    <MDBox
        bgColor="dark"  
        coloredShadow="info"
        display="flex"
        alignItems="center"
        position="static"
        minHeight="18.75rem"
        borderRadius="xl"
        sx={{
          backgroundImage: ({ functions: { rgba, linearGradient }, palette: { gradients } }) =>
            `${linearGradient(
              rgba(gradients.info.main, 0.6),
              rgba(gradients.info.state, 0.6)
            )}, url(${backgroundImage})`,
          backgroundSize: "cover",
          backgroundPosition: "50%",
          overflow: "hidden",
        }}
      />
  )
}

export default Background