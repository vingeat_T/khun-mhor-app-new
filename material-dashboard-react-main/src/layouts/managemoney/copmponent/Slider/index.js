import React from 'react'

// @mui material components
import Grid from "@mui/material/Grid";
import Slider from "@mui/material/Slider"

// Material Dashboard 2 React components
import MDBox from "components/MDBox";

function SliderBar() {
  return (
    <Grid container spacing={0}>
            <Grid item xs={12} sx={{paddingLeft: "28%", paddingRight: "25%"}}>
              <MDBox
                color="white"
                bgColor="light"
                variant="gradient"
                borderRadius="lg"
                shadow="lg"
                opacity={1}
                sx={{
                  width: 400, 
                  padding: "0% 3%",
                  paddingTop: 0.5,
                }}
              >
                <Slider 
                  defaultValue={0}
                  aria-label="Default" 
                  valueLabelDisplay="auto"
                  step={10}
                  marks
                  sx={{
                      '& .MuiSlider-track': {
                        border: 'none',
                        height: 5,
                        backgroundColor: "#3a8589"
                      },
                      '& .MuiSlider-rail': {
                        border: "none" ,
                        height: 5
                      },
                      '& .MuiSlider-mark': {
                        backgroundColor: "#3a8589",
                        height: 4
                      },
                      '&.MuiSlider-thumb': {
                        width: 13,
                        height: 13,
                        backgroundColor: '#fff',
                        border: 0,
                        '&:before': {
                          boxShadow: '0 4px 8px rgba(0,0,0,0.4)',
                        },
                        '&:hover': {
                          boxShadow: '10px 10px rgba(0, 0, 0, 0.87)'  
                        },
                        '& .Mui-focusVisible': {
                          boxShadow: 'none',
                        },
                        '& .Mui-active': {
                          width: 20,
                          height: 20,
                          boxShadow: '10px 10px rgba(0, 0, 0, 0.87)' 
                        },
                      },
                  }}
                />
              </MDBox>
            </Grid>
          </Grid>
  )
}

export default SliderBar