import React, { useState } from 'react'

// prop-types is a library for typechecking of props.
import PropTypes from "prop-types";

// @mui material components
import Grid from "@mui/material/Grid";

// Material Dashboard 2 React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";

// Material Dashboard 2 React example components
import MDAvatar from "components/MDAvatar";
import TextField from '@mui/material/TextField';

function InfoCard({ img, type }) {

    const [val, setVal] = useState("")

    const handleChange = (e) => {
        setVal(e.target.value)
    }

  return (
    <Grid item lg={3} align="center">
        {/* <Card sx={{ mt:20,width: 200, height: 200 }}> */}
        <MDBox 
            sx={{
            mt:5,
            width: 300,
            height: 250,
            }} 
            px={5}
        >
            <Grid container spacing={3} >
                <Grid item lg={6}>
                    <MDTypography variant="h4" fontWeight="regular" color="text">
                        {type}
                    </MDTypography>
                    <MDAvatar 
                        src={img} 
                        alt="profile-image"  
                        sx={{ width: 100, height: 100 }} 
                        shadow="sm" 
                    />
                    <Grid item lg={6} sx={{mt:4}} align="center">
                        <TextField 
                            color="dark"
                            value={val}
                            size="large"
                            onChange={handleChange}
                        />
                    </Grid>
                </Grid>
            </Grid>
        </MDBox>
        {/* </Card> */}
    </Grid>
  )
}

// Setting default props for the InfoCard
InfoCard.defaultProps = {
    img: "",
};

// Typechecking props for the InfoCard
InfoCard.propTypes = {
    img: PropTypes.node,
};
// Setting default props for the InfoCard
InfoCard.defaultProps = {
    type: "",
};

// Typechecking props for the InfoCard
InfoCard.propTypes = {
    type: PropTypes.node,
};

export default InfoCard