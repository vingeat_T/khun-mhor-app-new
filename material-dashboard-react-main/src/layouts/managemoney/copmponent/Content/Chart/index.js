import React from 'react'

// prop-types is a library for typechecking of props.
import PropTypes from "prop-types";

// @mui material components
import Grid from "@mui/material/Grid";

// Material Dashboard 2 React components
import MDBox from "components/MDBox";

import ReportsLineChart from "examples/Charts/LineCharts/ReportsLineChart";


// import DefaultDoughnutChart from "examples/Charts/DoughnutCharts/DefaultDoughnutChart"

function InfoChart({ data, head, color }) {

  return (
    <Grid item xs={12} md={6} lg={4}>
        <MDBox my={3} pt={3}>
            <ReportsLineChart
                color={color}
                title={head}
                description={
                <>
                    (<strong>+15%</strong>) เพิ่มขึ้น.
                </>
                }
                date="อัพเดทเมื่อ 1 เดือนที่แล้ว"
                chart={data}
            />
        </MDBox>
    </Grid>
  )
}

// Setting default props for the InfoChart
InfoChart.defaultProps = {
    data: {},
};

// Typechecking props for the InfoChart
InfoChart.propTypes = {
    data: PropTypes.node,
};
// Setting default props for the InfoChart
InfoChart.defaultProps = {
    head: "",
};

// Typechecking props for the InfoChart
InfoChart.propTypes = {
    head: PropTypes.node,
};
// Setting default props for the InfoChart
InfoChart.defaultProps = {
    color: "",
};

// Typechecking props for the InfoChart
InfoChart.propTypes = {
    color: PropTypes.node,
};

export default InfoChart