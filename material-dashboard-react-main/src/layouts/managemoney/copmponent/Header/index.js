import React from 'react'

// @mui material components
import Grid from "@mui/material/Grid";

// Material Dashboard 2 React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";

function Topper() {
  return (
    <MDBox mt={5}>
        <Grid container spacing={6}>
        <Grid item xs={12}>
            <MDBox
            mx={2}
            mt={-3}
            py={3}
            px={2}
            variant="gradient"
            bgColor="light"
            borderRadius="lg"
            coloredShadow="dark"
            >
            <MDTypography variant="h6" color="dark">
                จัดการรายได้ของหมอ
            </MDTypography>
            </MDBox>
        </Grid>
        </Grid>
    </MDBox>
  )
}

export default Topper