import React from 'react'

import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";

// prop-types is a library for typechecking of props.
import PropTypes from "prop-types";

// Material Dashboard 2 React components
import MDBox from "components/MDBox";
import MDButton from "components/MDButton";

import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import MDInput from "components/MDInput";
import { Image } from "semantic-ui-react";
import Modal from "@mui/material/Modal";
import blank from "assets/images/blankpicture.png";

function DrModal({ closeModal }) {

    const handleClose = () => {
        closeModal(false)
    }

    const style = {
        position: "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)",
        width: "50%",
        bgcolor: "#f0f2f5",
        boxShadow: 24,
        p: 2,
      };

  return (
    <Modal
        open={closeModal}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
    >
        <Box sx={style}>
            <Card>
                <MDBox pl={2} pt={1}>
                    <Typography id="modal-modal-title" variant="h4" component="h2">
                        จัดการข้อมูลหมอ
                    </Typography>
                </MDBox>
                <Grid container spacing={3}>
                    <Grid item sm={3} xs={3}>
                        <MDBox style={{ marginLeft: "30%",marginTop: "30%" }}>  
                            <Image src={blank} alt="profile-image" width="150px" height="120px" />
                        </MDBox>
                    </Grid>
                    <Grid item sm={9} xs={9}>
                        <MDBox pt={3} px={5}>
                            <MDInput type="text" label="ชื่อ-นามสกุล" fullWidth />
                        </MDBox>
                        <MDBox pt={3} px={5} md={6}>
                            <MDInput
                            type="tel"
                            label="เบอร์โทร"
                            style={{ width: "49%" }}
                            />
                            <MDInput
                            type="email"
                            label="อีเมลล์"
                            style={{ width: "49%", marginLeft: "2%" }}
                            />
                        </MDBox>
                        <MDBox pt={3} px={5} md={6}>
                            <MDInput
                            type="text"
                            label="ชื่อผู้ใช้ในระบบ"
                            style={{ width: "40%" }}
                            />
                            <MDInput
                            type="text"
                            label="ความชำนาญ"
                            style={{ width: "58%", marginLeft: "2%" }}
                            />
                        </MDBox>
                    </Grid>
                </Grid>
                <Grid container mt={1} py={1}>
                    <Grid item sm={12} xs={12} align="center">
                        <MDButton variant="gradient" color="secondary" onClick={handleClose}>
                            กลับ
                        </MDButton>
                        <MDButton
                            variant="gradient"
                            color="success"
                            style={{ marginLeft: "2%" }}
                            onClick={handleClose}
                        >
                            บันทึก
                        </MDButton>
                    </Grid>
                </Grid>
            </Card>
        </Box>
    </Modal>
  )
}

// Setting default props for the DrModal
DrModal.defaultProps = {
    closeModal: false,
  };
  
  // Typechecking props for the DrModal
  DrModal.propTypes = {
    closeModal: PropTypes.node,
  };

export default DrModal