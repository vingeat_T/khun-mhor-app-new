import React from 'react'
import Modal from "@mui/material/Modal";

import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Card from "@mui/material/Card";
import Grid from "@mui/material/Grid";
// prop-types is a library for typechecking of props.
import PropTypes from "prop-types";

// Material Dashboard 2 React components
import MDBox from "components/MDBox";

import MDAvatar from "components/MDAvatar";
import MDBadge from "components/MDBadge";

import MDButton from "components/MDButton";
import team2 from "assets/images/team-2.jpg";

function InfoModal({ closeInfo }) {

    const handleClose = () => {
        closeInfo(false)
    }

    const style = {
        position: "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)",
        width: "50%",
        bgcolor: "#f0f2f5",
        boxShadow: 24,
        p: 2,
      };

  return (
    <Modal
    open={closeInfo}
    onClose={handleClose}
    aria-labelledby="modal-modal-title"
    aria-describedby="modal-modal-description"
    >
    <Box sx={style}>
        <Card>
            <MDBox pl={2} pt={1}>
                <Typography id="modal-modal-title" variant="h4" component="h5">
                    จัดการข้อมูลหมอ
                </Typography>
            </MDBox>
        <MDBox pt={3} px={5}>
            <MDAvatar src={team2} alt="Avatar" variant="rounded" size="lg" />
        </MDBox>
        <MDBox pt={3} px={5}>
        <Typography id="modal-modal-title" variant="h6" component="h5" >
            ชื่อ-นามสกุล 
        </Typography>
        <p>John Smith</p>
        </MDBox>
        <MDBox pt={3} px={5} md={6}>
        <Typography id="modal-modal-title" variant="h6" component="h5" >
            เบอร์โทร 
        </Typography>
        <p>40-(770)-888-444</p>
        <Typography id="modal-modal-title" variant="h6" component="h5" >
            อีเมลล์
        </Typography>
        <p>someone@example.com</p>
        </MDBox>
        <MDBox pt={3} px={5} md={6}>
            <Typography id="modal-modal-title" variant="h7" component="h5">
            ภาษา
            </Typography>
            <MDBadge badgeContent= "EN" size="md" color="success" container style={{ marginRight: "2%" }} />
            <MDBadge badgeContent= "LA" size="md" color="success" container style={{ marginRight: "2%" }} />
            <MDBadge badgeContent= "TH" size="md" color="success" container style={{ marginRight: "2%" }} />
        </MDBox>
        <MDBox pt={3} px={5} md={6}>
        <Typography id="modal-modal-title" variant="h6" component="h5" >
        ความเชี่ยวชาญเฉพาะทาง 
        </Typography>
        <p>แพทย์กระดูก</p>
        </MDBox>
        <MDBox pt={3} px={5} md={6}>
        <Typography id="modal-modal-title" variant="h6" component="h5" >
            วุฒิบัตร 
        </Typography>
        <p>1.แพทยศาสตร์บัณทิต คณะแพทย์ มหาวิทยาลัยขอนแก่น</p>
        </MDBox>
        <MDBox pt={3} px={5} md={6}>
        <Typography id="modal-modal-title" variant="h6" component="h5" >
            สถานที่ 
        </Typography>
        <p>โรงพยาบาลศรีนครินทร์ คณะแพทยศาสตร์ มหาวิทยาลัยขอนแก่น</p>
        </MDBox>
        <Grid container my={1} py={1}>
            <Grid item sm={12} xs={12} align="center">
            <MDButton variant="gradient" color="secondary" onClick={handleClose}>
                กลับ
            </MDButton>
            <MDButton
                variant="gradient"
                color="success"
                style={{ marginLeft: "2%" }}
                onClick={handleClose}
            >
                บันทึก
            </MDButton>
            </Grid>
        </Grid>
        </Card>
    </Box>
    </Modal>
  )
}

// Setting default props for the InfoModal
InfoModal.defaultProps = {
    closeInfo: false,
};
  
// Typechecking props for the InfoModal
InfoModal.propTypes = {
    closeInfo: PropTypes.node,
};

export default InfoModal