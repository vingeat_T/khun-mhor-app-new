import React from 'react'

import Modal from "@mui/material/Modal";
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import FormGroup from '@mui/material/FormGroup';
import Grid from "@mui/material/Grid";
// prop-types is a library for typechecking of props.
import PropTypes from "prop-types";

// Material Dashboard 2 React components
import MDBox from "components/MDBox";

import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import MDInput from "components/MDInput";
import Card from "@mui/material/Card";
import MDAvatar from "components/MDAvatar";

import MDButton from "components/MDButton";
import team2 from "assets/images/team-2.jpg";

function EditModal({ closeEdit }) {

    const handleClose = () => {
        closeEdit(false)
    }

    const style = {
        position: "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)",
        width: "50%",
        bgcolor: "#f0f2f5",
        boxShadow: 24,
        p: 2,
      };

  return (
    <Modal
    open={closeEdit}
    onClose={handleClose}
    aria-labelledby="modal-modal-title"
    aria-describedby="modal-modal-description"
    >
    <Box sx={style}>
        <Card>
            <MDBox pl={2} pt={1}>
                <Typography id="modal-modal-title" variant="h4" component="h2">
                    จัดการข้อมูลหมอ
                </Typography>
            </MDBox>
        <MDBox pt={3} px={5}>
            <MDAvatar src={team2} alt="Avatar" variant="rounded" size="lg" />
        </MDBox>
        <MDBox pt={3} px={5}>
            <MDInput type="text" label="ชื่อ-นามสกุล" value="John Smith" fullWidth />
        </MDBox>
        <MDBox pt={3} px={5} md={6}>
            <MDInput
            type="tel"
            label="เบอร์โทร"
            value="40-(770)-888-444"
            style={{ width: "49%" }}
            />
            <MDInput
            type="email"
            label="อีเมลล์"
            value="someone@example.com"
            style={{ width: "49%", marginLeft: "2%" }}
            />
        </MDBox>
        <MDBox pt={3} px={5} md={6}>
        <Typography id="modal-modal-title" variant="h7" component="h5">
            เลือกภาษา
        </Typography>
        <FormGroup aria-label="position" row>
            <FormControlLabel control={<Checkbox defaultChecked />} label="TH" />
            <FormControlLabel control={<Checkbox defaultChecked />} label="EN" />
            <FormControlLabel control={<Checkbox defaultChecked />} label="LA" /> 
            <FormControlLabel control={<Checkbox defaultChecked />} label="CN" /> 
        </FormGroup>
        </MDBox>
        <MDBox pt={3} px={5} md={6}>
        <MDInput
            type="text"
            label="ความเชี่ยวชาญเฉพาะทาง"
            value="แพทย์กระดูก"
            style={{ width: "58%" }}
            />
        </MDBox>
        <MDBox pt={3} px={5} md={6}>
            <MDInput
            type="text"
            label="วุฒิบัตร"
            value="1.แพทยศาสตร์บัณทิต คณะแพทย์ มหาวิทยาลัยขอนแก่น"
            fullWidth
            />
        </MDBox>
        <MDBox pt={3} px={5} md={6}>
            <MDInput
            type="text"
            label="สถานที่"
            value="โรงพยาบาลศรีนครินทร์ คณะแพทยศาสตร์ มหาวิทยาลัยขอนแก่น"
            fullWidth
            />
        </MDBox>
        <Grid container my={1} py={1}>
            <Grid item sm={12} xs={12} align="center">
            <MDButton variant="gradient" color="secondary" onClick={handleClose}>
                กลับ
            </MDButton>
            <MDButton
                variant="gradient"
                color="success"
                style={{ marginLeft: "2%" }}
                onClick={handleClose}
            >
                บันทึก
            </MDButton>
            </Grid>
        </Grid>
        </Card>
    </Box>
    </Modal>
  )
}

// Setting default props for the EditModal
EditModal.defaultProps = {
    closeEdit: false,
};
  
// Typechecking props for the EditModal
EditModal.propTypes = {
    closeEdit: PropTypes.node,
};

export default EditModal