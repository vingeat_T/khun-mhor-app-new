// import { styled } from "@mui/material/styles";
// import Box from "@mui/material/Box";
// import Paper from "@mui/material/Paper";
// import Grid from "@mui/material/Grid";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import React from "react";
// import { useNavigate } from "react-router-dom";

// Material Dashboard 2 React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import MDButton from "components/MDButton";
import Icon from "@mui/material/Icon";

// Material Dashboard 2 React example components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import DataTable from "examples/Tables/DataTable";
import Footer from 'examples/Footer'

// Data
import authorsTableData from "layouts/usertest/data/dotorData";
// import Box from "@mui/material/Box";
// import Typography from "@mui/material/Typography";
// import MDInput from "components/MDInput";
// import { Image } from "semantic-ui-react";
// import Modal from "@mui/material/Modal";
// import blank from "assets/images/blankpicture.png";

import DrModal from 'layouts/usertest/component/drModal'

function UserTest() {

  const { columns, rows } = authorsTableData();

  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);

  return (
    <DashboardLayout>
      <DashboardNavbar />
      <MDBox pb={3}>
        <Grid container spacing={6}>
          <Grid item xs={12}>
            <Grid container mx={2} mt={1} py={1} px={2}>
              <Grid item sm={12} xs={12} align="right">
                <MDButton variant="gradient" color="success" onClick={handleOpen} style={{ marginRight: "2%" }}>
                  <Icon>add</Icon>
                  เพิ่ม
                </MDButton>
                
                { open && <DrModal closeModal={setOpen} />}

              </Grid>
            </Grid>
            <Card>
              <MDBox
                mx={2}
                mt={1}
                py={3}
                px={2}
                variant="gradient"
                bgColor="success"
                borderRadius="lg"
                coloredShadow="info"
              >
                <MDTypography variant="h6" color="white">
                  จัดการข้อมูลหมอ
                </MDTypography>
              </MDBox>
              <MDBox pt={3} pb={1}>
                <DataTable
                  table={{ columns, rows }}
                  isSorted={false}
                  entriesPerPage={false}
                  showTotalEntries={false}
                  noEndBorder
                />
              </MDBox>
            </Card>
          </Grid>
        </Grid>
      </MDBox>
      <Footer />
    </DashboardLayout>
  );
}

export default UserTest;
