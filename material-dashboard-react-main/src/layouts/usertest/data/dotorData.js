/* eslint-disable react/prop-types */

// Soft UI Dashboard React components
import { useState } from "react";
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import MDAvatar from "components/MDAvatar";
import MDBadge from "components/MDBadge";
import Grid from "@mui/material/Grid";
import MDButton from "components/MDButton";
import Icon from "@mui/material/Icon";
import * as React from "react";
import MDSnackbar from "components/MDSnackbar";
// import Box from "@mui/material/Box";
// import { useNavigate } from "react-router-dom";
// Images
// import logoXD from "assets/images/small-logos/logo-xd.svg";
// import logoAtlassian from "assets/images/small-logos/logo-atlassian.svg";
// import logoSlack from "assets/images/small-logos/logo-slack.svg";
// import logoSpotify from "assets/images/small-logos/logo-spotify.svg";
// import logoJira from "assets/images/small-logos/logo-jira.svg";
// import logoInvesion from "assets/images/small-logos/logo-invision.svg";
// import FormControlLabel from '@mui/material/FormControlLabel';
// import Checkbox from '@mui/material/Checkbox';
// import FormGroup from '@mui/material/FormGroup';
import team2 from "assets/images/team-2.jpg";
import team3 from "assets/images/team-3.jpg";
import team4 from "assets/images/team-4.jpg";
// import Modal from "@mui/material/Modal";
import VisibilityIcon from '@mui/icons-material/Visibility';

import EditModal from 'layouts/usertest/component/editModal'
import InfoModal from 'layouts/usertest/component/infoModal'

export default function data() {
  const Author = ({ image, name, email }) => (
    <MDBox display="flex" alignItems="center" lineHeight={1}>
      <MDAvatar src={image} name={name} size="sm" />
      <MDBox ml={2} lineHeight={1}>
        <MDTypography display="block" variant="button" fontWeight="medium">
          {name}
        </MDTypography>
        <MDTypography variant="caption">{email}</MDTypography>
      </MDBox>
    </MDBox>
  );

  const [successSB, setSuccessSB] = useState(false);
  const [deleteSB, setDeleteSB] = useState(false);

  // const openSuccessSB = () => setSuccessSB(true);
  const openDelete = () => setDeleteSB(true);
  const closeSuccessSB = () => setSuccessSB(false);
  const closeDeleteSB = () => setDeleteSB(false);

  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);

  const [open1, setOpen1] = React.useState(false);
  const viewOpen = () => setOpen1(true);


  const renderSuccessSB = (
    <MDSnackbar
      color="warning"
      icon="check"
      title="Console Dashboard"
      content="This is a data demo can't modify"
      dateTime="11 mins ago"
      open={successSB}
      onClose={closeSuccessSB}
      close={closeSuccessSB}
      bgWhite
    />
  );

  // const style2 = {
  //   position: "absolute",
  //   top: "50%",
  //   left: "50%",
  //   transform: "translate(-50%, -50%)",
  //   width: "50%",
  //   bgcolor: "background.paper",
  //   border: "2px solid #000",
  //   boxShadow: 24,
  //   p: 4,
  // };

  // const navigate = useNavigate();
  // const routeChange = () => {
  //   const path = "/tables";
  //   navigate(path);
  // };

  const renderDelete = (
    <MDSnackbar
      color="error"
      icon="check"
      title="Console Dashboard"
      content="This is a data demo can't delete"
      dateTime="11 mins ago"
      open={deleteSB}
      onClose={closeDeleteSB}
      close={closeDeleteSB}
      bgWhite
    />
  );

  const Job = ({ title, description }) => (
    <MDBox lineHeight={1} textAlign="left">
      <MDTypography display="block" variant="caption" color="text" fontWeight="medium">
        {title}
      </MDTypography>
      <MDTypography variant="caption">{description}</MDTypography>
    </MDBox>
  );

  // const dataColumns = [
  //   { Header: "ชื่อผู้ใช้", accessor: "author", width: "30%", align: "left" },
  //   { Header: "สถานะการออนไลน์", accessor: "status", align: "center" },
  //   { Header: "ความชำนาญ", accessor: "skill", align: "center" },
  //   { Header: "สถานะการเข้าใช้งาน", accessor: "employed", align: "center" },
  //   { Header: "เลือกแก้ไข", accessor: "action", align: "center" },
  // ]

  // const rows = [
  //   {img: team2, name: "John Michael", email: "john@creative-tim.com", status: "Offline", skill: "แพทย์กระดูก", employed: "23/04/18"},
  //   {img: team3, name: "Alexa Liras", email: "alexa@creative-tim.com", status: "Online", skill: "ศัลยแพทย์หัวใจ", employed: "11/01/19"},
  //   {img: team4, name: "Laurent Perrier", email: "laurent@creative-tim.com", status: "Online", skill: "ศัลยแพทย์ทั่วไป", employed: "19/09/17"},
  //   {img: team2, name: "Michael Levi", email: "michael@creative-tim.com", status: "Online", skill: "สูตินรีแพทย์", employed: "24/12/08"},
  //   {img: team2, name: "John Michael", email: "john@creative-tim.com", status: "Offline", skill: "แพทย์กระดูก", employed: "23/04/18"},
  //   {img: team3, name: "Alexa Liras", email: "alexa@creative-tim.com", status: "Online", skill: "ศัลยแพทย์หัวใจ", employed: "11/01/19"},
  //   {img: team4, name: "Laurent Perrier", email: "laurent@creative-tim.com", status: "Offline", skill: "ศัลยแพทย์ทั่วไป", employed: "19/09/17"},
  // ]

  // const setRowArray = []
  // if (rows) {
  //   rows.forEach((val) => {
  //     let colorSet
  //     if (val.status === 'Online') {
  //       colorSet = 'success'
  //     }
  //     if (val.status === 'Offline') {
  //       colorSet = 'dark'
  //     }

  //     const renderRow = {
  //       author: (
  //         <Author image={val.img} name={val.name} email={val.email} />
  //       ),
  //       status: (
  //         <MDBox ml={-1}>
  //           <MDBadge badgeContent={val.status} color={colorSet} variant="gradient" size="sm" />
  //         </MDBox>
  //       ),
  //       skill: (
  //         <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
  //           {val.skill}
  //         </MDTypography>
  //       ),
  //       employed: (
  //         <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
  //           {val.employed}
  //         </MDTypography>
  //       ),
  //       action: (
  //         <MDBox ml={2} pt={2}>
  //           <Grid container spacing={2}>
  //             <MDButton variant="gradient" color="warning" onClick={handleOpen}>
  //               <Icon>edit</Icon>
  //               แก้ไข
  //             </MDButton>
              
  //             {/* EditModal */}
  //             {open && <EditModal closeEdit={setOpen}/>}

  //             <MDButton variant="gradient" color="info" startIcon={<VisibilityIcon />} onClick={viewOpen}>
  //                 ดูเพิ่มเติม
  //             </MDButton>
              
  //             {/* InfoModal */}
  //             {open1 && <InfoModal closeInfo={setOpen1}/>}

  //             {renderSuccessSB}
  //             {renderDelete}
  //           </Grid>
  //         </MDBox>
  //       ),
  //     }
  //     setRowArray.push(renderRow)
  //   })
  // }

  return {
    // columns: dataColumns,

    // rows: setRowArray,

    columns: [
      { Header: "ชื่อผู้ใช้", accessor: "author", width: "30%", align: "left" },
      { Header: "สถานะการออนไลน์", accessor: "status", align: "center" },
      { Header: "ความชำนาญ", accessor: "skill", align: "center" },
      { Header: "สถานะการเข้าใช้งาน", accessor: "employed", align: "center" },
      { Header: "เลือกแก้ไข", accessor: "action", align: "center" },
    ],

    rows: [
      {
        author: <Author image={team2} name="John Michael" email="john@creative-tim.com" />,
        function: <Job title="Manager" description="Organization" />,
        status: (
          <MDBox ml={-1}>
            <MDBadge badgeContent="ออนไลน์" color="success" variant="gradient" size="sm" />
          </MDBox>
        ),
        skill: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            แพทย์กระดูก
          </MDTypography>
        ),
        employed: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            23/04/18
          </MDTypography>
        ),
        action: (
          <MDBox ml={-1}>
            <Grid container spacing={2}>
              <MDButton variant="gradient" color="warning" onClick={handleOpen}>
                <Icon>edit</Icon>
                แก้ไข
              </MDButton>
              
              {/* EditModal */}
              {open && <EditModal closeEdit={setOpen}/>}

              <MDButton variant="gradient" color="info" startIcon={<VisibilityIcon />} onClick={viewOpen}>
                  ดูเพิ่มเติม
              </MDButton>
              
              {/* InfoModal */}
              {open1 && <InfoModal closeInfo={setOpen1}/>}


              {renderSuccessSB}
              {renderDelete}
            </Grid>
          </MDBox>
        ),
      },
      {
        author: <Author image={team3} name="Alexa Liras" email="alexa@creative-tim.com" />,
        function: <Job title="Programator" description="Developer" />,
        status: (
          <MDBox ml={-1}>
            <MDBadge badgeContent="ออฟไลน์" color="dark" variant="gradient" size="sm" />
          </MDBox>
        ),
        skill: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            ศัลยแพทย์หัวใจ
          </MDTypography>
        ),
        employed: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            11/01/19
          </MDTypography>
        ),
        action: (
          <MDBox ml={-1}>
            <Grid container spacing={2}>
              <MDButton variant="gradient" color="warning">
                <Icon>edit</Icon>
                แก้ไข
              </MDButton>
              <MDButton variant="gradient" color="info" startIcon={<VisibilityIcon />} onClick={closeDeleteSB}>
                  ดูเพิ่มเติม
              </MDButton>
            </Grid>
          </MDBox>
        ),
      },
      {
        author: <Author image={team4} name="Laurent Perrier" email="laurent@creative-tim.com" />,
        function: <Job title="Executive" description="Projects" />,
        status: (
          <MDBox ml={-1}>
            <MDBadge badgeContent="ออนไลน์" color="success" variant="gradient" size="sm" />
          </MDBox>
        ),
        skill: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            ศัลยแพทย์ทั่วไป
          </MDTypography>
        ),
        employed: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            19/09/17
          </MDTypography>
        ),
        action: (
          <MDBox ml={-1}>
            <Grid container spacing={2}>
              <MDButton variant="gradient" color="warning">
                <Icon>edit</Icon>
                แก้ไข
              </MDButton>
              <MDButton variant="gradient" color="info" startIcon={<VisibilityIcon />} onClick={openDelete}>
                  ดูเพิ่มเติม
              </MDButton>
            </Grid>
          </MDBox>
        ),
      },
      {
        author: <Author image={team3} name="Michael Levi" email="michael@creative-tim.com" />,
        function: <Job title="Programator" description="Developer" />,
        status: (
          <MDBox ml={-1}>
            <MDBadge badgeContent="online" color="success" variant="gradient" size="sm" />
          </MDBox>
        ),
        skill: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            สูตินรีแพทย์
          </MDTypography>
        ),
        employed: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            24/12/08
          </MDTypography>
        ),
        action: (
          <MDBox ml={-1}>
            <Grid container spacing={2}>
              <MDButton variant="gradient" color="warning">
                <Icon>edit</Icon>
                แก้ไข
              </MDButton>
              <MDButton variant="gradient" color="info" startIcon={<VisibilityIcon />} onClick={openDelete}>
                  ดูเพิ่มเติม
              </MDButton>
            </Grid>
          </MDBox>
        ),
      },
      {
        author: <Author image={team3} name="Richard Gran" email="richard@creative-tim.com" />,
        function: <Job title="Manager" description="Executive" />,
        status: (
          <MDBox ml={-1}>
            <MDBadge badgeContent="offline" color="dark" variant="gradient" size="sm" />
          </MDBox>
        ),
        skill: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            แพทย์เฉพาะทางด้าน ตา หู คอ จมูก
          </MDTypography>
        ),
        employed: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            04/10/21
          </MDTypography>
        ),
        action: (
          <MDBox ml={-1}>
            <Grid container spacing={2}>
              <MDButton variant="gradient" color="warning">
                <Icon>edit</Icon>
                  แก้ไข
              </MDButton>
              <MDButton variant="gradient" color="info" startIcon={<VisibilityIcon />} onClick={openDelete}>
                  ดูเพิ่มเติม
              </MDButton>
            </Grid>
          </MDBox>
        ),
      },
      {
        author: <Author image={team4} name="Miriam Eric" email="miriam@creative-tim.com" />,
        function: <Job title="Programator" description="Developer" />,
        status: (
          <MDBox ml={-1}>
            <MDBadge badgeContent="ออฟไลน์" color="dark" variant="gradient" size="sm" />
          </MDBox>
        ),
        skill: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            ศัลยแพทย์ช่องปาก
          </MDTypography>
        ),
        employed: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            14/09/20
          </MDTypography>
        ),
        action: (
          <MDBox ml={-1}>
            <Grid container spacing={2}>
              <MDButton variant="gradient" color="warning">
                <Icon>edit</Icon>
                แก้ไข
              </MDButton>
              <MDButton variant="gradient" color="info" startIcon={<VisibilityIcon />} onClick={openDelete}>
                  ดูเพิ่มเติม
              </MDButton>
            </Grid>
          </MDBox>
        ),
      },
    ],
  };
}
