import * as React from "react";

export default function useShareableState() {
  const [search, setValue] = React.useState('');
  const [dataSelect, setdata] = React.useState({});
  return {
    search,
    setValue,
    dataSelect,
    setdata
  };
};