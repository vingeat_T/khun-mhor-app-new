import { useState } from "react";
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
// import MDAvatar from "components/MDAvatar";
// import MDBadge from "components/MDBadge";
import Grid from "@mui/material/Grid";
import MDButton from "components/MDButton";
// import Icon from "@mui/material/Icon";
import * as React from "react";
import MDSnackbar from "components/MDSnackbar";
import { useNavigate } from "react-router-dom";
import VisibilityIcon from '@mui/icons-material/Visibility';
import searchtable from "layouts/transetionCoin/data/searchtable";
import { useBetween } from "use-between";


export default function data() {
    
    const { search,setdata } = useBetween(searchtable);
    const navigate = useNavigate();
    const sendEmail = async (name) => {
        console.log(name)
        setdata(name)
        const path = "/search";
        navigate(path);

      };
    


 const dataTransation = [{ Id: "156789", Name: "John", Doctor: "หมอ ใหญ่", Type: "ถามทันที", Date: "2022-01-11", Amoute: "120$" },
        { Id: "156762", Name: "ธน", Doctor: "หมอ ใหม่", Type: "ถามทันที", Date: "2022-02-12", Amoute: "120$"},
        { Id: "156715", Name: "รอน", Doctor: "หมอ ให้", Type: "ถามฟรี", Date: "2022-01-12", Amoute: "0$" },
        { Id: "156797", Name: "มอณ", Doctor: "หมอ ใภ้", Type: "ถามทันที", Date: "2022-01-12", Amoute: "120$" },
        { Id: "156767", Name: "ยอน", Doctor: "หมอ ใช้", Type: "ถามรอ", Date: "2022-01-12", Amoute: "120$"},
        { Id: "156731", Name: "บอน", Doctor: "หมอ ใผ่", Type: "ถามทันที", Date: "2022-01-12", Amoute: "120$" },
        { Id: "156719", Name: "ชอน", Doctor: "หมอ ใจ", Type: "ถามรอ", Date: "2022-01-12", Amoute: "120$" },
        { Id: "156742", Name: "ถอน", Doctor: "หมอ ใส่", Type: "ถามรอ", Date: "2022-01-12", Amoute: "120$" },
        { Id: "156798", Name: "ออน", Doctor: "หมอ ใหล", Type: "ถามฟรี", Date: "2022-01-12", Amoute: "0$" },
        { Id: "156773", Name: "ฟอน", Doctor: "หมอ ใคร", Type: "ถามฟรี", Date: "2022-01-12", Amoute: "0$" },
        { Id: "156794", Name: "สอน", Doctor: "หมอ ใบ้", Type: "ถามรอ", Date: "2022-01-12", Amoute: "120$"},
        { Id: "156764", Name: "นอน", Doctor: "หมอ ใบ", Type: "ถามฟรี", Date: "2022-01-12", Amoute: "0$" }]

 const dataCalumn =  [
        { Header: "IdTransaction", accessor: "number", align: "left" },
        { Header: "ชื่อ", accessor: "name", align: "center" },
        { Header: "หมอที่รับผิดชอบ", accessor: "doctor", align: "center" },
        { Header: "ประเภทการใช้งาน", accessor: "type", align: "center" },
        { Header: "วันที่", accessor: "date", align: "center" },
        { Header: "วันที่", accessor: "action", align: "center" }
    ]


    const [successSB, setSuccessSB] = useState(false);
    const [deleteSB, setDeleteSB] = useState(false);

    // const openSuccessSB = () => setSuccessSB(true);
    // const openDelete = () => setDeleteSB(true);
    const closeSuccessSB = () => setSuccessSB(false);
    const closeDeleteSB = () => setDeleteSB(false);

    const renderSuccessSB = (
        <MDSnackbar
            color="warning"
            icon="check"
            title="Console Dashboard"
            content="This is a data demo can't modify"
            dateTime="11 mins ago"
            open={successSB}
            onClose={closeSuccessSB}
            close={closeSuccessSB}
            bgWhite
        />
    );

    const renderDelete = (
        <MDSnackbar
            color="error"
            icon="check"
            title="Console Dashboard"
            content="This is a data demo can't delete"
            dateTime="11 mins ago"
            open={deleteSB}
            onClose={closeDeleteSB}
            close={closeDeleteSB}
            bgWhite
        />
    );

    const setRowArray = []
    if (dataTransation) {
        dataTransation.forEach((item) => { 
            if((item.Name.indexOf(search) > -1) || (item.Doctor.indexOf(search) > -1)) {
            const renderRow = {
                number: (
                    <MDTypography component="a" href="#" variant="h6" color="text" fontWeight="medium">
                        {item.Id}
                    </MDTypography>
                ),
                name: (
                    <MDTypography component="a" href="#" variant="h6" color="text" fontWeight="medium">
                        {item.Name}
                    </MDTypography>
                ),
                doctor: (
                    <MDTypography component="a" href="#" variant="h6" color="text" fontWeight="medium">
                        {item.Doctor}
                    </MDTypography>
                ),
                symptom: (
                    <MDTypography component="a" href="#" variant="h6" color="text" fontWeight="medium">
                        {item.Symptom}
                    </MDTypography>
                ),
                type: (
                    <MDTypography component="a" href="#" variant="h6" color="text" fontWeight="medium">
                        {item.Type}
                    </MDTypography>
                ),
                date: (
                    <MDTypography component="a" href="#" variant="h6" color="text" fontWeight="medium">
                        {item.Date}
                    </MDTypography>
                ),
                amoute: (
                    <MDTypography component="a" href="#" variant="h6" color="text" fontWeight="medium">
                        {item.Amoute}
                    </MDTypography>
                ),
                action: (
                    <MDBox ml={2} mt={2}>
                        <Grid container spacing={2}>
                            <MDButton variant="gradient" color="info" startIcon={<VisibilityIcon />} onClick={() => sendEmail(item)}>
                                ดูเพิ่มเติม
                            </MDButton>
                            {renderSuccessSB}
                            {renderDelete}
                        </Grid>
                    </MDBox>
                ),
            }

            setRowArray.push(renderRow)
        }
        })
    }
    console.log('151', setRowArray);
    // const test = ([{
    //     number: (
    //         <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
    //             {dataTransation.Id}
    //         </MDTypography>
    //     )}])

    return {
        columns: dataCalumn,

        rows: setRowArray 

    };
}