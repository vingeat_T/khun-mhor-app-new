import React from "react";
import MDInput from "components/MDInput";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import Footer from 'examples/Footer'
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import { Image } from "semantic-ui-react";
import MDButton from "components/MDButton";
import { useNavigate,useSearchParams } from "react-router-dom";
import team2 from "assets/images/team-2.jpg";
import blank from "assets/images/blankpicture.png";
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import TextField from '@mui/material/TextField';
import Typography from "@mui/material/Typography";
import searchtable from "layouts/transetionCoin/data/searchtable";
import { useBetween } from "use-between";

function EditData() {
  const [searchParams] = useSearchParams();
  
  const { dataSelect } = useBetween(searchtable);
  const id = searchParams.get("id")
  console.log(dataSelect);
  let user = {
    name: "John Smith ",
    select: "",
    coin: "",
    date: "06/12/1935",
    tel: "0895477651",
    image: team2,
    weight: 96,
    high: 178,
    bmi: 30.5,
    blood: 90,
    blood2: 91,
    temp: 37,
    o2: 98
  }

  if (id === '120') {
    user = {
      name: null,
      select: null,
      coin: "0",
      date: null,
      tel: null,
      image: blank,
      weight: null,
      high: null,
      bmi: null,
      blood: null,
      blood2: null,
      temp: null,
      o2: null
    }
  }
  let dataInput = (
  <Grid container spacing={0}>
    <Grid item sm={3} xs={3}>
      <MDBox style={{ marginLeft: "25%",marginTop: "25%" }}>  
          <Image src={user.image} alt="profile-image" width="120px" height="120px" />
      </MDBox>
    </Grid>
    <Grid item sm={9} xs={9}>
        <MDBox pt={3} px={5}>
          <MDInput type="text" label="ชื่อ-นามสกุล" value={user.name} fullWidth />
        </MDBox>
        <MDBox pt={2} px={5} md={6}>
          {/* <MDInput
              type="tel"
              label="เบอร์โทร"
              value={user.tel}
              style={{ width: "48.5%" }}
              /> */}
              <FormControl>
              <FormLabel id="demo-radio-buttons-group-label" size="sm">เพศ</FormLabel>
              <RadioGroup
                aria-labelledby="demo-radio-buttons-group-label"
                defaultValue="ชาย"
                name="radio-buttons-group"
                aria-label="position" row
              >
                <FormControlLabel value="ชาย" control={<Radio />} label="ชาย" />
                <FormControlLabel value="หญิง" control={<Radio />} label="หญิง" />
                <FormControlLabel value="อื่นๆ" control={<Radio />} label="อื่นๆ" />
              </RadioGroup>
            </FormControl>
          </MDBox>
        <MDBox pt={3} px={5} md={6}>
          <TextField
            id="date"
            label="วันเดือนปีเกิด"
            type="date"
            defaultValue="2017-05-24"
            sx={{ width: "42%" }}
            InputLabelProps={{
              shrink: true,
            }}
          />
          <MDInput
            type="text"
            label="ยาที่ใช้ในปัจจุบัน"
            value={user.select}
            style={{ width: "50%", marginLeft: "2%" }}
          />
        </MDBox>
        <MDBox pt={3} px={5} md={6}>
          <FormControl>
            <FormLabel id="demo-radio-buttons-group-label" size="sm">โรคประจำตัว</FormLabel>
            <RadioGroup
              aria-labelledby="demo-radio-buttons-group-label"
              defaultValue="ไม่มี"
              name="radio-buttons-group"
              aria-label="position" row
            >
              <FormControlLabel value="ไม่มี" control={<Radio />} label="ไม่มี" />
              <FormControlLabel value="มี" control={<Radio />} label="มี" />
              <MDInput
              placeholder="โรคประจำตัว.." 
              type="text"  
              value={user.coin} 
              style={{ width: "55%" }} 
              />
            </RadioGroup>
          </FormControl>
        </MDBox>
        <MDBox pt={3} px={5} md={6}>
          <FormControl>
            <FormLabel id="demo-radio-buttons-group-label" size="sm">ประวัติการแพ้ยา</FormLabel>
            <RadioGroup
              aria-labelledby="demo-radio-buttons-group-label"
              defaultValue="ไม่มี"
              name="radio-buttons-group"
              aria-label="position" row
            >
              <FormControlLabel value="ไม่มี" control={<Radio />} label="ไม่มี" />
              <FormControlLabel value="มี" control={<Radio />} label="มี" />
              <MDInput 
              placeholder="แพ้ยา.." 
              type="text"  
              value={user.coin} 
              style={{ width: "55%" }} 
              />
            </RadioGroup>
          </FormControl>
        </MDBox>
        <MDBox pt={3} px={5} md={6}>
        <MDInput
            type="text"
            label="น้ำหนัก"
            value={user.weight}
            style={{ width: "45%", marginLeft: "2%" }}
          />
          <MDInput
            type="text"
            label="ส่วนสูง"
            value={user.high}
            style={{ width: "45%", marginLeft: "2%" }}
          />
        </MDBox>
        <MDBox pt={3} px={5} md={6}>
        <MDInput
            type="text"
            label="BMI"
            value={user.bmi}
            style={{ width: "45%", marginLeft: "2%" }}
          />
          <MDInput
            type="text"
            label="ความดันโลหิต"
            value={user.blood}
            style={{ width: "45%", marginLeft: "2%" }}
          />
        </MDBox>
        <MDBox pt={3} px={5} md={6}>
        <MDInput
            type="text"
            label="ชีพจร"
            value={user.blood2}
            style={{ width: "45%", marginLeft: "2%" }}
          />
          <MDInput
            type="text"
            label="อุณหภูมิร่างกาย"
            value={user.temp}
            style={{ width: "45%", marginLeft: "2%" }}
          />
        </MDBox>
        <MDBox pt={3} px={5} md={6}>
        <MDInput
            type="text"
            label="ระดับออกซิเจนปลายนิ้ว"
            value={user.o2}
            style={{ width: "70%", marginLeft: "2%" }}
          />
        </MDBox>
      </Grid>
    </Grid>
  )



  if (id === '115') {
      dataInput = (<Grid container spacing={6}>
        <Grid item sm={3} xs={3}>
          <MDBox pt={3} style={{ marginLeft: "25%",marginTop: "30%"}}>  
              <Image src={user.image} alt="profile-image" width="150px" height="120px" />
          </MDBox>
        </Grid>
        <Grid item sm={9} xs={9}>
            <MDBox pt={3} px={5}>
              <Typography id="modal-modal-title" variant="h6" component="h5" >
                ชื่อ-นามสกุล 
              </Typography>
              <p>{user.name}</p>
            </MDBox>
            <MDBox pt={2} px={5} md={6}>
            <Typography id="modal-modal-title" variant="h6" component="h5" >
                เพศ
              </Typography>
              <p>ชาย</p>
            </MDBox>
            <MDBox pt={3} px={5} md={6}>
            <Typography id="modal-modal-title" variant="h6" component="h5" >
                วันเดือนปีเกิด
            </Typography>
              <p>{user.date}</p>
            <Typography id="modal-modal-title" variant="h6" component="h5" >
              ยาที่ใช้ในปัจจุบัน
            </Typography>
              <p>{user.select}</p>
            </MDBox>
            <MDBox pt={3} px={5} md={6}>
            <Typography id="modal-modal-title" variant="h6" component="h5" >
              โรคประจำตัว
            </Typography>
              <p>-</p>
            </MDBox>
            <MDBox pt={3} px={5} md={6}>
            <Typography id="modal-modal-title" variant="h6" component="h5" >
            ประวัติการแพ้ยา
            </Typography>
              <p>-</p>
            </MDBox>
            <MDBox pt={3} px={5} md={6}>
            <Typography id="modal-modal-title" variant="h6" component="h5" >
              น้ำหนัก
            </Typography>
              <p>{user.weight}</p>
            <Typography id="modal-modal-title" variant="h6" component="h5" >
              ส่วนสูง
            </Typography>
              <p>{user.high}</p>
            </MDBox>
            <MDBox pt={3} px={5} md={6}>
            <Typography id="modal-modal-title" variant="h6" component="h5" >
              BMI
            </Typography>
              <p>{user.bmi}</p>
            <Typography id="modal-modal-title" variant="h6" component="h5" >
              ความดันโลหิต
            </Typography>
              <p>{user.blood}</p>
            </MDBox>
            <MDBox pt={3} px={5} md={6}>
            <Typography id="modal-modal-title" variant="h6" component="h5" >
              ชีพจร
            </Typography>
              <p>{user.blood2}</p>
            <Typography id="modal-modal-title" variant="h6" component="h5" >
              อุณหภูมิร่างกาย
            </Typography>
              <p>{user.temp}</p>
            </MDBox>
            <MDBox pt={3} px={5} md={6}>
            <Typography id="modal-modal-title" variant="h6" component="h5" >
              ระดับออกซิเจนปลายนิ้ว
            </Typography>
              <p>{user.o2}</p>
            </MDBox>
          </Grid>
        </Grid>
      )
  } 



  const navigate = useNavigate();
  const routeChange = () => {
    const path = "/tables";
    navigate(path);
  };

  return (
    <DashboardLayout>
      <DashboardNavbar />
      <MDBox pb={3}>
        <Grid container spacing={6}>
        <Grid item sm={3} xs={12} />
          <Grid item sm={6} xs={12} mt={3}>
            <Card>
              <MDBox
                mx={0}
                mt={0}
                py={3}
                px={1}
                variant="contained"
                bgColor="light"
                coloredShadow="info"
              >
                <MDTypography variant="h6" color="back">
                  จัดการข้อมูลผู้ใช้
                </MDTypography>
              </MDBox>
              {dataInput}
              <Grid container my={1} py={1}>
                <Grid item sm={12} xs={12} align="center">
                    <MDButton variant="gradient" color="secondary" onClick={routeChange}>
                    กลับ
                    </MDButton>
                    <MDButton
                    variant="gradient"
                    color="primary"
                    style={{ marginLeft: "2%" }}
                    onClick={routeChange}
                    >
                    บันทึก
                    </MDButton>
                </Grid>
              </Grid>
            </Card>
          </Grid>
        </Grid>
      </MDBox>
      <Footer />
    </DashboardLayout>
  );
}
export default EditData;
