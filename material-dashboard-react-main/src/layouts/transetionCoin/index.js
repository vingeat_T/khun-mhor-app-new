import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import React from "react";
import { useNavigate } from "react-router-dom";
import { useBetween } from "use-between";

// Material Dashboard 2 React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import MDButton from "components/MDButton";
import Icon from "@mui/material/Icon";
import MDInput from "components/MDInput"

import Footer from 'examples/Footer'

// Material Dashboard 2 React example components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import DataTable from "examples/Tables/DataTable";
import searchtable from "layouts/transetionCoin/data/searchtable";
// Data
import authorsTableData from "layouts/transetionCoin/data/dataCoin";

function Tables() {
  const { columns, rows } = authorsTableData();
  const { search,setValue } = useBetween(searchtable);

  const navigate = useNavigate();
  const routeChange = () => {
    // const path = "/tables/editdata/create?id=115";
    const path = "/search";
    navigate(path);
  };
  

  return (
    <DashboardLayout>
      <DashboardNavbar />
      <MDBox pb={3}>
        <Grid container spacing={6}>
          <Grid item xs={12}>
            <Grid container mx={2} mt={1} py={1} pr={3}>
            <Grid item sm={6} xs={6} align="left">
                <MDInput
                type="text"
                label="Search"
                value= {search}
                onChange={e => setValue(e.target.value)}
                style={{ width: "30%"}}
                />
                {/* <MDButton variant="gradient" color="success" onClick={clickSearch} /> */}
                <p>{search}</p>
              </Grid>
              <Grid item sm={6} xs={6} align="right" mt={1}>
                <MDButton variant="gradient" color="success" onClick={routeChange} style={{ marginRight: "2%" }}>
                  <Icon>add</Icon>
                  เพิ่ม
                </MDButton>
              </Grid>
            </Grid>
            <Card>
              <MDBox
                mx={2}
                mt={1}
                py={3}
                px={2}
                variant="gradient"
                bgColor="secondary"
                borderRadius="lg"
                coloredShadow="info"
              >
                <MDTypography variant="h6" color="white">
                  ประวัติการใช้/รับเหรียญ
                </MDTypography>
              </MDBox>
              <MDBox pt={3}>
                <DataTable
                  table={{ columns, rows }}
                  isSorted={false}
                  entriesPerPage={false}
                  showTotalEntries={false}
                  noEndBorder
                />
              </MDBox>
            </Card>
          </Grid>
        </Grid>
      </MDBox>
      <Footer />
    </DashboardLayout>
  );
}

export default Tables;