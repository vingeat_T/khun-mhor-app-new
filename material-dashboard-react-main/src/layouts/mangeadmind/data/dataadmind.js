/* eslint-disable react/prop-types */

// Soft UI Dashboard React components
import { useState } from "react";
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import MDAvatar from "components/MDAvatar";
import MDBadge from "components/MDBadge";
import Grid from "@mui/material/Grid";
import MDButton from "components/MDButton";
import Icon from "@mui/material/Icon";
import * as React from "react";
import MDSnackbar from "components/MDSnackbar";
import { useNavigate } from "react-router-dom";
// import { red } from '@mui/material/colors';
// const color = red[500];
// Images
// import logoXD from "assets/images/small-logos/logo-xd.svg";
// import logoAtlassian from "assets/images/small-logos/logo-atlassian.svg";
// import logoSlack from "assets/images/small-logos/logo-slack.svg";
// import logoSpotify from "assets/images/small-logos/logo-spotify.svg";
// import logoJira from "assets/images/small-logos/logo-jira.svg";
// import logoInvesion from "assets/images/small-logos/logo-invision.svg";
// import team2 from "assets/images/team-2.jpg";
// import team3 from "assets/images/team-3.jpg";
// import team4 from "assets/images/team-4.jpg";

import lv1 from "assets/images/lv1.png";
import lv2 from "assets/images/lv2.png";
import lv3 from "assets/images/lv3.png";
import lv4 from "assets/images/lv4.png";
import lv5 from "assets/images/lv5.png";

export default function data() {
  const Author = ({ image, name }) => (
    <MDBox display="flex" alignItems="center" lineHeight={1}>
      <MDAvatar src={image} name={name} variant="rounded" size="lg" />
      <MDBox ml={2} lineHeight={1}>
        <MDTypography display="block" variant="button" fontWeight="medium">
          {name}
        </MDTypography>
      </MDBox>
    </MDBox>
  );
  const navigate = useNavigate();
  const routeChange = () => {
    const path = "/mangeadmin/editdata/create";
    navigate(path);
  };

  const [successSB, setSuccessSB] = useState(false);
  const [deleteSB, setDeleteSB] = useState(false);

  // const openSuccessSB = () => setSuccessSB(true);
  const openDelete = () => setDeleteSB(true);
  const closeSuccessSB = () => setSuccessSB(false);
  const closeDeleteSB = () => setDeleteSB(false);

  const renderSuccessSB = (
    <MDSnackbar
      color="warning"
      icon="check"
      title="Console Dashboard"
      content="This is a data demo can't modify"
      dateTime="11 mins ago"
      open={successSB}
      onClose={closeSuccessSB}
      close={closeSuccessSB}
      bgWhite
    />
  );

  const renderDelete = (
    <MDSnackbar
      color="error"
      icon="check"
      title="Console Dashboard"
      content="This is a data demo can't delete"
      dateTime="11 mins ago"
      open={deleteSB}
      onClose={closeDeleteSB}
      close={closeDeleteSB}
      bgWhite
    />
  );

  // const Job = ({ title, description }) => (
  //   <MDBox lineHeight={1} textAlign="left">
  //     <MDTypography display="block" variant="caption" color="text" fontWeight="medium">
  //       {title}
  //     </MDTypography>
  //     <MDTypography variant="caption">{description}</MDTypography>
  //   </MDBox>
  // );

  const dataColumns = [
      { Header: "ตำแหน่ง", accessor: "author", width: "30%", align: "left" },
      { Header: "จำนวนสมาชิก", accessor: "status", align: "center" },
      { Header: "แก้ไขล่าสุด", accessor: "employed", align: "center" },
      { Header: "แก้ไข", accessor: "action", align: "center" },
    ]

  const dataRows = [
    {image: lv1, name: "Junior admin", status: "10", employed: "23/04/18"},
    {image: lv2, name: "Senior admin", status: "8", employed: "11/01/19"},
    {image: lv3, name: "Specialist", status: "5", employed: "19/09/17"},
    {image: lv4, name: "Manager", status: "4", employed: "24/12/08"},
    {image: lv5, name: "Creator", status: "1", employed: "04/10/21"}
  ]

  const setRowArray = []
  if (dataRows) {
    dataRows.forEach((val) => {
      const renderRow = {
        author: (
          <Author image={val.image} name={val.name} />
        ),
        status: (
          <MDBox ml={-1}>
            <MDBadge badgeContent={val.status} color="success" variant="gradient" size="md" />
          </MDBox>
        ),
        employed: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            {val.employed}
          </MDTypography>
        ),
        action: (
          <MDBox ml={2} pt={2}>
            <Grid container spacing={2}>
              <MDButton variant="gradient" color="warning" onClick={routeChange}>
                <Icon>edit</Icon>
                แก้ไข
              </MDButton>
              <MDButton variant="gradient" color="error" onClick={openDelete}>
                <Icon>delete</Icon>
                ลบ
              </MDButton>
              {renderSuccessSB}
              {renderDelete}
            </Grid>
          </MDBox>
        )
      }
      setRowArray.push(renderRow)
    })
  }


  return {

    columns: dataColumns,

    rows: setRowArray

    // columns: [
    //   { Header: "ตำแหน่ง", accessor: "author", width: "30%", align: "left" },
    //   { Header: "จำนวนสมาชิก", accessor: "status", align: "center" },
    //   { Header: "แก้ไขล่าสุด", accessor: "employed", align: "center" },
    //   { Header: "แก้ไข", accessor: "action", align: "center" },
    // ],

    // rows: [
    //   {
    //     author: (
    //       <Author
    //         image={lv1}
    //         name="junior admin"
    //         label="project #1"
    //         title="scandinavian"
    //       />
    //     ),
    //     function: <Job title="Manager" description="Organization" />,
    //     status: (
    //       <MDBox ml={-1}>
    //         <MDBadge badgeContent="10" color="success" variant="gradient" size="md" />
    //       </MDBox>
    //     ),
    //     rank: (
    //         <MDBox ml={-1}>
    //             <MDButton style={{ marginTop: "-12%" }} variant="gradient" color="dark" onClick={routeChange}>
    //                 <Icon>edit</Icon>
    //                 editmember
    //             </MDButton>
    //         </MDBox>
    //     ),
    //     employed: (
    //       <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
    //         23/04/18
    //       </MDTypography>
    //     ),
    //     action: (
    //       <MDBox ml={-1}>
    //         <Grid container spacing={2}>
    //           <MDButton variant="gradient" color="warning" onClick={routeChange}>
    //             <Icon>edit</Icon>
    //             แก้ไข
    //           </MDButton>
    //           <MDButton variant="gradient" color="error" onClick={openDelete}>
    //             <Icon>delete</Icon>
    //             ลบ
    //           </MDButton>
    //           {renderSuccessSB}
    //           {renderDelete}
    //         </Grid>
    //       </MDBox>
    //     ),
    //   },
    //   {
    //     author: <Author image={lv2} name="senior admin" />,
    //     function: <Job title="Programator" description="Developer" />,
    //     status: (
    //       <MDBox ml={-1}>
    //         <MDBadge badgeContent="8" color="success" variant="gradient" size="md" />
    //       </MDBox>
    //     ),
    //     rank: (
    //         <MDBox ml={-1}>
    //             <MDButton style={{ marginTop: "-12%" }}  variant="gradient" color="dark" onClick={routeChange}>
    //                 <Icon>edit</Icon>
    //                 editmember
    //             </MDButton>
    //         </MDBox>
    //     ),
    //     employed: (
    //       <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
    //         11/01/19
    //       </MDTypography>
    //     ),
    //     action: (
    //       <MDBox ml={-1}>
    //         <Grid container spacing={2}>
    //           <MDButton variant="gradient" color="warning" onClick={routeChange}>
    //             <Icon>edit</Icon>
    //             แก้ไข
    //           </MDButton>
    //           <MDButton variant="gradient" color="error" onClick={openDelete}>
    //             <Icon>delete</Icon>
    //             ลบ
    //           </MDButton>
    //           {renderSuccessSB}
    //           {renderDelete}
    //         </Grid>
    //       </MDBox>
    //     ),
    //   },
    //   {
    //     author: <Author image={lv3} name="specialist" />,
    //     function: <Job title="Executive" description="Projects" />,
    //     status: (
    //       <MDBox ml={-1}>
    //         <MDBadge badgeContent="5" color="success" variant="gradient" size="md" />
    //       </MDBox>
    //     ),
    //     rank: (
    //         <MDBox ml={-1}>
    //             <MDButton style={{ marginTop: "-12%" }} variant="gradient" color="dark" onClick={routeChange}>
    //                 <Icon>edit</Icon>
    //                 editmember
    //             </MDButton>
    //         </MDBox>
    //     ),
    //     employed: (
    //       <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
    //         19/09/17
    //       </MDTypography>
    //     ),
    //     action: (
    //       <MDBox ml={-1}>
    //         <Grid container spacing={2}>
    //           <MDButton variant="gradient" color="warning" onClick={routeChange}>
    //             <Icon>edit</Icon>
    //             แก้ไข
    //           </MDButton>
    //           <MDButton variant="gradient" color="error" onClick={openDelete}>
    //             <Icon>delete</Icon>
    //             ลบ
    //           </MDButton>
    //           {renderSuccessSB}
    //           {renderDelete}
    //         </Grid>
    //       </MDBox>
    //     ),
    //   },
    //   {
    //     author: <Author image={lv4} name="manager" />,
    //     function: <Job title="Programator" description="Developer" />,
    //     status: (
    //       <MDBox ml={-1}>
    //         <MDBadge badgeContent="4" color="success" variant="gradient" size="md" />
    //       </MDBox>
    //     ),
    //     rank: (
    //         <MDBox ml={-1}>
    //             <MDButton style={{ marginTop: "-12%" }} variant="gradient" color="dark" onClick={routeChange}>
    //                 <Icon>edit</Icon>
    //                 editmember
    //             </MDButton>
    //         </MDBox>
    //     ),
    //     employed: (
    //       <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
    //         24/12/08
    //       </MDTypography>
    //     ),
    //     action: (
    //       <MDBox ml={-1}>
    //         <Grid container spacing={2}>
    //           <MDButton variant="gradient" color="warning" onClick={routeChange}>
    //             <Icon>edit</Icon>
    //             แก้ไข
    //           </MDButton>
    //           <MDButton variant="gradient" color="error" onClick={openDelete}>
    //             <Icon>delete</Icon>
    //             ลบ
    //           </MDButton>
    //           {renderSuccessSB}
    //           {renderDelete}
    //         </Grid>
    //       </MDBox>
    //     ),
    //   },
    //   {
    //     author: <Author image={lv5} name="creator"/>,
    //     function: <Job title="Manager" description="Executive" />,
    //     status: (
    //       <MDBox ml={-1}>
    //         <MDBadge badgeContent="1" color="success" variant="gradient" size="md" />
    //       </MDBox>
    //     ),
    //     employed: (
    //       <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
    //         04/10/21
    //       </MDTypography>
    //     ),
    //     rank: (
    //         <MDBox ml={-1}>
    //             <MDButton style={{ marginTop: "-12%" }} variant="gradient" color="dark" onClick={routeChange}>
    //                 <Icon>edit</Icon>
    //                 editmember
    //             </MDButton>
    //         </MDBox>
    //     ),
    //     action: (
    //       <MDBox ml={-1}>
    //         <Grid container spacing={2}>
    //           <MDButton variant="gradient" color="warning" onClick={routeChange}>
    //             <Icon>edit</Icon>
    //             แก้ไข
    //           </MDButton>
    //           <MDButton variant="gradient" color="error" onClick={openDelete}>
    //             <Icon>delete</Icon>
    //             ลบ
    //           </MDButton>
    //           {renderSuccessSB}
    //           {renderDelete}
    //         </Grid>
    //       </MDBox>
    //     ),
    //   },
    // ],
  };
}
