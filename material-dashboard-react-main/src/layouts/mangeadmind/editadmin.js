// import React from "react";
// import MDInput from "components/MDInput";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import MDAvatar from "components/MDAvatar";
// import MDButton from "components/MDButton";
import { useSearchParams } from "react-router-dom";
// import team2 from "assets/images/team-2.jpg";
import blank from "assets/images/blankpicture.png";
import * as React from 'react';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
// import Icon from "@mui/material/Icon";
import lv1 from "assets/images/lv1.png";

// prop-types is a library for typechecking of props.
// import PropTypes from "prop-types";
import Box from '@mui/material/Box';
import Checkbox from '@mui/material/Checkbox';
import FormControlLabel from '@mui/material/FormControlLabel';
import CheckForm from 'layouts/mangeadmind/components/checkForm'
import CheckMore from 'layouts/mangeadmind/components/checkMore'
import ButtonForm from 'layouts/mangeadmind/components/buttonForEdit/buttonForm'
// import { Dropdown, Menu } from "semantic-ui-react";
// import MDBadge from "components/MDBadge";

import Footer from 'examples/Footer'

function EditAdmin() {
  const [searchParams] = useSearchParams();
  const id = searchParams.get("id")
  const [age, setAge] = React.useState('');
  const handleChange = (event) => {
    setAge(event.target.value);
  };

  console.log(id);
  let user = {
    name: "Junior admin",
    email: "john@smith.com",
    select: "null",
    coin: "10000",
    userName: "Vingeat",
    tel: "0895477651",
    image: lv1
  }
  let Testtax = (<MDTypography variant="h6" color="back">ต่ำแหน่ง {user.name}</MDTypography>)
  if (id) {
      user = {
        name: null,
        email: null,
        select: null,
        coin: "0",
        userName: null,
        tel: null,
        image: blank

      }
    Testtax = (<Box>
      <FormControl sx = {{width: "30%"}} >
        <InputLabel id="demo-simple-select-label" >เลือกตำแหน่ง</InputLabel>
        <Select
          style = {{height: 45}}
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={age}
          label="Age"
          onChange={handleChange}
        >
          <MenuItem value={10}>Junior admin</MenuItem>
          <MenuItem value={20}>Senior admin</MenuItem>
          <MenuItem value={30}>Specialist</MenuItem>
          <MenuItem value={30}>Manager</MenuItem>
          <MenuItem value={30}>Creator</MenuItem>
        </Select>
      </FormControl>
    </Box>
    );
  } 

  // const navigate = useNavigate();
  // const routeChange = () => {
  //   const path = "/mangeadmind";
  //   navigate(path);
  // };

  // const [checked, setChecked] = React.useState([true, false]);

  // const handleChange1 = (event) => {
  //   setChecked([event.target.checked, event.target.checked]);
  // };

  // const handleChange2 = (event) => {
  //   setChecked([event.target.checked, checked[1]]);
  // };

  // const handleChange3 = (event) => {
  //   setChecked([checked[0], event.target.checked]);
  // };

  // const children = (
  //   <Box sx={{ display: 'flex', flexDirection: 'column', ml: 3 }}>
  //     <FormControlLabel
  //       label="เพิ่ม"
  //       control={<Checkbox checked={checked[0]} onChange={handleChange2} />}
  //     />
  //     <FormControlLabel
  //       label="แก้ไข"
  //       control={<Checkbox checked={checked[1]} onChange={handleChange3} />}
  //     />
  //     <FormControlLabel
  //       label="ลบ"
  //       control={<Checkbox checked={checked[1]} onChange={handleChange3} />}
  //     />
  //   </Box>
  // );

  // const children1 = (
  //   <Box sx={{ display: 'flex', flexDirection: 'column', ml: 3 }}>
  //     <FormControlLabel
  //       label="แก้ไข"
  //       control={<Checkbox checked={checked[0]} onChange={handleChange2} />}
  //     />
  //     <FormControlLabel
  //       label="ลบ"
  //       control={<Checkbox checked={checked[1]} onChange={handleChange3} />}
  //     />
  //   </Box>
  // )

  // const [age, setAge] = React.useState('');

  // const handleChange = (event) => {
  //   setAge(event.target.value);
  // };

  return (
    <DashboardLayout>
      <DashboardNavbar />
      <MDBox mx={5} ml={2} my={3}>
        <Grid container spacing={0} width="210%">
        {/* <Grid item sm={3} xs={12} /> */}
          <Grid item sm={6} xs={12}>
            <Card>
              <MDBox
                mx={0}
                mt={0}
                py={2}
                px={2}
                variant="contained"
                bgColor="light"
                coloredShadow="info"
              >
                <MDTypography variant="h6" color="back">
                  จัดการสิทธิ Admin
                </MDTypography>
              </MDBox>
              <MDBox pt={3} px={5}>
                <MDAvatar src={user.image} alt="Avatar" variant="square" size="lg" />
              </MDBox>
              <MDBox pt={3} px={5}>
                {Testtax}
              </MDBox>
              <MDBox my={2} mx={3} bgColor="light">
                <MDBox
                  py={1}
                  fullWidth
                  bgColor="light"
                  coloredShadow="dark"
                >
                    <MDTypography px={2} variant="h6" color="back">เลือกสิทธิ</MDTypography>
                </MDBox>
                  <Grid container mt={1} py={1}>
                    
                    <CheckForm type="สิทธิจัดการหมอ" />
                    <CheckForm type="สิทธิจัดการผู้ใช้ทั่วไป" />
                    <CheckForm type="สิทธิจัดการแบนเนอร์" />
                    <CheckForm type="สิทธิจัดการถามฟรี" />

                      {/* <Grid item sm={1} xs={1} px={3}/> */}
                      
                  </Grid>
                  
                  <Grid container mt={1} py={1}>
                    <CheckMore type="สิทธิจัดการรีวิว"/>
                    <CheckMore type="สิทธิจัดการรายได้"/>
                    <CheckMore type="สิทธิจัดการถามรอ"/>
                    <CheckMore type="สิทธิจัดการถามทันที"/>

                      {/* <Grid item sm={1} xs={1} px={5}/> */}
                      
                  </Grid>
                  
                  <Grid container mt={1} py={1}>
                    <Grid item sm={5} xs={5} px={5}>
                        <FormControlLabel pt={5} control={<Checkbox defaultChecked />} label="สิทธิจัดการเอกสาร" />
                      </Grid>
                      <Grid item sm={1} xs={1} px={5}/>
                      <Grid item sm={5} xs={5} px={5}>
                        <FormControlLabel pt={5} control={<Checkbox defaultChecked />} label="สิทธิจัดการแจ้งเตือน" />
                    </Grid>
                  </Grid>
              </MDBox>

              <ButtonForm />

            </Card>
          </Grid>
        </Grid>
      </MDBox>
      <Footer />
    </DashboardLayout>
  );
}

// // Setting default props for the EditAdmin
// EditAdmin.defaultProps = {
//   name: "",
// };

// // Typechecking props for the EditAdmin
// EditAdmin.propTypes = {
//   name: PropTypes.node,
// };
// // Setting default props for the EditAdmin
// EditAdmin.defaultProps = {
//   image: "",
// };

// // Typechecking props for the EditAdmin
// EditAdmin.propTypes = {
//   image: PropTypes.node,
// };

export default EditAdmin;
