import React from 'react'
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
// prop-types is a library for typechecking of props.
import PropTypes from "prop-types";

import FormControlLabel from '@mui/material/FormControlLabel';

import Checkbox from '@mui/material/Checkbox';

function CheckForm({ type }) {

    const [checked, setChecked] = React.useState([false, false, false]);

    const handleChange1 = (event) => {
        setChecked([event.target.checked, event.target.checked, event.target.checked]);
    };

    const handleChange2 = (event) => {
        setChecked([event.target.checked, checked[1], checked[2]]);
    };

    const handleChange3 = (event) => {
        setChecked([checked[0], event.target.checked, checked[2]]);
    };

    const handleChange4 = (event) => {
        setChecked([checked[0], checked[1], event.target.checked]);
    };

    const children = (
        <Box sx={{ display: 'flex', flexDirection: 'column', ml: 3 }}>
          <FormControlLabel
            label="เพิ่ม"
            control={<Checkbox checked={checked[0]} onChange={handleChange2} />}
          />
          <FormControlLabel
            label="แก้ไข"
            control={<Checkbox checked={checked[1]} onChange={handleChange3} />}
          />
          <FormControlLabel
            label="ลบ"
            control={<Checkbox checked={checked[2]} onChange={handleChange4} />}
          />
        </Box>
      );

  return (
    <Grid item xs px={5}>
        <FormControlLabel
            label={type}
            control={
            <Checkbox
                checked={checked[0] && checked[1] && checked[2]}
                indeterminate={checked[0] !== checked[1]}
                onChange={handleChange1}
            />
            }
        />
        {children}
        </Grid>
  )
}

// Setting default props for the CheckForm
CheckForm.defaultProps = {
    type: "",
  };
  
  // Typechecking props for the CheckForm
  CheckForm.propTypes = {
    type: PropTypes.node,
  };

export default CheckForm