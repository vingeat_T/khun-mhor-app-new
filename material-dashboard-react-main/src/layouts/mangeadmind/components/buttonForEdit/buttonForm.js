import React from 'react'
import { useNavigate } from "react-router-dom";

import Grid from "@mui/material/Grid";

import MDButton from "components/MDButton";

function ButtonForm() {

    const navigate = useNavigate();
    const routeChange = () => {
        const path = "/mangeadmind";
        navigate(path);
    };

  return (
    <Grid container py={0} pb={2}>
        <Grid item sm={12} xs={12} align="center">
            <MDButton variant="gradient" color="secondary" onClick={routeChange}>
                กลับ
            </MDButton>
            <MDButton
            variant="gradient"
            color="success"
            style={{ marginLeft: "2%" }}
            onClick={routeChange}
            >
                บันทึก
            </MDButton>
        </Grid>
    </Grid>
  )
}

export default ButtonForm