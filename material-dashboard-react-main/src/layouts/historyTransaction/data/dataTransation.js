/* eslint-disable react/prop-types */

// Soft UI Dashboard React components
import { useState } from "react";
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
// import MDAvatar from "components/MDAvatar";
import MDBadge from "components/MDBadge";
import Grid from "@mui/material/Grid";
import MDButton from "components/MDButton";
// import Icon from "@mui/material/Icon";
import * as React from "react";
import MDSnackbar from "components/MDSnackbar";
import { useNavigate,useSearchParams } from "react-router-dom";
import VisibilityIcon from '@mui/icons-material/Visibility';
// Images
// import logoXD from "assets/images/small-logos/logo-xd.svg";
// import logoAtlassian from "assets/images/small-logos/logo-atlassian.svg";
// import logoSlack from "assets/images/small-logos/logo-slack.svg";
// import logoSpotify from "assets/images/small-logos/logo-spotify.svg";
// import logoJira from "assets/images/small-logos/logo-jira.svg";
// import logoInvesion from "assets/images/small-logos/logo-invision.svg";
// import team2 from "assets/images/team-2.jpg";
// import team3 from "assets/images/team-3.jpg";
// import team4 from "assets/images/team-4.jpg";

export default function data() {
    // const Author = ({ image, name, email }) => (
    //     <MDBox display="flex" alignItems="center" lineHeight={1}>
    //         <MDAvatar src={image} name={name} size="sm" />
    //         <MDBox ml={2} lineHeight={1}>
    //             <MDTypography display="block" variant="button" fontWeight="medium">
    //                 {name}
    //             </MDTypography>
    //             <MDTypography variant="caption">{email}</MDTypography>
    //         </MDBox>
    //     </MDBox>
    // );
    const navigate = useNavigate();
    const routeChange = () => {
        const path = "/editdatatransation?id=120";
        navigate(path);
    };
    const [searchParams] = useSearchParams();
    const id = searchParams.get("id")
    console.log('45------------',searchParams);
    console.log('46------------',id);
    let dataTransation
    let dataCalumn 
    if (id === '120') {

        dataTransation = [{ Id: "156789", Name: "John", Doctor: "หมอ ใหญ่", Type: "ถามทันที", Date: "2022-01-11", Status: "Success", Symptom: "เจ็บคอ" },
        { Id: "156762", Name: "John", Doctor: "หมอ ใหม่", Type: "ถามทันที", Date: "2022-02-12", Status: "Cancle", Symptom: "ปวดหัว"  },
        { Id: "156715", Name: "John", Doctor: "หมอ ให้", Type: "ถามฟรี", Date: "2022-01-12", Status: "Success", Symptom: "มีไข้"  },
        { Id: "156797", Name: "John", Doctor: "หมอ ใภ้", Type: "ถามทันที", Date: "2022-01-12", Status: "Pending", Symptom: "เวียนหัว"  },
        { Id: "156767", Name: "John", Doctor: "หมอ ใช้", Type: "ถามรอ", Date: "2022-01-12", Status: "Success", Symptom: "ไอแห้ง"  },
        { Id: "156731", Name: "John", Doctor: "หมอ ใผ่", Type: "ถามทันที", Date: "2022-01-12", Status: "Cancle", Symptom: "ท้องเสีย"  },
        { Id: "156719", Name: "John", Doctor: "หมอ ใจ", Type: "ถามรอ", Date: "2022-01-12", Status: "Success", Symptom: "ปวดท้อง"  },
        { Id: "156742", Name: "John", Doctor: "หมอ ใส่", Type: "ถามรอ", Date: "2022-01-12", Status: "Success", Symptom: "เจ็บคอ"  },
        { Id: "156798", Name: "John", Doctor: "หมอ ใหล", Type: "ถามฟรี", Date: "2022-01-12", Status: "Pending", Symptom: "ปวดหลัง"  },
        { Id: "156773", Name: "John", Doctor: "หมอ ใคร", Type: "ถามฟรี", Date: "2022-01-12", Status: "Pending", Symptom: "ไม่ได้กลิ่น"  },
        { Id: "156794", Name: "John", Doctor: "หมอ ใบ้", Type: "ถามรอ", Date: "2022-01-12", Status: "Cancle", Symptom: "ไอแห้ง"  },
        { Id: "156764", Name: "John", Doctor: "หมอ ใบ", Type: "ถามฟรี", Date: "2022-01-12", Status: "Sucess", Symptom: "เจ็บคอ"  }]
        

        dataCalumn =  [
            { Header: "IdTransaction", accessor: "number", align: "left" },
            { Header: "ชื่อ", accessor: "name", align: "center" },
            { Header: "หมอที่รับผิดชอบ", accessor: "doctor", align: "center" },
            { Header: "ประเภทการใช้งาน", accessor: "type", align: "center" },
            { Header: "วันที่", accessor: "date", align: "center" },
            { Header: "สถานะ", accessor: "status", align: "center" },
            { Header: "อาการ", accessor: "symptom", align: "center" },
        ]
    }
    else {
        dataTransation = [{ Id: "156789", Name: "John", Doctor: "หมอ ใหญ่", Type: "ถามทันที", Date: "2022-01-11", Amoute: "120$", Status: "Success" },
        { Id: "156762", Name: "ธน", Doctor: "หมอ ใหม่", Type: "ถามทันที", Date: "2022-02-12", Amoute: "120$", Status: "Cancle" },
        { Id: "156715", Name: "รอน", Doctor: "หมอ ให้", Type: "ถามฟรี", Date: "2022-01-12", Amoute: "0$", Status: "Success" },
        { Id: "156797", Name: "มอณ", Doctor: "หมอ ใภ้", Type: "ถามทันที", Date: "2022-01-12", Amoute: "120$", Status: "Pending" },
        { Id: "156767", Name: "ยอน", Doctor: "หมอ ใช้", Type: "ถามรอ", Date: "2022-01-12", Amoute: "120$", Status: "Success" },
        { Id: "156731", Name: "บอน", Doctor: "หมอ ใผ่", Type: "ถามทันที", Date: "2022-01-12", Amoute: "120$", Status: "Cancle" },
        { Id: "156719", Name: "ชอน", Doctor: "หมอ ใจ", Type: "ถามรอ", Date: "2022-01-12", Amoute: "120$", Status: "Success" },
        { Id: "156742", Name: "ถอน", Doctor: "หมอ ใส่", Type: "ถามรอ", Date: "2022-01-12", Amoute: "120$", Status: "Success" },
        { Id: "156798", Name: "ออน", Doctor: "หมอ ใหล", Type: "ถามฟรี", Date: "2022-01-12", Amoute: "0$", Status: "Pending" },
        { Id: "156773", Name: "ฟอน", Doctor: "หมอ ใคร", Type: "ถามฟรี", Date: "2022-01-12", Amoute: "0$", Status: "Pending" },
        { Id: "156794", Name: "สอน", Doctor: "หมอ ใบ้", Type: "ถามรอ", Date: "2022-01-12", Amoute: "120$", Status: "Cancle" },
        { Id: "156764", Name: "นอน", Doctor: "หมอ ใบ", Type: "ถามฟรี", Date: "2022-01-12", Amoute: "0$", Status: "Sucess" }]

    dataCalumn =  [
        { Header: "IdTransaction", accessor: "number", align: "left" },
        { Header: "ชื่อ", accessor: "name", align: "center" },
        { Header: "หมอที่รับผิดชอบ", accessor: "doctor", align: "center" },
        { Header: "ประเภทการใช้งาน", accessor: "type", align: "center" },
        { Header: "วันที่", accessor: "date", align: "center" },
        { Header: "สถานะ", accessor: "status", align: "center" },
        { Header: "แก้ไข", accessor: "action", align: "center" },
    ]
    }

    const [successSB, setSuccessSB] = useState(false);
    const [deleteSB, setDeleteSB] = useState(false);

    // const openSuccessSB = () => setSuccessSB(true);
    // const openDelete = () => setDeleteSB(true);
    const closeSuccessSB = () => setSuccessSB(false);
    const closeDeleteSB = () => setDeleteSB(false);

    const renderSuccessSB = (
        <MDSnackbar
            color="warning"
            icon="check"
            title="Console Dashboard"
            content="This is a data demo can't modify"
            dateTime="11 mins ago"
            open={successSB}
            onClose={closeSuccessSB}
            close={closeSuccessSB}
            bgWhite
        />
    );

    const renderDelete = (
        <MDSnackbar
            color="error"
            icon="check"
            title="Console Dashboard"
            content="This is a data demo can't delete"
            dateTime="11 mins ago"
            open={deleteSB}
            onClose={closeDeleteSB}
            close={closeDeleteSB}
            bgWhite
        />
    );

    const setRowArray = []
    if (dataTransation) {
        dataTransation.forEach((item) => { 
            let colorSet 
            if (item.Status==='Success') {
                colorSet = 'success'
            } else if (item.Status==='Cancle') {
                colorSet = 'error'
            } else if (item.Status==='Pending') {
                colorSet = 'warning'
            }
            const renderRow = {
                number: (
                    <MDTypography component="a" href="#" variant="h6" color="text" fontWeight="medium">
                        {item.Id}
                    </MDTypography>
                ),
                name: (
                    <MDTypography component="a" href="#" variant="h6" color="text" fontWeight="medium">
                        {item.Name}
                    </MDTypography>
                ),
                doctor: (
                    <MDTypography component="a" href="#" variant="h6" color="text" fontWeight="medium">
                        {item.Doctor}
                    </MDTypography>
                ),
                symptom: (
                    <MDTypography component="a" href="#" variant="h6" color="text" fontWeight="medium">
                        {item.Symptom}
                    </MDTypography>
                ),
                type: (
                    <MDTypography component="a" href="#" variant="h6" color="text" fontWeight="medium">
                        {item.Type}
                    </MDTypography>
                ),
                date: (
                    <MDTypography component="a" href="#" variant="h6" color="text" fontWeight="medium">
                        {item.Date}
                    </MDTypography>
                ),
                amoute: (
                    <MDTypography component="a" href="#" variant="h6" color="text" fontWeight="medium">
                        {item.Amoute}
                    </MDTypography>
                ),
                status: (
                    <MDBadge badgeContent= {item.Status} size="xs" color={colorSet} container />
                ),
                action: (
                    <MDBox ml={2} pt={2}>
                        <Grid container spacing={2}>
                            <MDButton variant="gradient" color="info" startIcon={<VisibilityIcon />} onClick={routeChange}>
                                ดูเพิ่มเติม
                            </MDButton>
                            {renderSuccessSB}
                            {renderDelete}
                        </Grid>
                    </MDBox>
                ),
            }
            setRowArray.push(renderRow)
        })
    }
    console.log('151', setRowArray);
    // const test = ([{
    //     number: (
    //         <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
    //             {dataTransation.Id}
    //         </MDTypography>
    //     )}])

    return {
        columns: dataCalumn,

        rows: setRowArray 

    };
}
