import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import React from "react";
import { useNavigate } from "react-router-dom";

// Material Dashboard 2 React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import MDButton from "components/MDButton";
// import Icon from "@mui/material/Icon";

// Material Dashboard 2 React example components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import DataTable from "examples/Tables/DataTable";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
// Data
import authorsTableData from "layouts/historyTransaction/data/dataTransation";
import team2 from "assets/images/team-2.jpg";
import { Image } from "semantic-ui-react";

function createDataInRow() {
  const { columns, rows } = authorsTableData();

  const navigate = useNavigate();
  const routeChange = () => {
    const path = "/history?id=115";
    navigate(path);
  };

  return (
    <DashboardLayout>
      <DashboardNavbar />
      <MDBox pt={6} pb={3}>
        <Grid container spacing={6}>
          <Grid item xs={12}>
            <Grid container mx={2} mt={1} py={1} px={2}>
              <Grid item sm={12} xs={12} align="right">
                <MDButton variant="gradient" color="success" onClick={routeChange} style={{ marginRight: "2%" }}>
                  <ArrowBackIcon/>
                  กลับ
                </MDButton>
              </Grid>
            </Grid>
            <Card>
              <MDBox
                mx={2}
                mt={1}
                py={3}
                px={2}
                variant="gradient"
                bgColor="secondary"
                borderRadius="lg"
                coloredShadow="info"
              >
                <MDTypography variant="h6" color="white">
                John
                </MDTypography>
              </MDBox>
              <Grid container mx={2} mt={1} py={1} px={2}>
              <Grid item sm={3} xs={3} />
              <Grid item sm={1.5} xs={1.5}>
                  <MDBox  style={{ marginLeft: "1%",marginTop: "-1%" }}>  
                      <Image src={team2} alt="profile-image" width="120px" height="120px" />
                  </MDBox>
              </Grid>
              <Grid item sm={3} xs={3}>
              <MDBox >
              <MDTypography component="a" href="#" variant="h5" color="text" fontWeight="medium">
                ชื่อ John  Michael
              </MDTypography>
              </MDBox>
              <MDBox>
              <MDTypography component="a" href="#" variant="h5" color="text" fontWeight="medium">
                อีเมล john@creative-tim.com
              </MDTypography>
              </MDBox>
              <MDBox>
              <MDTypography component="a" href="#" variant="h6" color="warning" fontWeight="medium">
                จำนวนการใช้เหรียญแบบรายเดือน 356
              </MDTypography>
              </MDBox>
              </Grid>
            </Grid>
              <MDBox pt={2}>
                <DataTable
                  table={{ columns, rows }}
                  isSorted={false}
                  entriesPerPage={false}
                  showTotalEntries={false}
                  noEndBorder
                />
              </MDBox>
            </Card>
          </Grid>
        </Grid>
      </MDBox>
    </DashboardLayout>
  );
}

export default createDataInRow;
