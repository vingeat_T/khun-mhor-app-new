import * as React from "react";

export default function useShareableState() {
  const [DataUser, setDataUser] = React.useState({});
  return {
    DataUser,
    setDataUser
  };
};