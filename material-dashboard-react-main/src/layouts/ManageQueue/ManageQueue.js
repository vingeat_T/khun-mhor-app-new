// import Card from "@mui/material/Card";

// import Grid from "@mui/material/Grid";
// import Card from "@mui/material/Card";
import * as React from 'react';
import Box from "@mui/material/Box";
// import InputLabel from "@mui/material/InputLabel";
// import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
// import Select from '@mui/material/Select';
// import Button from '@mui/material/Button';
// import DeleteIcon from '@mui/icons-material/Delete';
// import { Label } from '@mui/icons-material';

import { Button, Icon } from 'semantic-ui-react'
// import { Card } from '@mui/material';

function ManageQueue() {
  // const [piority, setpiority] = React.useState('');

  // const handleChange = (event) => {
  //   setpiority(event.target.value);
  // };

  return (
    <Box sx={{ minWidth: 120 }}>
      <FormControl fullWidth>
        <Box sx={{ typography: 'h6' }}>ลำดับความสำคัญ:</Box>
        {/* <InputLabel id="demo-simple-select-autowidth-label"> </InputLabel> */}
        {/* <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={piority}
          // label="ความสำคัญ"
          onChange={handleChange}
        >
          <MenuItem value={1}>น้อย</MenuItem>
          <MenuItem value={2}>ปานกลาง</MenuItem>
          <MenuItem value={3}>มาก</MenuItem>
        </Select> */}
        &nbsp;
        <Button.Group>
          <Button basic color='green'>
            น้อย
          </Button>
          <Button basic color='yellow'>
            ปานกลาง
          </Button>
          <Button basic color='red'>
            มาก
          </Button>
        </Button.Group>
        &nbsp;
        
        <Button icon color='red'>ลบ &nbsp;
          <Icon name='trash alternate outline' />
        </Button>
        
      </FormControl>
    </Box>

  );
}
export default ManageQueue;
