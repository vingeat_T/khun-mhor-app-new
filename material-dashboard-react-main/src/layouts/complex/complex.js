
import PropTypes from "prop-types";


import Card from "@mui/material/Card";

import Grid from "@mui/material/Grid";
import LocationOnOutlinedIcon from "@mui/icons-material/LocationOnOutlined";
import SchoolOutlinedIcon from "@mui/icons-material/SchoolOutlined";


import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";

import MDAvatar from "components/MDAvatar";
import { Button, Icon } from "semantic-ui-react";
import Box from "@mui/material/Box";
import { useNavigate } from "react-router-dom";

function Complex({ name, title, hospital, price, review, image, university }) {
  const navigate = useNavigate();
  const routeChange = () => {
    const path = "/statusQueue";
    navigate(path);
  };

  return (
    <Card>
      <MDBox display="flex" justifyContent="space-between" pt={2} px={2}>
        <MDBox textAlign="right" pb={0} px={1} mt={1}>
          <Grid item ml={-1} mt={-1}>
            <MDAvatar src={image} alt="profile-image" size="xl" shadow="sm" variant="rounded" />
          </Grid>
        </MDBox>
      </MDBox>
      <MDBox position="relative" width="100.25%" shadow="xl" borderRadius="xl">
        <Card
          title={title}
          sx={{
            maxWidth: "100%",
            margin: 0,
            boxShadow: ({ boxShadows: { md } }) => md,
            objectFit: "cover",
            objectPosition: "center",
          }}
        />
      </MDBox>
      <MDBox textAlign="right" pb={0} px={2} mt={-8.5}>
        <MDTypography variant="h6" fontWeight="bold" color="dark" textTransform="capitalize">
          {name}
        </MDTypography>
        <MDBox textAlign="right" pb={0} px={0} mt={0}>
          <MDTypography
            variant="button"
            fontWeight="regular"
            color="text"
            textTransform="capitalize"
          >
            {title}
          </MDTypography>
        </MDBox>
        <MDBox textAlign="right" pb={0} px={0} mt={0}>
          <MDTypography
            variant="caption"
            fontWeight="regular"
            color="text"
            textTransform="capitalize"
          >
            <SchoolOutlinedIcon />
            &nbsp;
            {university}
          </MDTypography>
        </MDBox>
        <MDBox textAlign="right" pb={0} px={0} mt={0}>
          <MDTypography variant="caption" fontWeight="regular" color="text">
            <LocationOnOutlinedIcon />
            &nbsp;
            {hospital}
          </MDTypography>
        </MDBox>
        <MDBox textAlign="right" pb={0} px={0} mt={0}>
          <MDTypography variant="body2" fontWeight="bold" color="dark">
            {price}
          </MDTypography>
        </MDBox>
        <MDBox textAlign="left" pb={0} px={0} mt={0}>
          <MDTypography variant="h5" fontWeight="bold" color="text">
            {review}
          </MDTypography>
        </MDBox>
        <MDBox textAlign="left" pb={2} px={0} mt={0}>
          <MDTypography variant="h5" fontWeight="regular" color="text">
            {review}
          </MDTypography>
        </MDBox>
      </MDBox>
      <Box sx={{ minWidth: 120 }}>
        {/* <Button icon color="red">
          ลบ &nbsp;
          <Icon name="trash alternate outline" />
        </Button> */}
        <Button fluid color="blue" onClick={routeChange}>
          ดูคิว &nbsp;
          <Icon name="angle right" />
        </Button>
      </Box>
    </Card>
  );
}

// Setting default values for the props of DefaultProjectCard
Complex.defaultProps = {};

// Typechecking props for the DefaultProjectCard
Complex.propTypes = {
  image: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  university: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  hospital: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  review: PropTypes.string.isRequired,
  action: PropTypes.shape({
    type: PropTypes.oneOf(["external", "internal"]),
    route: PropTypes.string.isRequired,
    color: PropTypes.oneOf([
      "primary",
      "secondary",
      "info",
      "success",
      "warning",
      "error",
      "light",
      "dark",
      "white",
    ]).isRequired,
    label: PropTypes.string.isRequired,
  }).isRequired,
  // icon: PropTypes.node.isRequired,
};

export default Complex;
