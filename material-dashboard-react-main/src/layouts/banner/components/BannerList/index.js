import * as React from 'react';
import Card from "@mui/material/Card";
import Grid from "@mui/material/Grid";
import Icon from "@mui/material/Icon";
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import MDButton from "components/MDButton";
import BannerImg from "layouts/banner/components/BannerImg";
import BannerModal from "layouts/banner/components/BannerModal";

const banners = [
    {
        id: 'slfe9x9h',
        img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSVP80sqQYyXdkNYB_Tdt_RhA-jBskNOCyWKA&usqp=CAU',
        link: 'https://www.google.co.th'
    },    
    {
        id: 'h3i964a5',
        img: 'https://fiverr-res.cloudinary.com/images/q_auto,f_auto/gigs/106464657/original/7c5667a76645ed12c47617453deca8bf2fac877e/create-a-youtube-banner-or-any-logo.png',
        link: 'https://www.youtube.com'
    },    
    {
        id: 'zu6qn94y',
        img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQezdz9IrGwB3VZPG_TvhNZked5_CHvOF-DRQ&usqp=CAU',
        link: 'https://www.facebook.com'
    }
]

const input = {
    id: '',
    img: '',
    link: ''
}

function BannerList() {
    const [modalOpen, setModalOpen] = React.useState(false);
    const [inputData, setInputData] = React.useState(input);
    const handleOpen = () => {
        setModalOpen(true)
        setInputData(input)
    };

    const bannerList = banners.map((banner) => <BannerImg key={banner.id} data={banner} setModalOpen={setModalOpen} setInputData={setInputData} /> );

    let modalPopup = null;
    if (modalOpen) {
        modalPopup = <BannerModal setOpenModal={setModalOpen} inputData={inputData} setInputData={setInputData} />
    }

    return (
        <Card>
            <MDBox px={2}>
                <Grid container spacing={6}>

                <Grid item xs={12}>
                    <Grid container mx={2} mt={1} py={1} px={2}>
                    <Grid item sm={12} xs={12} align="right">
                        <MDButton variant="gradient" color="success" onClick={handleOpen} style={{ marginRight: "2%" }}>
                        <Icon>add</Icon>
                        เพิ่ม
                        </MDButton>
                    </Grid>
                    </Grid>
                    <Card>
                    <MDBox
                        py={3}
                        px={2}
                        variant="gradient"
                        bgColor="dark"
                        borderRadius="lg"
                        coloredShadow="light"
                    >
                        <MDTypography variant="h6" color="white">
                            จัดการแบนเนอร์
                        </MDTypography>
                    </MDBox>
                    </Card>
                </Grid>

                    {/* <Grid item sm={6} xs={12}>
                        <MDTypography variant="h6" fontWeight="medium">
                            จัดการแบนเนอร์                            
                        </MDTypography>
                    </Grid>
                    <Grid item sm={6} xs={12} align="right">
                        <MDButton variant="contained" color="info" onClick={handleOpen}>
                            <Icon>add</Icon>&nbsp;เพิ่ม
                        </MDButton>
                    </Grid> */}
                </Grid>
            </MDBox>
            <MDBox pt={3} pb={3} px={2}>
                <MDBox component="ul" display="flex" flexDirection="column" p={0} m={0}>
                    {bannerList}
                </MDBox>
            </MDBox>
            {modalPopup}
        </Card>
    );
}

export default BannerList;
