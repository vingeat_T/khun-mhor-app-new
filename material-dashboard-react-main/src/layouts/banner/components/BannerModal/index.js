import * as React from 'react';
import PropTypes from 'prop-types';
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import MDButton from "components/MDButton";
import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import TextField from '@mui/material/TextField';
import Icon from "@mui/material/Icon";
import Grid from "@mui/material/Grid";
import { styled } from '@mui/material/styles';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '30%',
    minWidth: '400px',
    maxWidth: '600px',
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const styleInput = {
    width: '100%'
};

const Input = styled('input')({
    display: 'none',
});

function BannerModal({ setOpenModal, setInputData, inputData }) {
    const open = true
    
    setInputData(inputData)

    return (
        <Modal open={open} onClose={() => { 
            setOpenModal(false) 
            }} xs={12}>
            <MDBox sx={style}>
                <Grid container>
                    <Grid item sm={6} xs={12}>
                        <MDTypography variant="h6" fontWeight="medium">
                            แบนเนอร์ 
                        </MDTypography>  
                    </Grid>
                    <Grid item sm={6} xs={12} align="right">
                        <MDButton variant="outlined" color="error" onClick={() => { setOpenModal(false) }}>
                            X
                        </MDButton>
                    </Grid>
                </Grid>        
                <MDBox sx={{marginTop: '20px'}}>                
                    {inputData.img && <img src={inputData.img} alt="KhunMhorImg." width="100%"/> }
                    <label htmlFor="contained-button-file">
                        <Button variant="outlined" component="span" sx={{ color: 'info.main', width: '100%' }}>
                            อัพโหลด
                        </Button>
                    </label>
                    <Input accept="image/*" id="contained-button-file" type="file" />
                </MDBox>  
                <MDBox sx={{marginTop: '40px'}}> 
                    <TextField id="outlined-basic" label="Link" variant="outlined" sx={styleInput} value={inputData.link}/>
                </MDBox>            
                <MDBox sx={{marginTop: '60px'}}>
                    <MDButton variant="contained" color="success">
                        <Icon>save</Icon>&nbsp;บันทึก
                    </MDButton>
                </MDBox>
            </MDBox>
        </Modal>
    );
}

BannerModal.propTypes = {
    setOpenModal: PropTypes.func.isRequired,
    setInputData: PropTypes.func.isRequired,
    inputData: PropTypes.shape({
        id: null,
        img: null,
        link: null
    }).isRequired,
};

export default BannerModal;
