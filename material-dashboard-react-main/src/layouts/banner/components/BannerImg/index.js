import PropTypes from "prop-types";
import Grid from "@mui/material/Grid";
import Icon from "@mui/material/Icon";
import MDButton from "components/MDButton";

function BannerImg(props) {
    const { data, setModalOpen, setInputData } = props
    
    return (        
        <Grid container alignItems="center" mt={3} pl={5}>
            <Grid item sm={5} xs={12} align="center" mt={2}>
                <img src={data.img} alt="KhunMhorImg." width="100%"/>
            </Grid>
            <Grid item sm={5} xs={12} align="center" mt={2}>
                <span>{data.link}</span> 
            </Grid>
            <Grid item sm={2} xs={12} align="right" mt={2}>
                <MDButton variant="contained" color="warning" onClick={() => {
                    setModalOpen(true)
                    setInputData({
                        id: data.id,
                        img: data.img,
                        link: data.link
                    })
                }}>
                    <Icon>edit</Icon>&nbsp;edit
                </MDButton>
            </Grid>
        </Grid>
    );
}

BannerImg.propTypes = {
    data: PropTypes.shape({id: null, img: null, link: null}).isRequired,
    setModalOpen: PropTypes.func.isRequired,
    setInputData: PropTypes.func.isRequired,
};

export default BannerImg;
