import * as React from 'react';
// import { useState } from "react";
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import Grid from "@mui/material/Grid";
import { useNavigate } from "react-router-dom";
import Card from '@mui/material/Card';
import MDBox from 'components/MDBox';
import MDInput from 'components/MDInput';

export default function BasicSelect() {

  const navigate = useNavigate();
  const routeChange = () => {
    const path = "/tables/editdata/create?id=115";
    navigate(path);
  };


  const handleChange = (event) => {
    console.log(event);
    console.log(event.target.value);
    let path = "/dashboard"
    if (event.target.value === 10) {
      path = "/tables/editdata/create?id=115";
    } else if (event.target.value === 20) {
      path = "/tables";
    } else if (event.target.value === 30) {
      path = "/usertest";
    }
    navigate(path);
  };


  return (
    <Grid container mx={2} mt={1} py={1} px={2}>
        <Grid item sm={12} xs={6} align="center">
            <Box sx={{ minWidth: 120 }} align="center">
            <FormControl style={{ width: "20%" }}>
                <InputLabel id="demo-simple-select-label">Age</InputLabel>
                <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                // value={age}
                label="Age"
                onChange={routeChange}
                >
                <MenuItem value={10}>Ten</MenuItem>
                <MenuItem value={20}>Twenty</MenuItem>
                <MenuItem value={30}>Thirty</MenuItem>
                </Select>
            </FormControl>
            </Box>
        </Grid>
        <Grid item sm={12} xs={6} align="center">
          <Card >
            <MDBox ml={-1}>
              <MDInput OnChange={handleChange} value={10} type="text" size="sm" />
            </MDBox>
          </Card>
        </Grid>
    </Grid>
  );
}