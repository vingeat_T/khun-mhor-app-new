/* eslint-disable react/prop-types */

// Soft UI Dashboard React components
import { useState } from "react";
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import MDAvatar from "components/MDAvatar";
import MDBadge from "components/MDBadge";
import Grid from "@mui/material/Grid";
import MDButton from "components/MDButton";
import Icon from "@mui/material/Icon";
import * as React from "react";
import MDSnackbar from "components/MDSnackbar";
import { useNavigate,useSearchParams } from "react-router-dom";
// Images
// import logoXD from "assets/images/small-logos/logo-xd.svg";
// import logoAtlassian from "assets/images/small-logos/logo-atlassian.svg";
// import logoSlack from "assets/images/small-logos/logo-slack.svg";
// import logoSpotify from "assets/images/small-logos/logo-spotify.svg";
// import logoJira from "assets/images/small-logos/logo-jira.svg";
// import logoInvesion from "assets/images/small-logos/logo-invision.svg";
import team2 from "assets/images/team-2.jpg";
import team3 from "assets/images/team-3.jpg";
import team4 from "assets/images/team-4.jpg";
import VisibilityIcon from '@mui/icons-material/Visibility';
import AccessibilityIcon from '@mui/icons-material/Accessibility';
import searchtable from "layouts/stateValue/stateValue";
import { useBetween } from "use-between";
// import Link from '@mui/material/Link';

export default function data() {
  const [searchParams] = useSearchParams();
  const id = searchParams.get("id")
  const Author = ({ image, name, email }) => (
    <MDBox display="flex" alignItems="center" lineHeight={1}>
      <MDAvatar src={image} name={name} size="sm" />
      <MDBox ml={2} lineHeight={1}>
        <MDTypography display="block" variant="button" fontWeight="medium">
          {name}
        </MDTypography>
        <MDTypography variant="caption">{email}</MDTypography>
      </MDBox>
    </MDBox>
  );

  const { setDataUser } = useBetween(searchtable);
    const navigate = useNavigate();
    const routeChange = async (dataUse) => {
        console.log(dataUse)
        setDataUser(dataUse)
        const path = "/tables/editdata";
        navigate(path);

      };
  // const routeChange = () => {
  //   const path = "/tables/editdata";
  //   navigate(path);
  // };

  const viewOpen = () => {
    const path = "/tables/editdata?id=115";
    navigate(path);
  };

  const toFamily = () => {
    const path = "/tables?id=100";
    navigate(path);
  };


  const [successSB, setSuccessSB] = useState(false);
  const [deleteSB, setDeleteSB] = useState(false);

  // const openSuccessSB = () => setSuccessSB(true);
  // const openDelete = () => setDeleteSB(true);
  const closeSuccessSB = () => setSuccessSB(false);
  const closeDeleteSB = () => setDeleteSB(false);

  

  let dataTransation = [{ Image: team2, Name: "John Michael", Email: "john@creative-tim.com", Status: "ออนไลน์", Date: "23/04/18"},
    { Image: team3, Name: "Alexa Liras", Email: "alexa@creative-tim.com", Status: "ออฟไลน์", Date: "11/01/19" },
    { Image: team4, Name: "Laurent Perrier", Email: "laurent@creative-tim.com", Status: "ออนไลน์", Date: "11/01/19" },
    { Image: team3, Name: "Michael Levi", Email: "michael@creative-tim.com", Status: "ออนไลน์", Date: "11/01/20" },
    { Image: team3, Name: "Richard Gran", Email: "michael@creative-tim.com", Status: "ออนไลน์", Date: "11/01/20" },
    { Image: team2, Name: "John", Email: "miriam@creative-tim.com", Status: "ออฟไลน์", Date: "11/01/20" }]

  let dataCollum =  [
      { Header: "ชื่อผู้ใช้", accessor: "author", width: "30%", align: "left" },
      { Header: "สถานะการออนไลน์", accessor: "status", align: "center" },
      { Header: "การเข้าใช้งานล่าสุด", accessor: "employed", align: "center" },
      { Header: "ผู้รับบริการอื่น", accessor: "family", align: "center" },
      { Header: "เลือกแก่ไข", accessor: "action", align: "center" },
    ]

  if (id === '100') {

    dataTransation = [{ Image: team2, Name: "John Michael", Email: "john@creative-tim.com",Relation:"แม่", Status: "ออนไลน์", Date: "23/04/18"},
    { Image: team3, Name: "Alexa Liras", Email: "alexa@creative-tim.com",Relation:"พ่อ", Status: "ออฟไลน์", Date: "11/01/19" }]

    dataCollum =  [
      { Header: "ชื่อผู้ใช้", accessor: "author", width: "30%", align: "left" },
      { Header: "สถานะการออนไลน์", accessor: "status", align: "center" },
      { Header: "การเข้าใช้งานล่าสุด", accessor: "employed", align: "center" },
      { Header: "ความเกี่ยวข้อง", accessor: "relation", align: "center" },
      { Header: "เลือกแก่ไข", accessor: "action", align: "center" },
    ]

  }

  const renderSuccessSB = (
    <MDSnackbar
      color="warning"
      icon="check"
      title="Console Dashboard"
      content="This is a data demo can't modify"
      dateTime="11 mins ago"
      open={successSB}
      onClose={closeSuccessSB}
      close={closeSuccessSB}
      bgWhite
    />
  );

  const renderDelete = (
    <MDSnackbar
      color="error"
      icon="check"
      title="Console Dashboard"
      content="This is a data demo can't delete"
      dateTime="11 mins ago"
      open={deleteSB}
      onClose={closeDeleteSB}
      close={closeDeleteSB}
      bgWhite
    />
  );

  // const Job = ({ title, description }) => (
  //   <MDBox lineHeight={1} textAlign="left">
  //     <MDTypography display="block" variant="caption" color="text" fontWeight="medium">
  //       {title}
  //     </MDTypography>
  //     <MDTypography variant="caption">{description}</MDTypography>
  //   </MDBox>
  // );
  const setRowArray = []
    if (dataTransation) {
        dataTransation.forEach((item) => { 
            let colorSet 
            if (item.Status==='ออนไลน์') {
                colorSet = 'success'
            } else if (item.Status==='ออฟไลน์') {
                colorSet = 'error'
            } 
            const renderRow = {
              author: (
                <Author
                  image={item.Image}
                  name={item.Name}
                  email={item.Email}
                />
              ),
              status: (
                <MDBox ml={-1}>
                  <MDBadge badgeContent={item.Status} color={colorSet} variant="gradient" size="sm" />
                </MDBox>
              ),
              employed: (
                <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
                  {item.Date}
                </MDTypography>
              ),
              relation: (
                <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
                  {item.Relation}
                </MDTypography>
              ),
              family: (
                <MDBox ml={0}>
                    <MDButton variant="gradient" color="light" startIcon={<AccessibilityIcon />} onClick={toFamily}>
                      ผูัที่เกี่ยวข้อง
                    </MDButton>
                    {renderSuccessSB}
                    {renderDelete}
                </MDBox>
              ),
              action: (
                <MDBox ml={2} pt={2}>
                  <Grid container spacing={2}>
                    <MDButton variant="gradient" color="warning"  onClick={() => routeChange(item)}>
                      <Icon>edit</Icon>
                      แก้ไข
                    </MDButton>
                    <MDButton variant="gradient" color="info" startIcon={<VisibilityIcon />} onClick={viewOpen}>
                      ดูเพิ่มเติม
                    </MDButton>
                    {renderSuccessSB}
                    {renderDelete}
                  </Grid>
                </MDBox>
              )
            }
            setRowArray.push(renderRow)
        })
    }
    console.log('151', setRowArray);




  return {
    columns: dataCollum,

    rows: setRowArray,
  };
}
