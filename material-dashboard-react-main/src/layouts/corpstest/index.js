/**
=========================================================
* Material Dashboard 2 React - v2.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/
import * as React from 'react';
// @mui material components
import Grid from "@mui/material/Grid";
// import { DataGrid } from '@mui/x-data-grid';
// import Card from "@mui/material/Card";

// Material Dashboard 2 React components
import MDBox from "components/MDBox";
// import Box from '@mui/material/Box';
import MDTypography from "components/MDTypography";

// Material Dashboard 2 React example components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import Footer from "examples/Footer";
// import MDAvatar from "components/MDAvatar";
// import Slider from '@mui/material/Slider';
// import TextField from '@mui/material/TextField';


// import DataTable from "examples/Tables/DataTable";

// Data
// import authorsTableData from "layouts/tables/data/authorsTableData";
// import Manage from "layouts/managemoney/data/manage"
// import manage from "layouts/managemoney/data/manage";
// import datagrid from "layouts/managemoney/data/datagrid";
// import Project from "layouts/managemoney/project";

import Background from "layouts/corpstest/data/backgroundImage"
import HeadPro from "layouts/corpstest/data/Header"
import InfoChart from "layouts/corpstest/data/Content/Chart"

// import { Card } from '@mui/material';
// import doctor2 from "assets/images/doctor2.jpg";

// import MDAvatar from "components/MDAvatar";
// import doctor1 from "assets/images/doctor1.jpg";
// import Manage from "layouts/managemoney/data/manage";
// import doctor3 from "assets/images/doctor3.jpg";



function CorpsTest() {

  return (
    <DashboardLayout>
      <DashboardNavbar />
      {/* <Project> */}
      <MDBox p={2} pb={3}>
        <Background />
        <MDBox 
          mt={-15} 
          mx= {3}
          py= {2}
          px= {2}
          shadow="xl"
          bgColor="white" 
          borderRadius="xl"
        >
          
          <MDBox mt={5} mb={2}>
            <Grid container spacing={6}>
              <Grid item xs={12}>
                <MDBox
                  mx={2}
                  mt={-3}
                  py={3}
                  px={4}
                  variant="gradient"
                  bgColor="light"
                  borderRadius="lg"
                  coloredShadow="dark"
                >
                  <MDTypography variant="h5" color="dark">
                    Test
                  </MDTypography>
                </MDBox>
              </Grid>
            </Grid>
          </MDBox>

          <MDBox mt={5}>
            <HeadPro />
            <InfoChart />
          </MDBox>

        </MDBox>
      </MDBox>
      {/* </Project> */}
      <Footer />
    </DashboardLayout>
  );
}

export default CorpsTest;
