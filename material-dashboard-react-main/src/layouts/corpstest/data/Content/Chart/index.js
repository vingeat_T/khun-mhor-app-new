import React from 'react'

// Material Dashboard 2 React components
import MDBox from "components/MDBox";
// @mui material components
import Grid from "@mui/material/Grid";
import ReportsLineChart from "examples/Charts/LineCharts/ReportsLineChart";

import reportsLineChartData from "layouts/dashboard2/data/reportsLineChartData";

function InfoChart() {
    
  const { tasks } = reportsLineChartData;
  return (
    <Grid item xs={12} md={6} lg={4}>
        <MDBox mb={3}>
        <ReportsLineChart
            color="light"
            title="เงิน"
            description=""
            date="อัพเดทล่าสุด 1 เดือนล่าสุด"
            chart={tasks}
        />
        </MDBox>
    </Grid>
  )
}

export default InfoChart