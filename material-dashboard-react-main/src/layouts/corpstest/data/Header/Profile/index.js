import React from 'react'

// @mui material components
import Grid from "@mui/material/Grid";

// Material Dashboard 2 React components
import MDBox from "components/MDBox";
import MDAvatar from "components/MDAvatar";
import MDTypography from "components/MDTypography";

import burceMars from "assets/images/bruce-mars.jpg";
import TabBar from "layouts/corpstest/data/Header/TabsBar"

function Topper() {
  return (
    <Grid container spacing={3} alignItems="center" px={2}>
        <Grid item>
          <MDAvatar src={burceMars} alt="profile-image" size="xl" shadow="sm" />
        </Grid>
        <Grid item>
        <MDBox height="100%" mt={0.5} lineHeight={1}>
          <MDTypography variant="h5" fontWeight="medium">
              Corp Krub
          </MDTypography>
          <MDTypography variant="button" color="text" fontWeight="regular">
              Intern
          </MDTypography>
        </MDBox>
        </Grid>
        <TabBar />
    </Grid>
  )
}

export default Topper