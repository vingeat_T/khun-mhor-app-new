import React from 'react'

// Material Dashboard 2 React components
import MDBox from "components/MDBox";

import Topper from "layouts/corpstest/data/Header/Profile"

function HeadPro() {

  return (
    <MDBox position="relative" mb={3} ml={1}>
        <Topper />
    </MDBox>
  )
}

export default HeadPro