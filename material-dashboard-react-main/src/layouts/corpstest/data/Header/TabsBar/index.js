import React, { useState, useEffect } from 'react'

// @mui material components
import Grid from "@mui/material/Grid";
import AppBar from "@mui/material/AppBar";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Icon from "@mui/material/Icon";

// Material Dashboard 2 PRO React base styles
import breakpoints from "assets/theme/base/breakpoints";

function TabBar() {

    const [tabsOrientation, setTabsOrientation] = useState("horizontal");
    const [tabValue, setTabValue] = useState(0);

    const handleSetTabValue = (event, newValue) => {
        setTabValue(newValue)
        console.log(tabValue);
    };

    useEffect(() => {
    function handleTabsOrientation() {
        return window.innerWidth < breakpoints.values.sm
          ? setTabsOrientation("vertical")
          : setTabsOrientation("horizontal");
      }
  
      /** 
       The event listener that's calling the handleTabsOrientation function when resizing the window.
      */
      window.addEventListener("resize", handleTabsOrientation);
  
      // Call the handleTabsOrientation function to set the state with the initial value.
      handleTabsOrientation();
  
      // Remove event listener on cleanup
      return () => window.removeEventListener("resize", handleTabsOrientation);
    }, [tabsOrientation]);

  return (
        <Grid item xs={12} md={6} lg={4} sx={{ ml: "auto" }}>
            <AppBar position="static">
                <Tabs orientation={tabsOrientation} value={tabValue} onChange={handleSetTabValue}>
                    <Tab
                        label="Profile"
                        icon={
                        <Icon fontSize="small" sx={{ mt: -0.25 }}>
                            home
                        </Icon>
                        }
                    />
                    <Tab
                        label="Message"
                        icon={
                        <Icon fontSize="small" sx={{ mt: -0.25 }}>
                            email
                        </Icon>
                        }
                    />
                    <Tab
                        label="Settings"
                        icon={
                        <Icon fontSize="small" sx={{ mt: -0.25 }}>
                            settings
                        </Icon>
                        }
                    />
                </Tabs>
            </AppBar>
        </Grid>
  )
}

export default TabBar