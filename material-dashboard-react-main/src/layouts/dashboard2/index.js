/**
=========================================================
* Material Dashboard 2 React - v2.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2022 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

// @mui material components
import Grid from "@mui/material/Grid";

// Material Dashboard 2 React components
import MDBox from "components/MDBox";

// Material Dashboard 2 React example components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import Footer from "examples/Footer";
import ReportsBarChart from "examples/Charts/BarCharts/ReportsBarChart";
import ReportsLineChart from "examples/Charts/LineCharts/ReportsLineChart";
import ComplexStatisticsCard from "examples/Cards/StatisticsCards/ComplexStatisticsCard";

// Data
import reportsBarChartData from "layouts/dashboard2/data/reportsBarChartData";
import reportsLineChartData from "layouts/dashboard2/data/reportsLineChartData";

// Dashboard components
import Projects from "layouts/dashboard2/components/Projects";
import OrdersOverview from "layouts/dashboard2/components/OrdersOverview";
// import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';


function Dashboard() {
  const { sales, tasks } = reportsLineChartData;

  return (
    <DashboardLayout>
      <DashboardNavbar />
      <MDBox py={4}>
        <Grid container spacing={3}>
          <Grid item xs={12} md={6} lg={3}>
            <MDBox mb={1.5}>
              <ComplexStatisticsCard
                color="info"
                icon="person"
                title="ข้อมูลผู้ใช้เข้าใช้งาน"
                count="2,300"
                percentage={{
                  color: "success",
                  amount: "+55%",
                  label: "อัทเดทเมื่อ 1 อาทิตย์ที่แล้ว",
                }}
              />
            </MDBox>
          </Grid>
          <Grid item xs={12} md={6} lg={3}>
            <MDBox mb={1.5}>
              <ComplexStatisticsCard
                color="success"
                icon="person"
                title="ข้อมูลคุณหมอเข้าใช้งาน"
                count={230}
                percentage={{
                  color: "warnings",
                  amount: "-3%",
                  label: "อัทเดทเมื่อ 1 อาทิตย์ที่แล้ว",
                }}
              />
            </MDBox>
          </Grid>
          <Grid item xs={12} md={6} lg={3}>
            <MDBox mb={1.5}>
              <ComplexStatisticsCard
                color="warning"
                icon="$"
                title="รายได้แอป"
                count="34k"
                percentage={{
                  color: "success",
                  amount: "+1%",
                  label: "๊อัทเดทเมื่อ 1 อาทิตย์ที่แล้ว",
                }}
              />
            </MDBox>
          </Grid>
          <Grid item xs={12} md={6} lg={3}>
            <MDBox mb={1.5}>
              <ComplexStatisticsCard
                color="primary"
                icon="table_view"
                title="จำนวนคิวที่เหลือในระบบ"
                count="25"
                percentage={{
                  color: "success",
                  amount: "",
                  label: "อัพเดทเมื่่อนาทีที่แล้ว",
                }}
              />
            </MDBox>
          </Grid>
        </Grid>
        <MDBox mt={4.5}>
          <Grid container spacing={3}>
            <Grid item xs={12} md={6} lg={4}>
              <MDBox mb={3}>
                <ReportsBarChart
                  color="info"
                  title="จำนวนผู้ใช้รายสัปดาห์"
                  description="ดูการเข้าใช้งานของผู้ใช้ แสดงในรูปแบบกราฟรายสัปดาห์"
                  date="อัพเดทล่าสุด 2 วันที่แล้ว"
                  chart={reportsBarChartData}
                />
              </MDBox>
            </Grid>
            <Grid item xs={12} md={6} lg={4}>
              <MDBox mb={3}>
                <ReportsLineChart
                  color="success"
                  title="รายได้รายเดิอน"
                  description={
                    <>
                      (<strong>+15%</strong>) เพิ่มขึ้น.
                    </>
                  }
                  date="อัพเดทเมื่อ 1 เดือนที่แล้ว"
                  chart={sales}
                />
              </MDBox>
            </Grid>
            <Grid item xs={12} md={6} lg={4}>
              <MDBox mb={3}>
                <ReportsLineChart
                  color="warning"
                  title="จำนวนคิว"
                  description="จำนวนคิวที่มีการตอบรับจากแพทย์"
                  date="อัพเดทล่าสุด 1 เดือนล่าสุด"
                  chart={tasks}
                />
              </MDBox>
            </Grid>
          </Grid>
        </MDBox>
        <MDBox>
          <Grid container spacing={3}>
            <Grid item xs={12} md={6} lg={8}>
              <Projects />
            </Grid>
            <Grid item xs={12} md={6} lg={4}>
              <OrdersOverview />
            </Grid>
          </Grid>
        </MDBox>
      </MDBox>
      <Footer />
    </DashboardLayout>
  );
}

export default Dashboard;
