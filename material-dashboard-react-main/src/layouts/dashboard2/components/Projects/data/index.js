/* eslint-disable react/prop-types */
/* eslint-disable react/function-component-definition */
/**
=========================================================
* Material Dashboard 2 React - v2.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2022 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

// @mui material components
// import Tooltip from "@mui/material/Tooltip";
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import MDAvatar from "components/MDAvatar";
// import MDProgress from "components/MDProgress";

// Images
// import logoXD from "assets/images/small-logos/logo-xd.svg";
// import logoAtlassian from "assets/images/small-logos/logo-atlassian.svg";
// import logoSlack from "assets/images/small-logos/logo-slack.svg";
// import logoSpotify from "assets/images/small-logos/logo-spotify.svg";
// import logoJira from "assets/images/small-logos/logo-jira.svg";
// import logoInvesion from "assets/images/small-logos/logo-invision.svg";
import team1 from "assets/images/num1.png";
import team2 from "assets/images/num2.png";
import team3 from "assets/images/num3.png";
// import team4 from "assets/images/tnum1.png;
// import Icon from "@mui/material/Icon";

export default function data() {
  // const avatars = (members) =>
  //   members.map(([image, name]) => (
  //     <Tooltip key={name} title={name} placeholder="bottom">
  //       <MDAvatar
  //         src={image}
  //         alt="name"
  //         size="xs"
  //         sx={{
  //           border: ({ borders: { borderWidth }, palette: { white } }) =>
  //             `${borderWidth[2]} solid ${white.main}`,
  //           cursor: "pointer",
  //           position: "relative",

  //           "&:not(:first-of-type)": {
  //             ml: -1.25,
  //           },

  //           "&:hover, &:focus": {
  //             zIndex: "10",
  //           },
  //         }}
  //       />
  //     </Tooltip>
  //   ));

  const Company = ({ image, name }) => (
    <MDBox display="flex" alignItems="center" lineHeight={1}>
      <MDAvatar src={image} name={name} size="sm" />
      <MDTypography variant="button" fontWeight="medium" ml={1} lineHeight={1}>
        {name}
      </MDTypography>
    </MDBox>
  );

  return {
    columns: [
      { Header: "name", accessor: "companies", width: "45%", align: "left" },
      { Header: "ยอดผู้ใช้", accessor: "members", width: "15%", align: "left" },
      { Header: "ดาวที่ได้รับ", accessor: "budget", align: "center" },
      { Header: "หัวใจที่ได้รับ", accessor: "completion", align: "center" },
    ],

    rows: [
      {
        companies: <Company image={team1} name="John Michael" />,
        members: (
          <MDTypography variant="caption" color="text" fontWeight="medium">
            284
          </MDTypography>
           
        ),
        budget: (
          <MDTypography variant="caption" color="text" fontWeight="medium">
            96
          </MDTypography>
        ),
        completion: (
          <MDTypography variant="caption" color="text" fontWeight="medium">
            56
          </MDTypography>
        ),
      },
      {
        companies: <Company image={team2} name="Alexa Liras" />,
        members: (
          <MDTypography variant="caption" color="text" fontWeight="medium">
            283
          </MDTypography>
           
        ),
        budget: (
          <MDTypography variant="caption" color="text" fontWeight="medium">
            93
          </MDTypography>
        ),
        completion: (
          <MDTypography variant="caption" color="text" fontWeight="medium">
            51
          </MDTypography>
        ),
      },
      {
        companies: <Company image={team3} name="Laurent Perrier" />,
        members: (
          <MDTypography variant="caption" color="text" fontWeight="medium">
            290
          </MDTypography>
           
        ),
        budget: (
          <MDTypography variant="caption" color="text" fontWeight="medium">
            84
          </MDTypography>
        ),
        completion: (
          <MDTypography variant="caption" color="text" fontWeight="medium">
            45
          </MDTypography>
        ),
      },
      {
        companies: <Company  name="Michael Levi" />,
        members: (
          <MDTypography variant="caption" color="text" fontWeight="medium">
            290
          </MDTypography>
           
        ),
        budget: (
          <MDTypography variant="caption" color="text" fontWeight="medium">
            80
          </MDTypography>
        ),
        completion: (
          <MDTypography variant="caption" color="text" fontWeight="medium">
            49
          </MDTypography>
        ),
      },
      {
        companies: <Company  name="Richard Gran" />,
        members: (
          <MDTypography variant="caption" color="text" fontWeight="medium">
            270
          </MDTypography>
           
        ),
        budget: (
          <MDTypography variant="caption" color="text" fontWeight="medium">
            79
          </MDTypography>
        ),
        completion: (
          <MDTypography variant="caption" color="text" fontWeight="medium">
            43
          </MDTypography>
        ),
      },
      {
        companies: <Company  name="Miriam Eric" />,
        members: (
          <MDTypography variant="caption" color="text" fontWeight="medium">
            280
          </MDTypography>
           
        ),
        budget: (
          <MDTypography variant="caption" color="text" fontWeight="medium">
            76
          </MDTypography>
        ),
        completion: (
          <MDTypography variant="caption" color="text" fontWeight="medium">
            43
          </MDTypography>
        ),
      },
    ],
  };
}
