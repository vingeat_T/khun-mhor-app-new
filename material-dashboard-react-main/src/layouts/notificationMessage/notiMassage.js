import * as React from "react";

import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import Footer from 'examples/Footer'

import { Form, Button } from "semantic-ui-react";

import Grid from "@mui/material/Grid";
import MDBox from "components/MDBox";
import Card from "@mui/material/Card";
import MDTypography from "components/MDTypography";
import FevorMessage from "layouts/notificationMessage/fevorMessage";
// import BookmarkIcon from "@mui/icons-material/Bookmark";

function NotificationMessage() {
  return (
    <DashboardLayout>
      <DashboardNavbar />
      <MDBox mx={8} mt={3} mb={3}  borderRadius="lg" coloredShadow="light" pt={3} pb={8}>
        <Grid container spacing={6} justifyContent="center">
          <Grid item xs={8} lg={6}>
            <Card>
              <MDBox p={2}>
              &nbsp;
                <MDTypography variant="h3" display="flex" justifyContent="center">
                  ส่งข้อความแจ้งเตือน
                </MDTypography>
                <Form>
                  <Form.Group grouped>
                    <MDTypography variant="h4" display="flex" justifyContent="left">
                      หัวข้อ
                    </MDTypography>
                    <Form.Field control="textarea" rows="3" />
                  </Form.Group>
                  <Form.Group grouped>
                    <MDTypography variant="h4" display="flex" justifyContent="left">
                      ข้อความ
                    </MDTypography>
                    <Form.Field control="textarea" rows="3" />
                  </Form.Group>
                  <MDTypography display="flex" justifyContent="right">
                    <Button icon color="green">
                      ส่ง
                    </Button>
                  </MDTypography>
                </Form>
              </MDBox>
            </Card>
            &nbsp;
            <Card>
              <MDBox p={2} lineHeight={0} pt={2} >
                <MDTypography variant="h3" display="flex" justifyContent="center">
                    ข้อความที่บันทึกไว้
                  {/* <BookmarkIcon fontSize="inherit" sx={{ mt: 0.5 }} /> &nbsp; ข้อความที่บันทึกไว้ */}
                </MDTypography>
                <FevorMessage
                  header="Changes in Service"
                  // content="We updated our privacy policy "
                />
                <FevorMessage
                  header="Changes in Service"
                  // content="We updated our privacy policy "
                />
                <FevorMessage
                  header="Changes in Service"
                  // content="We updated our privacy policy "
                />
                <FevorMessage
                  header="Changes in Service"
                  // content="We updated our privacy policy "
                />
                <FevorMessage
                  header="Changes in Service"
                  // content="We updated our privacy policy "
                />
              </MDBox>
            </Card>
          </Grid>

          <Grid item xs={12} lg={5}>
            <Card>
              <MDBox p={2} lineHeight={0}>
                <MDTypography variant="h3" display="flex" justifyContent="center">
                  ประวัติการส่งข้อความ
                </MDTypography>
                <FevorMessage
                  header="Changes in Service"
                  // content="We updated our privacy policy "
                />
                <FevorMessage
                  header="Changes in Service"
                  // content="We updated our privacy policy "
                />
                <FevorMessage
                  header="Changes in Service"
                  // content="We updated our privacy policy "
                />
                <FevorMessage
                  header="Changes in Service"
                  // content="We updated our privacy policy "
                />
                <FevorMessage
                  header="Changes in Service"
                  // content="We updated our privacy policy "
                />
                <FevorMessage
                  header="Changes in Service"
                  // content="We updated our privacy policy "
                />
                <FevorMessage
                  header="Changes in Service"
                  // content="We updated our privacy policy "
                />
                <FevorMessage
                  header="Changes in Service"
                  // content="We updated our privacy policy "
                />
                <FevorMessage
                  header="Changes in Service"
                  // content="We updated our privacy policy "
                />
                <FevorMessage
                  header="Changes in Service"
                  // content="We updated our privacy policy "
                />
              </MDBox>
            </Card>
          </Grid>
        </Grid>
      </MDBox>
      <Footer />
    </DashboardLayout>
  );
}
export default NotificationMessage;
