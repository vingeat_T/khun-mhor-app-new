// import * as React from 'react';
// import Button from '@mui/material/Button';
// import Stack from '@mui/material/Stack';
// import SnackbarContent from '@mui/material/SnackbarContent';

// const action = (
//   <Button color="secondary" size="small">
//     lorem ipsum dolorem
//   </Button>
// );

// export default function FevorMessage() {
//   return (
//     <Stack spacing={2} sx={{ maxWidth: 600 }}>
//       <SnackbarContent message="I love snacks." action={action} />
//       <SnackbarContent
//         message={
//           'I love candy. I love cookies. I love cupcakes. \
//           I love cheesecake. I love chocolate.'
//         }
//       />
//       <SnackbarContent
//         message="I love candy. I love cookies. I love cupcakes."
//         action={action}
//       />
//       <SnackbarContent
//         message={
//           'I love candy. I love cookies. I love cupcakes. \
//           I love cheesecake. I love chocolate.'
//         }
//         action={action}
//       />
//     </Stack>
//   );
// }
import Grid from "@mui/material/Grid";
import React from "react";
import { Message } from "semantic-ui-react";
import MDTypography from "components/MDTypography";
import PropTypes from "prop-types";
import BookmarkIcon from "@mui/icons-material/Bookmark";
import IconButton from "@mui/material/IconButton";
// import Stack from "@mui/material/Stack";
import MDBox from "components/MDBox";

function FevorMessage({ header }) {
  return (
    <MDBox p={2}>
      <Grid container>
        <Grid item xs={11}>
          <Message>
            <MDTypography variant="caption text" fontWeight="light">
              {header}
            </MDTypography>
            {/* <MDTypography variant="body2" fontWeight="light" color="text">
        {content}
      </MDTypography> */}
          </Message>
        </Grid>
        <Grid item xs={1}>
        <IconButton aria-label="" size="large" >
          <BookmarkIcon fontSize="inherit" sx={{mt:0.5}} />
        </IconButton>
        </Grid>
      </Grid>
    </MDBox>
  );
}
FevorMessage.defaultProps = {};
FevorMessage.propTypes = {
  header: PropTypes.string.isRequired,
  // content: PropTypes.string.isRequired
};

export default FevorMessage;
