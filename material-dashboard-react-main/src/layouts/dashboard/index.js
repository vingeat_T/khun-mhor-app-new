/**
=========================================================
* Material Dashboard 2 React - v2.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

// @mui material components
import Grid from "@mui/material/Grid";
// import Divider from "@mui/material/Divider";

// @mui icons


// Material Dashboard 2 React components
import MDBox from "components/MDBox";
import MDPagination from "components/MDPagination"
// import MDTypography from "components/MDTypography";

// Material Dashboard 2 React example components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import Footer from "examples/Footer";

import ComplexStatistics from "examples/Cards/StatisticsCards/ComplexStatistics";

// Overview page components
import Projects from "layouts/dashboard/components/Projects";
import doctor1 from "assets/images/doctor1.jpg";
import doctor2 from "assets/images/doctor2.jpg";
import doctor3 from "assets/images/doctor3.jpg";
// import Header from "layouts/profile/components/Header";

function Overview() {
  return (
    <DashboardLayout>
      <DashboardNavbar />
      <Projects>
      <MDBox>
      &nbsp;
        <MDBox p={2}>
          <Grid container spacing={6}>
            <Grid item lg={6} xl={4}>
            <MDBox mb={1}>
              <Grid>
                <ComplexStatistics
                  value="20"
                  image={doctor1}
                  color="dark"
                  name="พญ. สิริน"
                  title="โรคหู คอ จมูก"
                  university="แพทย์ มข."
                  description="โรงพยาบาลขอนแก่น"
                  hospital="โรงพยาบาลขอนแก่น"
                  price="฿200"
                  review="5.0"
                  heart="5.0"
                  action={{
                    type: "internal",
                    route: "/report",
                    color: "dark",
                    label: "รายละเอียด",
                  }}
    
                />
              </Grid>
              </MDBox>
            </Grid>
            <Grid item lg={6} xl={4}>
            <MDBox mb={1}>
              <ComplexStatistics
                image={doctor2}
                name="นพ. ธนวัฒน์ "
                title="โรคทางเดินหายใจ"
                university="แพทย์ มข."
                description="โรงพยาบาลขอนแก่น"
                hospital="โรงพยาบาลขอนแก่น"
                price="฿200"
                review="5.0"
                heart="5.0"
                action={{
                  type: "internal",
                  route: "/report",
                  color: "dark",
                  label: "รายละเอียด",
                }}
  
              />
              </MDBox>
            </Grid>
            <Grid item lg={6} xl={4}>
            <MDBox mb={1}>
              <ComplexStatistics
                image={doctor3}
                name="นพ. อาทิตย์"
                title="โรคผิวหนัง"
                university="แพทย์ มข."
                hospital="โรงพยาบาลขอนแก่น"
                price="฿200"
                review="5.0"
                heart="5.0"
                action={{
                  type: "internal",
                  route: "/report",
                  color: "dark",
                  label: "รายละเอียด",
                }}
              />
              </MDBox>
            </Grid>
            <Grid item lg={6} xl={4}>
            <MDBox mb={1}>
              <ComplexStatistics
                value="20"
                image={doctor1}
                color="dark"
                name="พญ. สิริน"
                title="โรคหู คอ จมูก"
                university="แพทย์ มข."
                description="โรงพยาบาลขอนแก่น"
                hospital="โรงพยาบาลขอนแก่น"
                price="฿200"
                review="5.0"
                heart="5.0"
                action={{
                  type: "internal",
                  route: "/report",
                  color: "dark",
                  label: "รายละเอียด",
                }}
  
              />
              </MDBox>
            </Grid>
            <Grid item lg={6} xl={4}>
            <MDBox mb={1}>
              <ComplexStatistics
                image={doctor2}
                name="นพ. ธนวัฒน์ "
                title="โรคทางเดินหายใจ"
                university="แพทย์ มข."
                description="โรงพยาบาลขอนแก่น"
                hospital="โรงพยาบาลขอนแก่น"
                price="฿200"
                review="5.0"
                heart="5.0"
                action={{
                  type: "internal",
                  route: "/report",
                  color: "dark",
                  label: "รายละเอียด",
                }}
  
              />
              </MDBox>
            </Grid>
            <Grid item lg={6} xl={4}>
            <MDBox mb={1}>
              <ComplexStatistics
                image={doctor3}
                name="นพ. อาทิตย์"
                title="โรคผิวหนัง"
                university="แพทย์ มข."
                hospital="โรงพยาบาลขอนแก่น"
                price="฿200"
                review="5.0"
                heart="5.0"
                action={{
                  type: "internal",
                  route: "/report",
                  color: "dark",
                  label: "รายละเอียด",
                }}
              />
              </MDBox>
            </Grid>
          </Grid>
        </MDBox>
        {/* here */}
        <Grid item xs={12} md={6} lg={2} mt={-3} sx={{ ml: "auto", mt: 3 }}>
          <MDPagination />
        </Grid>
        </MDBox>
      </Projects>
      <Footer />
    </DashboardLayout>
  );
}

export default Overview;
