
import Grid from "@mui/material/Grid";

import MDBox from "components/MDBox";


import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import Footer from "examples/Footer";



import ComplexStatisticsCard from "layouts/ComplexStatisticsCard/index";
import DropdownExampleSearchSelection from "layouts/dropdown/dropDown";

import { Dropdown, Menu } from "semantic-ui-react";
import user1 from "assets/images/user1.jpg";
import user2 from "assets/images/user2.jpg";
import user3 from "assets/images/user3.jpg";
import user4 from "assets/images/user4.jpg";
import user5 from "assets/images/user5.jpg";
import user6 from "assets/images/user6.jpg";



function waitAsk() {
  // const { sales, tasks } = reportsLineChartData;

  function redirect(value) {
    if (value) {
      window.location.href = value;
    }
  }
  
  return (
    <DashboardLayout>
      <DashboardNavbar />
      {/* <Card mb={20}> */}
      {/* <DropdownMenuAsk /> */}
      <Menu vertical>
        <Dropdown item text="ถามหมอ">
          <Dropdown.Menu>
            <Dropdown.Item onClick={() => redirect("freeAsk")}>ถามฟรี</Dropdown.Item>
            <Dropdown.Item onClick={() => redirect("waitAsk")}>ถามรอ</Dropdown.Item>
            <Dropdown.Item onClick={() => redirect("nowAsk")}>ถามทันที</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </Menu>
      <MDBox mx={1}>
      <DropdownExampleSearchSelection/>
      </MDBox>
      <MDBox py={3}>
        <Grid container spacing={3} display="flex" justifyContent="space-between"  pt={2} px={5}>
          <Grid item xs={12} md={6} lg={4}>
            <MDBox mb={1.5}>
              <ComplexStatisticsCard
                detail1="18/2/2565"
                detail2="ลำดับที่ 1"
                color="dark"
                image={user1}
                iconx="cancel"
                title="สโนว์"
                count="ปวดเท้า"
                header="รายละเอียดอาการ"
                content="ปวดมากๆปวดจี๊ดๆเท้าเป็นหนึ่งในอวัยวะที่มีความซับซ้อนที่สุดของร่างกายประกอบด้วยกระดูก 26 ชิ้น เชื่อมต่อกันด้วยข้อต่อ กล้ามเนื้อ เอ็นกล้ามเนื้อและเอ็นยึด"
              />
              {/* <MesSage
              header="dark"
              content="มีอาการปวดเท้าค่ะ"
              /> */}
            </MDBox>
          </Grid>
          <Grid item xs={12} md={6} lg={4}>
            <MDBox mb={1.5}>
              <ComplexStatisticsCard
                detail1="17/2/2565"
                detail2="ลำดับที่ 2"
                color="dark"
                image={user2}
                title="Apatsara"
                count="มีประจำมานาน"
                header="รายละเอียดอาการ"
                content="ประจำเดือนมานานหลายวันมา ประมาณ2 อาทิตย์ได้ มาทีก็มาเยอะเปลื่ยนผ้าอนามัยจนเหนื่อย รู้สึกเพลียมากไม่มีแรง แล้วก็ปวดท้องมาก แบบนี้จะเเป็นอะไรมั๊ยคะ"
              />
            </MDBox>
          </Grid>
          <Grid item xs={12} md={6} lg={4}>
            <MDBox mb={1.5}>
              <ComplexStatisticsCard
                detail1="16/2/2565"
                detail2="ลำดับที่ 3"
                color="dark"
                image={user3}
                title="Anna"
                count="ปวดหัว"
                header="รายละเอียดอาการ"
                content="ปวดหัวปวดบ่อยอาทิตย์นึงปวดหลายรอบมาก เมื่อเย็นของวันนี้รู้สึกตัวร้อน หายใจไม่ทั่วท้อง ไม่แน่ใจว่าเพราะปวดหัวรึป่าว บางครั้งปวดแล้วก็อ้วก พออ้วกแล้วจะดีขึ้น"
              />
            </MDBox>
          </Grid>
          <Grid item xs={12} md={6} lg={4}>
            <MDBox mb={1.5}>
              <ComplexStatisticsCard
                detail1="17/2/2565"
                detail2="ลำดับที่ 2"
                color="dark"
                image={user4}
                title="Apatsara"
                count="มีประจำเดือนนาน"
                header="รายละเอียดอาการ"
                content="ประจำเดือนมานานหลายวันมา ประมาณ2 อาทิตย์ได้ มาทีก็มาเยอะเปลื่ยนผ้าอนามัยจนเหนื่อย รู้สึกเพลียมากไม่มีแรง แล้วก็ปวดท้องมาก แบบนี้จะเเป็นอะไรมั๊ยคะ"
              />
            </MDBox>
          </Grid>
          <Grid item xs={12} md={6} lg={4}>
            <MDBox mb={1.5}>
              <ComplexStatisticsCard
                detail1="16/2/2565"
                detail2="ลำดับที่ 3"
                color="dark"
                image={user5}
                title="Anna"
                count="ปวดหัว"
                header="รายละเอียดอาการ"
                content="ปวดหัวปวดบ่อยอาทิตย์นึงปวดหลายรอบมาก เมื่อเย็นของวันนี้รู้สึกตัวร้อน หายใจไม่ทั่วท้อง ไม่แน่ใจว่าเพราะปวดหัวรึป่าว บางครั้งปวดแล้วก็อ้วก พออ้วกแล้วจะดีขึ้น"
              />
            </MDBox>
          </Grid>
          <Grid item xs={12} md={6} lg={4}>
            <MDBox mb={1.5}>
              <ComplexStatisticsCard
                detail1="17/2/2565"
                detail2="ลำดับที่ 2"
                color="dark"
                image={user6}
                title="Apatsara"
                count="มีประจำเดือนนาน"
                header="รายละเอียดอาการ"
                content="ประจำเดือนมานานหลายวันมา ประมาณ2 อาทิตย์ได้ มาทีก็มาเยอะเปลื่ยนผ้าอนามัยจนเหนื่อย รู้สึกเพลียมากไม่มีแรง แล้วก็ปวดท้องมาก แบบนี้จะเเป็นอะไรมั๊ยคะ"
              />
            </MDBox>
          </Grid>
        </Grid>
      </MDBox>
      {/* </Card> */}
      <Footer />
    </DashboardLayout>
  );
}
export default waitAsk;
