import React from "react";
import MDInput from "components/MDInput";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import Footer from 'examples/Footer'
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
// import MDAvatar from "components/MDAvatar";
import MDButton from "components/MDButton";
import { useNavigate,useSearchParams } from "react-router-dom";
// import team2 from "assets/images/team-2.jpg";
// import blank from "assets/images/blankpicture.png";
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import FormControlLabel from '@mui/material/FormControlLabel';
import RadioGroup from '@mui/material/RadioGroup';
import FormGroup from '@mui/material/FormGroup';
import Radio from '@mui/material/Radio';

 
function EditData() {
  const [searchParams] = useSearchParams();
  
  const id = searchParams.get("id")
  console.log(id);
  let user = {
    name: "โจ้โจ้ ",
    type: 10,
    date: "1997-11-23",
    select: "null",
    tall: "173",
    weight: "55",
    bmi: "53",
    reason: "ส่งฮอลฟรุตตี้ไป",
    boold: "96",
    boold2: "85",
    temp: "37",
    o2: "600",
    disease: "ทางเดินอาหาร",
    drug: "พาราเซตามอล",
    radioBoxDisease: ["มี","เห็นคนหน้าตาดีแล้วใจมันสั่น"],
    radioBoxDrug: ["แพ้ยา","ยาหยี"]
  }
  if (id) {
      user = {
        name: null,
        type: null,
        date: null,
        select: null,
        tall: null,
        weight: null,
        bmi: null,
        reason: null,
        boold: null,
        boold2: null,
        temp: null,
        o2: null,
        disease: null,
        drug: null,
        radioBoxDisease: ["ไม่มี",null],
        radioBoxDrug: ["ไม่แพ้ยา",null]
      }
  } 

  const navigate = useNavigate();
  const routeChange = () => {
    const path = "/documentcreate";
    navigate(path);
  };
  const [age, setAge] = React.useState(user.type);
  const handleChange = (event) => {
    setAge(event.target.value);
  };

  const [symp, setSymp] = React.useState(user.radioBoxDisease[1])
  const [drug, setDrug] = React.useState(user.radioBoxDrug[1])


  return (
    <DashboardLayout>
      <DashboardNavbar />
      <MDBox pb={3}>
        <Grid container spacing={6}>
        <Grid item sm={3} xs={12} />
          <Grid item sm={6} xs={12} mt={3}>
            <Card>
              <MDBox
                mx={0}
                mt={0}
                py={2}
                px={1}
                variant="contained"
                bgColor="light"
                coloredShadow="info"
              >
                <MDTypography variant="h6" color="back">
                  แก้ไขเอกสาร
                </MDTypography>
              </MDBox>
              <MDBox pt={5} px={5}>
                <MDInput type="text" label="Name" value={user.name} fullWidth />
              </MDBox>
              <MDBox pt={3} px={5} md={6}>
                <Box>
                    <FormControl sx = {{width: "48%"}} >
                        <InputLabel id="demo-simple-select-label" >เพศ</InputLabel>
                        <Select
                        defaultValue={null}
                        style = {{height: 38}}
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={age}
                        label="เพศ"
                        onChange={handleChange}
                        >
                        <MenuItem value={10}>ชาย</MenuItem>
                        <MenuItem value={20}>หญิง</MenuItem>
                        </Select>
                    </FormControl>
                    <MDInput
                    type="date"
                    label={id ? "" : "วันเดือนปีเกิด"}
                    value={user.date}
                    style={{ width: "50%", marginLeft: "2%" }}
                    />
                    </Box>
              </MDBox>
              <MDBox pt={3} px={5} md={6}>
                <MDInput type="text" label="ส่วนสูง" value={user.tall} style={{ width: "31%" }} />
                <MDInput type="text" label="น้ำหนัก" value={user.weight} style={{ width: "31%" ,marginLeft: "2%"}} />
                <MDInput type="text" label="BMI" value={user.bmi} style={{ width: "34%" ,marginLeft: "2%"}} />
              </MDBox>
              <MDBox pt={3} px={5} md={6}>
                <MDInput type="text" label="ความดันโลหิต" value={user.boold} style={{ width: "48%" }} />
                <MDInput type="text" label="ชีพจร" value={user.boold2} style={{ width: "50%" ,marginLeft: "2%"}} />
              </MDBox>
              <MDBox pt={3} px={5} md={6}>
                <MDInput type="text" label="อุณหภูมิร่างกาย" value={user.temp} style={{ width: "48%"}} />
                <MDInput type="text" label="ระดับออกซิเจนปลายนิ้ว" value={user.o2} style={{ width: "50%" ,marginLeft: "2%"}} />
              </MDBox>
              <MDBox pt={3} px={5} md={6}>
                <MDInput type="text" label="ยาที่ใช้ในปัจจุบัน" value={user.drug} style={{ width: "48%" }} />
              </MDBox>
              <MDBox pt={3} px={5} md={6}>
                <MDTypography variant="h6" color="back">
                  โรคประจำตัว
                </MDTypography>
                <RadioGroup
                  defaultValue={user.radioBoxDisease[0]}
                  name="radio-buttons-group"
                >
                  <FormControlLabel value="ไม่มี" control={<Radio onClick={() => {setSymp("")}} />} label="ไม่มี" />
                  <FormGroup aria-label="position" row>
                  <FormControlLabel value="มี" control={<Radio onClick={() => {setSymp(user.radioBoxDisease[1])}} />} label="มี" />
                  <MDInput
                  placeholder="โรคประจำตัว.."
                  type="text"
                  value={symp}
                  style={{ width: "40%", marginLeft: "5.5%" }}
                />
                </FormGroup>
                </RadioGroup>
              </MDBox>
              <MDBox pt={3} px={5} md={6}>
                <MDTypography variant="h6" color="back">
                  ประวัติแพ้ยา
                </MDTypography>
                <RadioGroup
                  defaultValue={user.radioBoxDrug[0]}
                  name="radio-buttons-group"
                >
                  <FormControlLabel value="ไม่แพ้ยา" control={<Radio onClick={() => {setDrug("")}} />} label="ไม่แพ้ยา" />
                  <FormGroup aria-label="position" row>
                  <FormControlLabel value="แพ้ยา" control={<Radio onClick={() => {setDrug(user.radioBoxDrug[1])}} />} label="แพ้ยา" />
                  <MDInput
                  placeholder="แพ้ยา"
                  type="text"
                  value={drug}
                  style={{ width: "40%", marginLeft: "2%" }}
                />
                </FormGroup>
                </RadioGroup>
              </MDBox>
              <MDBox pt={3} px={5} md={6}>
                <MDInput label="คำแนะนำแพทย์" value={user.reason}  multiline rows={5} style={{ width: "98%" }} />
              </MDBox>
              <Grid container my={1} py={1}>
                <Grid item sm={12} xs={12} align="center">
                    <MDButton variant="gradient" color="secondary" onClick={routeChange}>
                    กลับ
                    </MDButton>
                    <MDButton
                    variant="gradient"
                    color="primary"
                    style={{ marginLeft: "2%" }}
                    onClick={routeChange}
                    >
                    บันทึก
                    </MDButton>
                </Grid>
              </Grid>
            </Card>
          </Grid>
        </Grid>
      </MDBox>
      <Footer />
    </DashboardLayout>
  );
}
export default EditData;
