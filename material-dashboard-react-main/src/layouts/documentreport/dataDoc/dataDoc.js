/* eslint-disable react/prop-types */

// Soft UI Dashboard React components
import { useState } from "react";
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
// import MDAvatar from "components/MDAvatar";
import MDBadge from "components/MDBadge";
import Grid from "@mui/material/Grid";
import MDButton from "components/MDButton";
import Icon from "@mui/material/Icon";
import * as React from "react";
import MDSnackbar from "components/MDSnackbar";
import { useNavigate } from "react-router-dom";
// Images
// import logoXD from "assets/images/small-logos/logo-xd.svg";
// import logoAtlassian from "assets/images/small-logos/logo-atlassian.svg";
// import logoSlack from "assets/images/small-logos/logo-slack.svg";
// import logoSpotify from "assets/images/small-logos/logo-spotify.svg";
// import logoJira from "assets/images/small-logos/logo-jira.svg";
// import logoInvesion from "assets/images/small-logos/logo-invision.svg";
// import team2 from "assets/images/team-2.jpg";
// import team3 from "assets/images/team-3.jpg";
// import team4 from "assets/images/team-4.jpg";

export default function data() {

  const navigate = useNavigate();
  const routeChange = () => {
    const path = "/document";
    navigate(path);
  };

  const [successSB, setSuccessSB] = useState(false);
  const [deleteSB, setDeleteSB] = useState(false);

  // const openSuccessSB = () => setSuccessSB(true);
  const openDelete = () => setDeleteSB(true);
  const closeSuccessSB = () => setSuccessSB(false);
  const closeDeleteSB = () => setDeleteSB(false);

  const renderSuccessSB = (
    <MDSnackbar
      color="warning"
      icon="check"
      title="Console Dashboard"
      content="This is a data demo can't modify"
      dateTime="11 mins ago"
      open={successSB}
      onClose={closeSuccessSB}
      close={closeSuccessSB}
      bgWhite
    />
  );

  const renderDelete = (
    <MDSnackbar
      color="error"
      icon="check"
      title="Console Dashboard"
      content="This is a data demo can't delete"
      dateTime="11 mins ago"
      open={deleteSB}
      onClose={closeDeleteSB}
      close={closeDeleteSB}
      bgWhite
    />
  );

  const dataColumns = [
    { Header: "รหัสเอกสาร", accessor: "author", width: "30%", align: "left" },
    { Header: "ผู้รับผิดชอบเอกสาร", accessor: "doctor", align: "center" },
    { Header: "สถานะ", accessor: "status", align: "center" },
    { Header: "อัพเดท", accessor: "employed", align: "center" },
    { Header: "แก้ไข", accessor: "action", align: "center" },
  ]

  const dataRows = [
    { author: "1RP-0000001", doctor: "พญ. สิริน", status: "SUCCESS", employed: "23/04/18"},
    { author: "1RP-0000002", doctor: "นพ. โจ้", status: "MODIFY", employed: "11/01/19"},
    { author: "1RP-0000003", doctor: "นพ. เทล", status: "SUCCESS", employed: "19/09/17"},
    { author: "1RP-0000004", doctor: "นพ. เอิทธ์", status: "SUCCESS", employed: "24/12/08"},
    { author: "1RP-0000005", doctor: "พญ. แก้ว", status: "SUCCESS", employed: "04/10/21"},
    { author: "1RP-0000006", doctor: "พญ. ขิง", status: "SUCCESS", employed: "14/09/20"},
  ]

  const setRowArray = []
  if (dataRows) {
    dataRows.forEach((val) => {
      let colorSet
      if (val.status === "SUCCESS") {
        colorSet = 'success'
      }
      if (val.status === "MODIFY") {
        colorSet = 'error'
      }
      const renderRow = {
        author: (
          <MDTypography component="a" variant="h5" color="text" fontWeight="medium">
            {val.author}
          </MDTypography>
        ),
        doctor: (
          <MDTypography component="a" variant="caption" color="text" fontWeight="medium">
            {val.doctor}
          </MDTypography>
        ),
        status: (
          <MDBox ml={-1}>
            <MDBadge badgeContent={val.status} color={colorSet} variant="gradient" size="sm" />
          </MDBox>
        ),
        employed: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            {val.employed}
          </MDTypography>
        ),
        action: (
          <MDBox ml={2} pt={2}>
            <Grid container spacing={2}>
              <MDButton variant="gradient" color="warning" onClick={routeChange}>
                <Icon>edit</Icon>
                แก้ไข
              </MDButton>
              <MDButton variant="gradient" color="error" onClick={openDelete}>
                <Icon>delete</Icon>
                ลบ
              </MDButton>
              {renderSuccessSB}
              {renderDelete}
            </Grid>
          </MDBox>
        )
      }
      setRowArray.push(renderRow)
    })
  }

  return {
    columns: dataColumns,

    rows: setRowArray

    // columns: [
    //   { Header: "รหัสเอกสาร", accessor: "author", width: "30%", align: "left" },
    //   { Header: "ผู้รับผิดชอบเอกสาร", accessor: "doctor", align: "center" },
    //   { Header: "สถานะ", accessor: "status", align: "center" },
    //   { Header: "อัพเดท", accessor: "employed", align: "center" },
    //   { Header: "แก้ไข", accessor: "action", align: "center" },
    // ],

    // rows: [
    //   {
    //     author: (
    //       <MDTypography component="a" variant="h5" color="text" fontWeight="medium">
    //         1RP-0000001
    //       </MDTypography>
    //     ),
    //     doctor: (
    //       <MDTypography component="a" variant="caption" color="text" fontWeight="medium">
    //         พญ. สิริน
    //       </MDTypography>
    //     ),
    //     status: (
    //       <MDBox ml={-1}>
    //         <MDBadge badgeContent="success" color="success" variant="gradient" size="sm" />
    //       </MDBox>
    //     ),
    //     employed: (
    //       <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
    //         23/04/18
    //       </MDTypography>
    //     ),
    //     action: (
    //       <MDBox ml={-1}>
    //         <Grid container spacing={2}>
    //           <MDButton variant="gradient" color="warning" onClick={routeChange}>
    //             <Icon>edit</Icon>
    //             แก้ไข
    //           </MDButton>
    //           <MDButton variant="gradient" color="error" onClick={openDelete}>
    //             <Icon>delete</Icon>
    //             ลบ
    //           </MDButton>
    //           {renderSuccessSB}
    //           {renderDelete}
    //         </Grid>
    //       </MDBox>
    //     ),
    //   },
    //   {
    //     author: (
    //       <MDTypography component="a" variant="h5" color="text" fontWeight="medium">
    //         1RP-0000002
    //       </MDTypography>
    //     ),
    //     doctor: (
    //       <MDTypography component="a" variant="caption" color="text" fontWeight="medium">
    //         นพ.โจ้
    //       </MDTypography>
    //     ),
    //     status: (
    //       <MDBox ml={-1}>
    //         <MDBadge badgeContent="modify" color="error" variant="gradient" size="sm" />
    //       </MDBox>
    //     ),
    //     employed: (
    //       <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
    //         11/01/19
    //       </MDTypography>
    //     ),
    //     action: (
    //       <MDBox ml={-1}>
    //         <Grid container spacing={2}>
    //           <MDButton variant="gradient" color="warning" onClick={routeChange}>
    //             <Icon>edit</Icon>
    //             แก้ไข
    //           </MDButton>
    //           <MDButton variant="gradient" color="error" onClick={openDelete}>
    //             <Icon>delete</Icon>
    //             ลบ
    //           </MDButton>
    //           {renderSuccessSB}
    //           {renderDelete}
    //         </Grid>
    //       </MDBox>
    //     ),
    //   },
    //   {
    //     author: (
    //       <MDTypography component="a" variant="h5" color="text" fontWeight="medium">
    //         1RP-0000003
    //       </MDTypography>
    //     ),
    //     doctor: (
    //       <MDTypography component="a" variant="caption" color="text" fontWeight="medium">
    //         นพ.เทล
    //       </MDTypography>
    //     ),
    //     status: (
    //       <MDBox ml={-1}>
    //         <MDBadge badgeContent="success" color="success" variant="gradient" size="sm" />
    //       </MDBox>
    //     ),
    //     employed: (
    //       <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
    //         19/09/17
    //       </MDTypography>
    //     ),
    //     action: (
    //       <MDBox ml={-1}>
    //         <Grid container spacing={2}>
    //           <MDButton variant="gradient" color="warning" onClick={routeChange}>
    //             <Icon>edit</Icon>
    //             แก้ไข
    //           </MDButton>
    //           <MDButton variant="gradient" color="error" onClick={openDelete}>
    //             <Icon>delete</Icon>
    //             ลบ
    //           </MDButton>
    //           {renderSuccessSB}
    //           {renderDelete}
    //         </Grid>
    //       </MDBox>
    //     ),
    //   },
    //   {
    //     author: (
    //       <MDTypography component="a" variant="h5" color="text" fontWeight="medium">
    //         1RP-0000004
    //       </MDTypography>
    //     ),
    //     doctor: (
    //       <MDTypography component="a" variant="caption" color="text" fontWeight="medium">
    //         นพ.เอิทธ์
    //       </MDTypography>
    //     ),
    //     status: (
    //       <MDBox ml={-1}>
    //         <MDBadge badgeContent="success" color="success" variant="gradient" size="sm" />
    //       </MDBox>
    //     ),
    //     employed: (
    //       <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
    //         24/12/08
    //       </MDTypography>
    //     ),
    //     action: (
    //       <MDBox ml={-1}>
    //         <Grid container spacing={2}>
    //           <MDButton variant="gradient" color="warning" onClick={routeChange}>
    //             <Icon>edit</Icon>
    //             แก้ไข
    //           </MDButton>
    //           <MDButton variant="gradient" color="error" onClick={openDelete}>
    //             <Icon>delete</Icon>
    //             ลบ
    //           </MDButton>
    //           {renderSuccessSB}
    //           {renderDelete}
    //         </Grid>
    //       </MDBox>
    //     ),
    //   },
    //   {
    //     author: (
    //       <MDTypography component="a" variant="h5" color="text" fontWeight="medium">
    //         1RP-0000005
    //       </MDTypography>
    //     ),
    //     doctor: (
    //       <MDTypography component="a" variant="caption" color="text" fontWeight="medium">
    //         พญ. แก้ว
    //       </MDTypography>
    //     ),
    //     status: (
    //       <MDBox ml={-1}>
    //         <MDBadge badgeContent="success" color="success" variant="gradient" size="sm" />
    //       </MDBox>
    //     ),
    //     employed: (
    //       <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
    //         04/10/21
    //       </MDTypography>
    //     ),
    //     action: (
    //       <MDBox ml={-1}>
    //         <Grid container spacing={2}>
    //           <MDButton variant="gradient" color="warning" onClick={routeChange}>
    //             <Icon>edit</Icon>
    //             แก้ไข
    //           </MDButton>
    //           <MDButton variant="gradient" color="error" onClick={openDelete}>
    //             <Icon>delete</Icon>
    //             ลบ
    //           </MDButton>
    //           {renderSuccessSB}
    //           {renderDelete}
    //         </Grid>
    //       </MDBox>
    //     ),
    //   },
    //   {
    //     author: (
    //       <MDTypography component="a" variant="h5" color="text" fontWeight="medium">
    //         1RP-0000006
    //       </MDTypography>
    //     ),
    //     doctor: (
    //       <MDTypography component="a" variant="caption" color="text" fontWeight="medium">
    //         พญ. ขิง
    //       </MDTypography>
    //     ),
    //     status: (
    //       <MDBox ml={-1}>
    //         <MDBadge badgeContent="success" color="success" variant="gradient" size="sm" />
    //       </MDBox>
    //     ),
    //     employed: (
    //       <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
    //         14/09/20
    //       </MDTypography>
    //     ),
    //     action: (
    //       <MDBox ml={-1}>
    //         <Grid container spacing={2}>
    //           <MDButton variant="gradient" color="warning" onClick={routeChange}>
    //             <Icon>edit</Icon>
    //             แก้ไข
    //           </MDButton>
    //           <MDButton variant="gradient" color="error" onClick={openDelete}>
    //             <Icon>delete</Icon>
    //             ลบ
    //           </MDButton>
    //           {renderSuccessSB}
    //           {renderDelete}
    //         </Grid>
    //       </MDBox>
    //     ),
    //   },
    // ],
  };
}
