import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import React from "react";
import { useNavigate } from "react-router-dom";

// Material Dashboard 2 React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import MDButton from "components/MDButton";
import Icon from "@mui/material/Icon";

// Material Dashboard 2 React example components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import DataTable from "examples/Tables/DataTable";
import Footer from 'examples/Footer'

// Data
import authorsTableData from "layouts/documentreport/dataDoc/dataDoc";

function Tables() {
  const { columns, rows } = authorsTableData();

  const navigate = useNavigate();
  const routeChange = () => {
    const path = "/document?id=115";
    navigate(path);
  };

  return (
    <DashboardLayout>
      <DashboardNavbar />
      <MDBox pb={3}>
        <Grid container spacing={6}>
          <Grid item xs={12}>
            <Grid container mx={2} mt={1} py={1} px={2}>
              <Grid item sm={12} xs={12} align="right">
                <MDButton variant="gradient" color="success" onClick={routeChange} style={{ marginRight: "2%" }}>
                  <Icon>add</Icon>
                  เพิ่ม
                </MDButton>
              </Grid>
            </Grid>
            <Card>
              <MDBox
                mx={2}
                mt={1}
                py={3}
                px={2}
                variant="gradient"
                bgColor="dark"
                borderRadius="lg"
                coloredShadow="info"
              >
                <MDTypography variant="h6" color="white">
                  จัดการเอกสาร
                </MDTypography>
              </MDBox>
              <MDBox pt={3}>
                <DataTable
                  table={{ columns, rows }}
                  isSorted={false}
                  entriesPerPage={false}
                  showTotalEntries={false}
                  noEndBorder
                />
              </MDBox>
            </Card>
          </Grid>
        </Grid>
      </MDBox>
      <Footer />
    </DashboardLayout>
  );
}

export default Tables;