import React from "react";
import MDInput from "components/MDInput";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import MDAvatar from "components/MDAvatar";
import MDButton from "components/MDButton";
import { useNavigate,useSearchParams } from "react-router-dom";
import team2 from "assets/images/team-2.jpg";
import blank from "assets/images/blankpicture.png";
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import Footer from 'examples/Footer'

function EditData() {
  const [searchParams] = useSearchParams();
  const id = searchParams.get("id")
  console.log(id);
  let user = {
    name: "John Smith ",
    email: "john@smith.com",
    select: "null",
    coin: "10000",
    userName: "Vingeat",
    tel: "0895477651",
    image: team2
  }
  if (id) {
      user = {
        name: null,
        email: null,
        select: null,
        coin: "0",
        userName: null,
        tel: null,
        image: blank

      }
  } 

  const navigate = useNavigate();
  const routeChange = () => {
    const path = "/admin";
    navigate(path);
  };
  const [age, setAge] = React.useState('');
  const handleChange = (event) => {
    setAge(event.target.value);
  };

  return (
    <DashboardLayout>
      <DashboardNavbar />
      <MDBox pb={3} mt={2}>
        <Grid container spacing={6}>
        <Grid item sm={3} xs={12} />
          <Grid item sm={6} xs={12}>
            <Card>
              <MDBox
                mx={0}
                mt={0}
                py={2}
                px={2}
                variant="contained"
                bgColor="light"
                coloredShadow="info"
              >
                <MDTypography variant="h6" color="back">
                  จัดการผู้ดูและระบบ
                </MDTypography>
              </MDBox>
              <MDBox pt={3} px={5}>
                <MDAvatar src={user.image} alt="Avatar" variant="square" size="xl" />
              </MDBox>
              <MDBox pt={3} px={5}>
                <Box>
                    <FormControl sx = {{width: "30%"}} >
                        <InputLabel id="demo-simple-select-label" >ตำแหน่ง</InputLabel>
                        <Select
                        style = {{height: 40}}
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={age}
                        label="Rank"
                        onChange={handleChange}
                        >
                        <MenuItem value={10}>Junior admin</MenuItem>
                        <MenuItem value={20}>Senior admin</MenuItem>
                        <MenuItem value={30}>Specialist</MenuItem>
                        <MenuItem value={30}>Manager</MenuItem>
                        <MenuItem value={30}>Creator</MenuItem>
                        </Select>
                    </FormControl>
                </Box>
              </MDBox>
              <MDBox pt={3} px={5}>
                <MDInput type="text" label="ชื่อ - สกุล" value={user.name} fullWidth />
              </MDBox>
              <MDBox pt={3} px={5} md={6}>
                <MDInput
                    type="tel"
                    label="เบอร์โทรศัพท์"
                    value={user.tel}
                    style={{ width: "48.5%" }}
                    />
                    <MDInput
                    type="email"
                    label="อีเมลล์"
                    value={user.email}
                    style={{ width: "48.5%", marginLeft: "2%" }}
                    />
                </MDBox>
              <MDBox pt={3} px={5} md={6}>
                <MDInput type="text" label="ชื่อผู้ใช้" value={user.userName} style={{ width: "48.5%" }} />
                <MDInput
                  type="text"
                  label="รห้สผ่าน"
                  value={user.select}
                  style={{ width: "48.5%", marginLeft: "2%" }}
                />
              </MDBox>
              <Grid container mt={2} py={1} mb={2}>
                <Grid item sm={12} xs={12} align="center">
                    <MDButton variant="gradient" color="secondary" onClick={routeChange}>
                    กลับ
                    </MDButton>
                    <MDButton
                    variant="gradient"
                    color="primary"
                    style={{ marginLeft: "2%" }}
                    onClick={routeChange}
                    >
                    บันทึก
                    </MDButton>
                </Grid>
              </Grid>
            </Card>
          </Grid>
        </Grid>
      </MDBox>
      <Footer />
    </DashboardLayout>
  );
}
export default EditData;
