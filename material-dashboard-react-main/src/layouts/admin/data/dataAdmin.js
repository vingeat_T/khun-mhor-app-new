/* eslint-disable react/prop-types */

// Soft UI Dashboard React components
import { useState } from "react";
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import MDAvatar from "components/MDAvatar";
import MDBadge from "components/MDBadge";
import Grid from "@mui/material/Grid";
import MDButton from "components/MDButton";
import Icon from "@mui/material/Icon";
import * as React from "react";
import MDSnackbar from "components/MDSnackbar";
import { useNavigate } from "react-router-dom";
// Images
// import logoXD from "assets/images/small-logos/logo-xd.svg";
// import logoAtlassian from "assets/images/small-logos/logo-atlassian.svg";
// import logoSlack from "assets/images/small-logos/logo-slack.svg";
// import logoSpotify from "assets/images/small-logos/logo-spotify.svg";
// import logoJira from "assets/images/small-logos/logo-jira.svg";
// import logoInvesion from "assets/images/small-logos/logo-invision.svg";
import team2 from "assets/images/team-2.jpg";
import team3 from "assets/images/team-3.jpg";
import team4 from "assets/images/team-4.jpg";

export default function data() {
  const Author = ({ image, name, email }) => (
    <MDBox display="flex" alignItems="center" lineHeight={1}>
      <MDAvatar src={image} name={name} size="sm" />
      <MDBox ml={2} lineHeight={1}>
        <MDTypography display="block" variant="button" fontWeight="medium">
          {name}
        </MDTypography>
        <MDTypography variant="caption">{email}</MDTypography>
      </MDBox>
    </MDBox>
  );
  const navigate = useNavigate();
  const routeChange = () => {
    const path = "/editadmin";
    navigate(path);
  };

  const [successSB, setSuccessSB] = useState(false);
  const [deleteSB, setDeleteSB] = useState(false);

  // const openSuccessSB = () => setSuccessSB(true);
  const openDelete = () => setDeleteSB(true);
  const closeSuccessSB = () => setSuccessSB(false);
  const closeDeleteSB = () => setDeleteSB(false);

  const renderSuccessSB = (
    <MDSnackbar
      color="warning"
      icon="check"
      title="Console Dashboard"
      content="This is a data demo can't modify"
      dateTime="11 mins ago"
      open={successSB}
      onClose={closeSuccessSB}
      close={closeSuccessSB}
      bgWhite
    />
  );

  const renderDelete = (
    <MDSnackbar
      color="error"
      icon="check"
      title="Console Dashboard"
      content="This is a data demo can't delete"
      dateTime="11 mins ago"
      open={deleteSB}
      onClose={closeDeleteSB}
      close={closeDeleteSB}
      bgWhite
    />
  );

  // const Job = ({ title, description }) => (
  //   <MDBox lineHeight={1} textAlign="left">
  //     <MDTypography display="block" variant="caption" color="text" fontWeight="medium">
  //       {title}
  //     </MDTypography>
  //     <MDTypography variant="caption">{description}</MDTypography>
  //   </MDBox>
  // );

  const dataColumns = [
    { Header: "ชื่อผู้ใช้", accessor: "author", width: "30%", align: "left" },
    { Header: "สถานะการออนไลน์", accessor: "status", align: "center" },
    { Header: "ตำแหน่ง", accessor: "rank", align: "center" },
    { Header: "การเข้าใช้งานล่าสุด", accessor: "employed", align: "center" },
    { Header: "เลือกแก่ไข", accessor: "action", align: "center" },
  ]

  const dataRows = [
    {image: team2, name: "John Michael", email: "john@creative-tim.com", status: "Online", rank: "ผู้ดูแลระบบขั้นต้น", employed: "23/04/18"},
    {image: team3, name: "Alexa Liras", email: "alexa@creative-tim.com", status: "Offline", rank: "ผู้ดูแลระบบขั้นกลาง", employed: "11/01/19"},
    {image: team4, name: "Laurent Perrier", email: "laurent@creative-tim.com", status: "Online", rank: "ผู้ดูแลระบบขั้นสูง", employed: "19/09/17"},
    {image: team2, name: "Michael Levi", email: "michael@creative-tim.com", status: "Online", rank: "ผู้ดูแลจัดการระบบ", employed: "24/12/08"},
    {image: team3, name: "Richard Gran", email: "richard@creative-tim.com", status: "Offline", rank: "ผู้ดูแลระบบขั้นพิเศษ", employed: "04/10/21"},
    {image: team4, name: "Miriam Eric", email: "miriam@creative-tim.com", status: "Offline", rank: "ผู้ดูแลระบบขั้นกลาง", employed: "14/09/20"}
  ]

  const setRowArray = []

  if (dataRows) {
    dataRows.forEach((val) => {
      let colorSt
      let colorRnk
      if (val.status === 'Online') {
        colorSt = 'success'
      }
      if (val.status === 'Offline') {
        colorSt = 'dark'
      }
      if (val.rank === 'ผู้ดูแลระบบขั้นต้น') {
        colorRnk = 'copper'
      }
      if (val.rank === 'ผู้ดูแลระบบขั้นกลาง') {
        colorRnk = 'info'
      }
      if (val.rank === 'ผู้ดูแลระบบขั้นสูง') {
        colorRnk = 'error'
      }
      if (val.rank === 'ผู้ดูแลจัดการระบบ') {
        colorRnk = 'secondary'
      }
      if (val.rank === 'ผู้ดูแลระบบขั้นพิเศษ') {
        colorRnk = 'gold'
      }
      const renderRow = {
        author: (
          <Author 
          image={val.image}
          name={val.name}
          email={val.email}
          />
        ),
        status: (
          <MDBox ml={-1}>
            <MDBadge badgeContent={val.status} color={colorSt} variant="gradient" size="sm" />
          </MDBox>
        ),
        rank: (
          <MDBox ml={-1}>
              <MDBadge badgeContent={val.rank} color={colorRnk} variant="gradient" size="sm" />
          </MDBox>
        ),
        employed: (
          <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
            {val.employed}
          </MDTypography>
        ),
        action: (
          <MDBox ml={2} pt={2}>
            <Grid container spacing={2}>
              <MDButton variant="gradient" color="warning" onClick={routeChange}>
                <Icon>edit</Icon>
                แก้ไข
              </MDButton>
              <MDButton variant="gradient" color="error" onClick={openDelete}>
                <Icon>delete</Icon>
                ลบ
              </MDButton>
              {renderSuccessSB}
              {renderDelete}
            </Grid>
          </MDBox>
        )
      }
      setRowArray.push(renderRow)
    })
  }

  return {
    columns: dataColumns,

    rows: setRowArray


    // columns: [
    //   { Header: "ชื่อผู้ใช้", accessor: "author", width: "30%", align: "left" },
    //   { Header: "สถานะการออนไลน์", accessor: "status", align: "center" },
    //   { Header: "ตำแหน่ง", accessor: "rank", align: "center" },
    //   { Header: "การเข้าใช้งานล่าสุด", accessor: "employed", align: "center" },
    //   { Header: "เลือกแก่ไข", accessor: "action", align: "center" },
    // ],

    // rows: [
    //   {
    //     author: (
    //       <Author
    //         image={team2}
    //         name="John Michael"
    //         email="john@creative-tim.com"
    //         label="project #1"
    //         title="scandinavian"
    //         action={{
    //           type: "internal",
    //           route: "/pages/profile/profile-overview",
    //           color: "info",
    //           label: "view project",
    //         }}
    //       />
    //     ),
    //     function: <Job title="Manager" description="Organization" />,
    //     status: (
    //       <MDBox ml={-1}>
    //         <MDBadge badgeContent="ออนไลน์" color="success" variant="gradient" size="sm" />
    //       </MDBox>
    //     ),
    //     employed: (
    //       <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
    //         23/04/18
    //       </MDTypography>
    //     ),
    //     rank: (
    //         <MDBox ml={-1}>
    //             <MDBadge badgeContent="ผู้ดูแลระบบขั้นต้น" color="copper" variant="gradient" size="sm" />
    //         </MDBox>
    //     ),
    //     action: (
    //       <MDBox ml={-1}>
    //         <Grid container spacing={2}>
    //           <MDButton variant="gradient" color="warning" onClick={routeChange}>
    //             <Icon>edit</Icon>
    //             แก้ไข
    //           </MDButton>
    //           <MDButton variant="gradient" color="error" onClick={openDelete}>
    //             <Icon>delete</Icon>
    //             ลบ
    //           </MDButton>
    //           {renderSuccessSB}
    //           {renderDelete}
    //         </Grid>
    //       </MDBox>
    //     ),
    //   },
    //   {
    //     author: <Author image={team3} name="Alexa Liras" email="alexa@creative-tim.com" />,
    //     function: <Job title="Programator" description="Developer" />,
    //     status: (
    //       <MDBox ml={-1}>
    //         <MDBadge badgeContent="ออฟไลน์" color="dark" variant="gradient" size="sm" />
    //       </MDBox>
    //     ),
    //     rank: (
    //         <MDBox ml={-1}>
    //             <MDBadge badgeContent="ผู้ดูแลระบบขั้นกลาง" color="copper" variant="gradient" size="sm" />
    //         </MDBox>
    //     ),
    //     employed: (
    //       <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
    //         11/01/19
    //       </MDTypography>
    //     ),
    //     action: (
    //       <MDBox ml={-1}>
    //         <Grid container spacing={2}>
    //           <MDButton variant="gradient" color="warning" onClick={routeChange}>
    //             <Icon>edit</Icon>
    //             แก้ไข
    //           </MDButton>
    //           <MDButton variant="gradient" color="error" onClick={openDelete}>
    //             <Icon>delete</Icon>
    //             ลบ
    //           </MDButton>
    //           {renderSuccessSB}
    //           {renderDelete}
    //         </Grid>
    //       </MDBox>
    //     ),
    //   },
    //   {
    //     author: <Author image={team4} name="Laurent Perrier" email="laurent@creative-tim.com" />,
    //     function: <Job title="Executive" description="Projects" />,
    //     status: (
    //       <MDBox ml={-1}>
    //         <MDBadge badgeContent="ออนไลน์" color="success" variant="gradient" size="sm" />
    //       </MDBox>
    //     ),
    //     rank: (
    //         <MDBox ml={-1}>
    //             <MDBadge badgeContent="ผู้ดูแลระบบขั้นสูง" color="error" variant="gradient" size="sm" />
    //         </MDBox>
    //     ),
    //     employed: (
    //       <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
    //         19/09/17
    //       </MDTypography>
    //     ),
    //     action: (
    //       <MDBox ml={-1}>
    //         <Grid container spacing={2}>
    //           <MDButton variant="gradient" color="warning" onClick={routeChange}>
    //             <Icon>edit</Icon>
    //             แก้ไข
    //           </MDButton>
    //           <MDButton variant="gradient" color="error" onClick={openDelete}>
    //             <Icon>delete</Icon>
    //             ลบ
    //           </MDButton>
    //           {renderSuccessSB}
    //           {renderDelete}
    //         </Grid>
    //       </MDBox>
    //     ),
    //   },
    //   {
    //     author: <Author image={team3} name="Michael Levi" email="michael@creative-tim.com" />,
    //     function: <Job title="Programator" description="Developer" />,
    //     status: (
    //       <MDBox ml={-1}>
    //         <MDBadge badgeContent="ออนไลน์" color="success" variant="gradient" size="sm" />
    //       </MDBox>
    //     ),
    //     rank: (
    //         <MDBox ml={-1}>
    //             <MDBadge badgeContent="ผู้ดูแลจัดการระบบ" color="warning" variant="gradient" size="sm" />
    //         </MDBox>
    //     ),
    //     employed: (
    //       <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
    //         24/12/08
    //       </MDTypography>
    //     ),
    //     action: (
    //       <MDBox ml={-1}>
    //         <Grid container spacing={2}>
    //           <MDButton variant="gradient" color="warning" onClick={routeChange}>
    //             <Icon>edit</Icon>
    //             แก้ไข
    //           </MDButton>
    //           <MDButton variant="gradient" color="error" onClick={openDelete}>
    //             <Icon>delete</Icon>
    //             ลบ
    //           </MDButton>
    //           {renderSuccessSB}
    //           {renderDelete}
    //         </Grid>
    //       </MDBox>
    //     ),
    //   },
    //   {
    //     author: <Author image={team3} name="Richard Gran" email="richard@creative-tim.com" />,
    //     function: <Job title="Manager" description="Executive" />,
    //     status: (
    //       <MDBox ml={-1}>
    //         <MDBadge badgeContent="ออฟไลน์" color="dark" variant="gradient" size="sm" />
    //       </MDBox>
    //     ),
    //     rank: (
    //         <MDBox ml={-1}>
    //             <MDBadge badgeContent="ผู้ดูแลระบบขั้นพิเศษ" color="gold" variant="gradient" size="sm" />
    //         </MDBox>
    //     ),
    //     employed: (
    //       <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
    //         04/10/21
    //       </MDTypography>
    //     ),
    //     action: (
    //       <MDBox ml={-1}>
    //         <Grid container spacing={2}>
    //           <MDButton variant="gradient" color="warning" onClick={routeChange}>
    //             <Icon>edit</Icon>
    //             แก้ไข
    //           </MDButton>
    //           <MDButton variant="gradient" color="error" onClick={openDelete}>
    //             <Icon>delete</Icon>
    //             ลบ
    //           </MDButton>
    //           {renderSuccessSB}
    //           {renderDelete}
    //         </Grid>
    //       </MDBox>
    //     ),
    //   },
    //   {
    //     author: <Author image={team4} name="Miriam Eric" email="miriam@creative-tim.com" />,
    //     function: <Job title="Programator" description="Developer" />,
    //     status: (
    //       <MDBox ml={-1}>
    //         <MDBadge badgeContent="ออฟไลน์" color="dark" variant="gradient" size="sm" />
    //       </MDBox>
    //     ),
    //     rank: (
    //         <MDBox ml={-1}>
    //             <MDBadge badgeContent="ผู้ดูแลระบบขั้นกลาง" color="siver" variant="gradient" size="sm" />
    //         </MDBox>
    //     ),
    //     employed: (
    //       <MDTypography component="a" href="#" variant="caption" color="text" fontWeight="medium">
    //         14/09/20
    //       </MDTypography>
    //     ),
    //     action: (
    //       <MDBox ml={-1}>
    //         <Grid container spacing={2}>
    //           <MDButton variant="gradient" color="warning" onClick={routeChange}>
    //             <Icon>edit</Icon>
    //             แก้ไข
    //           </MDButton>
    //           <MDButton variant="gradient" color="error" onClick={openDelete}>
    //             <Icon>delete</Icon>
    //             ลบ
    //           </MDButton>
    //           {renderSuccessSB}
    //           {renderDelete}
    //         </Grid>
    //       </MDBox>
    //     ),
    //   },
    // ],
  };
}
