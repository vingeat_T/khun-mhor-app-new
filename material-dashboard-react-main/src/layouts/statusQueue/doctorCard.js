import { Card, Image } from "semantic-ui-react";
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import PropTypes from "prop-types";


function DoctorCard({ image, header, meta, description }) {
  return (
    <Card display="flex">
        <MDBox m={1} >
          <Image src={image} alt="profile-image" fluid />
        </MDBox>
      <MDBox textAlign="left" lineHeight={1} px={2}>
        <MDTypography variant="h5">
          {header}
        </MDTypography>
      </MDBox>
      <MDBox textAlign="left" lineHeight={1} px={2}>
        <MDTypography variant="h6" color="text" fontWeight="regular">
          {meta}
        </MDTypography>
      </MDBox>
      <MDBox textAlign="left" lineHeight={1} px={2}>
        <MDTypography variant="h6" color="text" fontWeight="regular">
          {description}
        </MDTypography>
        &nbsp;
      </MDBox>
    </Card>
  );
}
DoctorCard.defaultProps={};
DoctorCard.propTypes={
    image: PropTypes.string.isRequired,
    header: PropTypes.string.isRequired,
    meta: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired
};
export default DoctorCard;


  /* image={doc4}
                  header="นพ.เซียว จ้าน"
                  meta="แผนก: ศัลยกรรมทรวงอก"
                  description="สถานพยาบาล: โรงพยาบาลขอนแก่น" */

