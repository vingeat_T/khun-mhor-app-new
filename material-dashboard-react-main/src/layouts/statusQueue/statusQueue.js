import * as React from "react";

import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import DropdownExampleSearchSelection from "layouts/dropdown/dropDown";
// import ComplexStatisticsCard from "layouts/ComplexStatisticsCard/index";

import { Dropdown, Menu } from "semantic-ui-react";

import Grid from "@mui/material/Grid";
import MDBox from "components/MDBox";
// import MDTypography from "components/MDTypography";

// import doc4 from "assets/images/doc4.jpg";

// import user3 from "assets/images/user3.jpg";
// import user4 from "assets/images/user4.jpg";
// import user5 from "assets/images/user5.jpg";
// import user6 from "assets/images/user6.jpg";

import Header from "layouts/doctorHeader/doctorHeader";
// import DoctorCard from "layouts/statusQueue/doctorCard"
// import PatienCard from "layouts/statusQueue/components/patienCard"
import DrForm from "layouts/statusQueue/doctorForm/drForm"

// import Statusswitch from "layouts/statusQueue/switch";
// import Queses from "layouts/statusQueue/queues";
// import ChairOutlinedIcon from "@mui/icons-material/ChairOutlined";
import Footer from 'examples/Footer'

function statusQueue() {

  // const [queue, setQueue] = React.useState(6)

  function redirect(value) {
    if (value) {
      window.location.href = value;
    }
  }

  return (
    <DashboardLayout>
      <DashboardNavbar />
      <Menu vertical>
        <Dropdown item text="ถามหมอ">
          <Dropdown.Menu>
            <Dropdown.Item onClick={() => redirect("freeAsk")}>ถามฟรี</Dropdown.Item>
            <Dropdown.Item onClick={() => redirect("waitAsk")}>ถามรอ</Dropdown.Item>
            <Dropdown.Item onClick={() => redirect("nowAsk")}>ถามทันที</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </Menu>
      <DropdownExampleSearchSelection />
      &nbsp;
      <Header>
        <MDBox mb={2} />
        <MDBox p={5}>
          <Grid container spacing={2}>
            <Grid item xl={8} xs={6}>
              <DrForm />
            </Grid>

            {/* <PatienCard /> */}

            {/* <Grid item xs={8} align="center">
              <PatienCard />
            </Grid> */}
            

            {/* <Grid item xs={6} md={6} xl={8}>
            <MDBox mb={1.5} xs={1.5}>
              <Card style={{ width: "100%", height: "100%" }}>
                <MDBox py={2}>
                  <Grid container spacing={2} >
                    <Grid item xs={6} md={6} >
                      <MDBox mb={1.5} xs={1.5}>
                        <ComplexStatisticsCard
                          detail1="18/2/2565"
                          detail2="ลำดับที่ 1"
                          color="dark"
                          image={user3}
                          iconx="cancel"
                          title="สโนว์"
                          count="ปวดเท้า"
                          header="รายละเอียดอาการ"
                          content="ปวดเท้า ชอบปวดเท้าเป็นประจำ เท้าเป็นหนึ่งในอวัยวะที่มีความซับซ้อนที่สุดของร่างกายประกอบด้วยกระดูก 26 ชิ้น เชื่อมต่อกันด้วยข้อต่อ กล้ามเนื้อ เอ็นกล้ามเนื้อและเอ็นยึด"
                        />
                        
                      </MDBox>
                    </Grid>
                    <Grid item xs={6} md={6}>
                      <MDBox mb={1.5} xs={1.5}>
                        <ComplexStatisticsCard
                          detail1="17/2/2565"
                          detail2="ลำดับที่ 2"
                          color="dark"
                          image={user4}
                          title="Apatsara"
                          count="มีประจำมานาน"
                          header="รายละเอียดอาการ"
                          content="ประจำเดือนมานานหลายวันมา ประมาณ2 อาทิตย์ได้ มาทีก็มาเยอะเปลื่ยนผ้าอนามัยจนเหนื่อย รู้สึกเพลียมากไม่มีแรง แล้วก็ปวดท้องมาก แบบนี้จะเเป็นอะไรมั๊ยคะ"
                        />
                      </MDBox>
                    </Grid>
                    <Grid item xs={6} md={6}>
                      <MDBox mb={1.5} xs={1.5}>
                        <ComplexStatisticsCard
                          detail1="16/2/2565"
                          detail2="ลำดับที่ 3"
                          color="dark"
                          image={user5}
                          title="Anna"
                          count="ปวดหัว"
                          header="รายละเอียดอาการ"
                          content="ปวดหัวปวดบ่อยอาทิตย์นึงปวดหลายรอบมาก เมื่อเย็นของวันนี้รู้สึกตัวร้อน หายใจไม่ทั่วท้อง ไม่แน่ใจว่าเพราะปวดหัวรึป่าว บางครั้งปวดแล้วก็อ้วก พออ้วกแล้วจะดีขึ้น"
                        />
                      </MDBox>
                    </Grid>
                    <Grid item xs={6} md={6}>
                      <MDBox mb={1.5} xs={1.5}>
                        <ComplexStatisticsCard
                          detail1="17/2/2565"
                          detail2="ลำดับที่ 2"
                          color="dark"
                          image={user6}
                          title="Apatsara"
                          count="มีประจำเดือนนาน"
                          header="รายละเอียดอาการ"
                          content="ประจำเดือนมานานหลายวันมา ประมาณ2 อาทิตย์ได้ มาทีก็มาเยอะเปลื่ยนผ้าอนามัยจนเหนื่อย รู้สึกเพลียมากไม่มีแรง แล้วก็ปวดท้องมาก แบบนี้จะเเป็นอะไรมั๊ยคะ"
                        />
                      </MDBox>
                    </Grid>
                  </Grid>
                </MDBox>
              </Card>
            </MDBox>
            </Grid> */}
          </Grid>
          
        </MDBox>
        
      </Header>
      <Footer />
    </DashboardLayout>
  );
}
export default statusQueue;
