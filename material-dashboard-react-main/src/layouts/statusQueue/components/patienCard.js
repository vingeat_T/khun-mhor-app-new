import React from 'react'
import ComplexStatisticsCard from "layouts/ComplexStatisticsCard/index";

import Grid from "@mui/material/Grid";
// import Card from "@mui/material/Card";
import MDBox from "components/MDBox";

import user3 from "assets/images/user3.jpg";
import user4 from "assets/images/user4.jpg";
import user5 from "assets/images/user5.jpg";
import user6 from "assets/images/user6.jpg";

function PatienCard() {
  return (
    <MDBox py={3}>
        <Grid container spacing={3} display="flex" justifyContent="space-between"  pt={2} px={5}>
          <Grid item xs={12} md={6} lg={4}>
            <MDBox mb={1.5}>
              <ComplexStatisticsCard
                detail1="18/2/2565"
                detail2="ลำดับที่ 1"
                color="dark"
                image={user3}
                iconx="cancel"
                title="สโนว์"
                count="ปวดเท้า"
                header="รายละเอียดอาการ"
                content="ปวดมากๆปวดจี๊ดๆ"
              />
            </MDBox>
          </Grid>
          <Grid item xs={12} md={6} lg={4}>
            <MDBox mb={1.5}>
            <ComplexStatisticsCard
                        detail1="17/2/2565"
                        detail2="ลำดับที่ 2"
                        color="dark"
                        image={user4}
                        title="Apatsara"
                        count="มีประจำมานาน"
                        header="รายละเอียดอาการ"
                        content="ประจำเดือนมานานหลายวันมา ประมาณ2 อาทิตย์ได้ มาทีก็มาเยอะเปลื่ยนผ้าอนามัยจนเหนื่อย รู้สึกเพลียมากไม่มีแรง แล้วก็ปวดท้องมาก แบบนี้จะเเป็นอะไรมั๊ยคะ"
                    />
            </MDBox>
          </Grid>
          <Grid item xs={12} md={6} lg={4}>
            <MDBox mb={1.5}>
            <ComplexStatisticsCard
                        detail1="16/2/2565"
                        detail2="ลำดับที่ 3"
                        color="dark"
                        image={user5}
                        title="Anna"
                        count="ปวดหัว"
                        header="รายละเอียดอาการ"
                        content="ปวดหัวปวดบ่อยอาทิตย์นึงปวดหลายรอบมาก เมื่อเย็นของวันนี้รู้สึกตัวร้อน หายใจไม่ทั่วท้อง ไม่แน่ใจว่าเพราะปวดหัวรึป่าว บางครั้งปวดแล้วก็อ้วก พออ้วกแล้วจะดีขึ้น"
                    />
            </MDBox>
          </Grid>
          <Grid item xs={12} md={6} lg={4}>
            <MDBox mb={1.5}>
            <ComplexStatisticsCard
                        detail1="17/2/2565"
                        detail2="ลำดับที่ 2"
                        color="dark"
                        image={user6}
                        title="Apatsara"
                        count="มีประจำเดือนนาน"
                        header="รายละเอียดอาการ"
                        content="ประจำเดือนมานานหลายวันมา ประมาณ2 อาทิตย์ได้ มาทีก็มาเยอะเปลื่ยนผ้าอนามัยจนเหนื่อย รู้สึกเพลียมากไม่มีแรง แล้วก็ปวดท้องมาก แบบนี้จะเเป็นอะไรมั๊ยคะ"
                    />
            </MDBox>
          </Grid>

                    
              </Grid>
              </MDBox>
  )
}

export default PatienCard