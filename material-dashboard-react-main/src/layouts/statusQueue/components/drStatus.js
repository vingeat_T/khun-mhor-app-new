import MDTypography from 'components/MDTypography'
import React from 'react'
import PropTypes from "prop-types";

function DrStatus({ val }) {

    const busy = <MDTypography variant="span" color="error" fontWeight="bold">กำลังให้คำปรึกษา</MDTypography>
    const idle = <MDTypography variant="span" color="success" fontWeight="bold">ว่าง</MDTypography>

  return (
    <>
        { val ? busy : idle}
    </>
  )
}

// Setting default props for the DrStatus
DrStatus.defaultProps = {
    val: 0,
  };
  
  // Typechecking props for the DrStatus
  DrStatus.propTypes = {
    val: PropTypes.node,
  };

export default DrStatus