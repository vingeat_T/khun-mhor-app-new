import React from 'react'
import Grid from "@mui/material/Grid";
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";

import doc4 from "assets/images/doc4.jpg";
import DoctorCard from "layouts/statusQueue/doctorCard"

// import Statusswitch from "layouts/statusQueue/switch";

// import ChairOutlinedIcon from "@mui/icons-material/ChairOutlined";

function DrForm() {
  return (
    <Grid>
        <Grid item xs={6} md={4} align="center">
              <MDBox mb={1.5} xs={1.5} p={3} mt={-3}>
                <DoctorCard
                  image={doc4}
                  header="นพ.เซียว จ้าน"
                  meta="ความชำนาญ: ศัลยกรรมทรวงอก"
                  description="สถานพยาบาล: โรงพยาบาลขอนแก่น"
                />
                </MDBox>
                <MDBox mt={-3} px={0} align="center">
                  <MDTypography variant="span" color="text">ราคาในการให้คำปรึกษา : </MDTypography>
                  <MDTypography variant="span" color="dark" fontWeight="bold">200 ฿ </MDTypography>
                </MDBox>
              </Grid>
              
            {/* <Grid item xs={6} md={4} align="center">
              <MDBox align="center">
                <ChairOutlinedIcon fontSize="large" />
                <MDTypography variant="h3" fontWeight="medium" textTransform="capitalize">
                  รออยู่ 6 คิว
                </MDTypography>
              </MDBox>
            &nbsp;

              <MDBox align="center">
                <Statusswitch />
              </MDBox>
            &nbsp;

            </Grid> */}
    </Grid>
  )
}

export default DrForm