
import PropTypes from "prop-types";


import Card from "@mui/material/Card";
import Divider from "@mui/material/Divider";
import Icon from "@mui/material/Icon";

import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
import ChairOutlinedIcon from '@mui/icons-material/ChairOutlined';


function Queses({  icon, title,title2,title3, description, value }) {
  return (
    <Card style={{ width: "100%", height: "100%" }}>
      <MDBox p={2} mx={3} display="flex" justifyContent="center">
        <MDBox>
          <Icon fontSize="default">{icon}</Icon>
        </MDBox>
      </MDBox>
      <MDBox pb={2} px={5} mt={-6} textAlign="center" lineHeight={1.25} pt={12}>
      <ChairOutlinedIcon fontSize="large"/>
      </MDBox>
      <MDBox pb={2} px={2} textAlign="center" lineHeight={1.25}>
        <MDTypography variant="h3" fontWeight="medium" textTransform="capitalize">
          {title}
        </MDTypography>
        <MDTypography variant="h1" fontWeight="medium" textTransform="capitalize">
          {title2}
        </MDTypography>
        <MDTypography variant="h3" fontWeight="medium" textTransform="capitalize">
          {title3}
        </MDTypography>
        {description && (
          <MDTypography variant="caption" color="text" fontWeight="regular">
            {description}
          </MDTypography>
        )}
        {description && !value ? null : <Divider />}
        {value && (
          <MDTypography variant="h5" fontWeight="medium">
            {value}
          </MDTypography>
        )}
      </MDBox>
    </Card>
  );
}

// Setting default values for the props of DefaultInfoCard
Queses.defaultProps = {
//   color: "info",
  value: "",
  description: "",
};

// Typechecking props for the DefaultInfoCard
Queses.propTypes = {
//   color: PropTypes.oneOf(["primary", "secondary", "info", "success", "warning", "error", "dark"]),
  icon: PropTypes.node.isRequired,
  title: PropTypes.string.isRequired,
  title2: PropTypes.string.isRequired,
  title3: PropTypes.string.isRequired,
  description: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

export default Queses;
