import React from 'react'
import { Dropdown } from 'semantic-ui-react'

const diseaseOptions = [
  { key: 'af', value: 'af', text: 'โรคหู' },
  { key: 'ax', value: 'ax', text: 'โรคตา' },
  { key: 'al', value: 'al', text: 'โรคสมอง' },
  { key: 'dz', value: 'dz', text: 'โรคทางเดินหายใจ'},
  { key: 'dx', value: 'dx', text: 'โรคแขน'},
  { key: 'dy', value: 'dy', text: 'โรคขา'},
  { key: 'da', value: 'da', text: 'โรคกระดูก'},
]

const DropdownExampleSearchSelection = () => (
  <Dropdown
    placeholder='เลือกโรค'
    fluid
    multiple
    search
    selection
    options={diseaseOptions}
  />
)

export default DropdownExampleSearchSelection