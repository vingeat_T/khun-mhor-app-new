/**
=========================================================
* Material Dashboard 2 React - v2.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

// react-router-dom components
// import { Link } from "react-router-dom";

// prop-types is a library for typechecking of props
import PropTypes from "prop-types";

// @mui material components
import Card from "@mui/material/Card";
import Icon from "@mui/material/Icon";
import Grid from "@mui/material/Grid";
import LocationOnOutlinedIcon from '@mui/icons-material/LocationOnOutlined';
import SchoolOutlinedIcon from '@mui/icons-material/SchoolOutlined';

// Material Dashboard 2 React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
// import MDButton from "components/MDButton";
import MDAvatar from "components/MDAvatar";
import MDReview from "components/MDReview";
import MDHeart from "components/MDHeart";

function ComplexStatisticsCard({ name, title, hospital, icon, price, star, heart, image, university }) {
 return (
    <Card sx={{width:350, height: 240}}>
      <MDBox display="flex" justifyContent="space-between" pt={3} px={3}>
        <MDBox>
          <Grid item>
            <MDAvatar  src={image} alt="profile-image" sx={{ width: 50, height: 50 }} shadow="sm" />
          </Grid>
          <Icon fontSize="medium" color="inherit">
            {icon}
          </Icon>
        </MDBox>
      </MDBox>
      <MDBox textAlign="right" pb={0} px={3} mt={-8}>
        <MDTypography variant="h5" fontWeight="regular" color="text" textTransform="capitalize">{name}</MDTypography>
        <MDBox textAlign="right" pb={0} px={0} mt={0}>
          <MDTypography variant="h5" fontWeight="regular" color="text">
            {title}
          </MDTypography>
        </MDBox>
        <MDBox textAlign="right" pb={0} px={0} mt={0}>
        <MDTypography variant="h5" fontWeight="regular" color="text" textTransform="capitalize">
        <SchoolOutlinedIcon />
          {university}
          </MDTypography>
        </MDBox>
        <MDBox textAlign="right" pb={0} px={0} mt={0}>
          <MDTypography variant="h5" fontWeight="regular" color="text">
          <LocationOnOutlinedIcon />
            {hospital}
          </MDTypography>
        </MDBox>
        <MDBox textAlign="right" pb={0} px={0} mt={0}>
          <MDTypography variant="h5" fontWeight="regular" color="text">
            {price}
          </MDTypography>
        </MDBox>
        <MDBox textAlign="left" pb={2} px={0} mt={1}>
          <MDTypography variant="h5" fontWeight="regular" color="text">
            {star}
          </MDTypography>
        </MDBox>
        <MDBox textAlign="left" pb={2} px={5} mt={-5}>
          <MDReview/>
        </MDBox>
        <MDBox textAlign="left" pb={2} px={0} mt={-2}>
          <MDTypography variant="h5" fontWeight="regular" color="text">
            {heart}
          </MDTypography>
        </MDBox>
        <MDBox textAlign="left" pb={2} px={5} mt={-5}>
          <MDHeart/>
        </MDBox>
      </MDBox>
    </Card>
  );
}

// Setting default values for the props of DefaultProjectCard
ComplexStatisticsCard.defaultProps = {

};

// Typechecking props for the DefaultProjectCard
ComplexStatisticsCard.propTypes = {
  image: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  university: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  hospital: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  star: PropTypes.string.isRequired,
  heart: PropTypes.string.isRequired,
  action: PropTypes.shape({
    type: PropTypes.oneOf(["external", "internal"]),
    route: PropTypes.string.isRequired,
    color: PropTypes.oneOf([
      "primary",
      "secondary",
      "info",
      "success",
      "warning",
      "error",
      "light",
      "dark",
      "white",
    ]).isRequired,
    label: PropTypes.string.isRequired,
  }).isRequired,
  icon: PropTypes.node.isRequired,

};

export default ComplexStatisticsCard;
