/**
=========================================================
* Material Dashboard 2 React - v2.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/
import * as React from "react";
// react-router-dom components
import { useNavigate } from "react-router-dom";

// prop-types is a library for typechecking of props
import PropTypes from "prop-types";

// @mui material components
import Card from "@mui/material/Card";
// import Icon from "@mui/material/Icon";
import Grid from "@mui/material/Grid";
import LocationOnOutlinedIcon from '@mui/icons-material/LocationOnOutlined';
import SchoolOutlinedIcon from '@mui/icons-material/SchoolOutlined';

// Material Dashboard 2 React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
// import MDButton from "components/MDButton";
import MDAvatar from "components/MDAvatar";
import MDReview from "components/MDReview";
import MDHeart from "components/MDHeart";

function ComplexStatistics({ name, title, hospital, price, review, image, university, heart }) {
  const [ask, setask] = React.useState("");
  const navigate = useNavigate()
  const handleChange = (event) => {
    setask(event.target.value);
   let path = "/rating";
   if (event.target.value === 1) {
    path = "/";
  } else if (event.target.value === 2) {
    path = "/";
  } else if (event.target.value === 3) {
    path = "/";
  }
   navigate(path);
 };

 return (
    <Card sx={{width:338, height: 220}} value={ask} onClick={handleChange}>
      <MDBox display="flex" justifyContent="space-between" pt={3} px={3}>
        <MDBox>
          <Grid item>
            <MDAvatar  src={image} alt="profile-image" sx={{ width: 50, height: 50 }}  shadow="sm" />
          </Grid>
        </MDBox>
      </MDBox>
      <MDBox position="relative" width="100.25%" shadow="xl" borderRadius="xl">
        <Card
          title={title}
          sx={{
            maxWidth: "100%",
            margin: 0,
            boxShadow: ({ boxShadows: { md } }) => md,
            objectFit: "cover",
            objectPosition: "center",
          }}
        />
      </MDBox>
      <MDBox textAlign="right" pb={0} px={3} mt={-6}>
      <MDTypography variant="h5" fontWeight="regular" color="text" textTransform="capitalize">{name}</MDTypography>
      <MDBox textAlign="right" pb={0} px={0} mt={0}>
        <MDTypography variant="h5" fontWeight="regular" color="text" textTransform="capitalize">{title}</MDTypography>
        </MDBox>
        <MDBox textAlign="right" pb={0} px={0} mt={0}>
        <MDTypography variant="h5" fontWeight="regular" color="text" textTransform="capitalize">
        <SchoolOutlinedIcon />
          {university}
          </MDTypography>
        </MDBox>
        <MDBox textAlign="right" pb={0} px={0} mt={0}>
          <MDTypography variant="h5" fontWeight="regular" color="text">
          <LocationOnOutlinedIcon />
            {hospital}
          </MDTypography>
        </MDBox>
        <MDBox textAlign="right" pb={0} px={0} mt={0}>
          <MDTypography variant="h5" fontWeight="regular" color="text">
            {price}
          </MDTypography>
        </MDBox>
        <MDBox textAlign="left" pb={2} px={0} mt={0}>
          <MDTypography variant="h5" fontWeight="regular" color="text">
            {review}
          </MDTypography>
        </MDBox>
        <MDBox textAlign="left" pb={2} px={5} mt={-5}>
          <MDReview/>
        </MDBox>
        <MDBox textAlign="left" pb={2} px={0} mt={-2}>
          <MDTypography variant="h5" fontWeight="regular" color="text">
            {heart}
          </MDTypography>
        </MDBox>
        <MDBox textAlign="left" pb={2} px={5} mt={-5}>
          <MDHeart/>
        </MDBox>
      </MDBox>
    </Card>
  );
}

// Setting default values for the props of DefaultProjectCard
ComplexStatistics.defaultProps = {

};

// Typechecking props for the DefaultProjectCard
ComplexStatistics.propTypes = {
  image: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  university: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  hospital: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  review: PropTypes.string.isRequired,
  heart: PropTypes.string.isRequired,
  action: PropTypes.shape({
    type: PropTypes.oneOf(["external", "internal"]),
    route: PropTypes.string.isRequired,
    color: PropTypes.oneOf([
      "primary",
      "secondary",
      "info",
      "success",
      "warning",
      "error",
      "light",
      "dark",
      "white",
    ]).isRequired,
    label: PropTypes.string.isRequired,
  }).isRequired,
  // icon: PropTypes.node.isRequired,
};

export default ComplexStatistics;
