/**
=========================================================
* Material Dashboard 2 React - v2.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

// react-router-dom components
// import { Link } from "react-router-dom";

// prop-types is a library for typechecking of props
import PropTypes from "prop-types";

// @mui material components
import Card from "@mui/material/Card";
// import Icon from "@mui/material/Icon";
import Grid from "@mui/material/Grid";

// Material Dashboard 2 React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";
// import MDButton from "components/MDButton";
import MDAvatar from "components/MDAvatar";
import MDRating from "components/MDRating";
import MDHearting from "components/MDHearting";

function Datareport({ name, title, comment, date, image }) {
 return (
    <Card sx={{width:"100%"}}>
      <MDBox display="flex" justifyContent="space-between" pt={3} px={3}>
        <MDBox>
          <Grid item>
            <MDAvatar src={image} alt="profile-image" sx={{ width: 80, height: 80 }} shadow="sm" />
          </Grid>
      </MDBox>
        </MDBox>
        <MDBox textAlign="left" pb={2} px={6} mt={2}>
            <MDTypography variant="h5" fontWeight="regular" color="text" textTransform="capitalize">
                {name}
            </MDTypography>  
        </MDBox>
      <MDBox position="relative" width="100.25%" shadow="xl" borderRadius="xl">
        <Card
          title={title}
          sx={{
            maxWidth: "100%",
            margin: 0,
            boxShadow: ({ boxShadows: { md } }) => md,
            objectFit: "cover",
            objectPosition: "center",
          }}
        />
      </MDBox>
      <MDBox textAlign="right" pb={2} px={4} mt={-15}>
        <MDRating/>
      </MDBox>
      <MDBox textAlign="right" pb={2} px={4} mt={-3}>
        <MDHearting/>
      </MDBox>
      <MDBox textAlign="right" pb={2} px={4} mt={-1}>
        <MDBox textAlign="right" pb={0} px={0} mt={-1}>
          <MDTypography variant="h4" fontWeight="regular" color="text">
            {comment}
          </MDTypography>
        </MDBox>
        <MDBox textAlign="right" pb={0} px={0} mt={0}>
          <MDTypography variant="h4" fontWeight="regular" color="text">
            {date}
          </MDTypography>
        </MDBox>
        &nbsp;
      </MDBox>
    </Card>
  );
}

// Setting default values for the props of DefaultProjectCard
Datareport.defaultProps = {

};

// Typechecking props for the DefaultProjectCard
Datareport.propTypes = {
  image: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  comment: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  action: PropTypes.shape({
    type: PropTypes.oneOf(["external", "internal"]),
    route: PropTypes.string.isRequired,
    color: PropTypes.oneOf([
      "primary",
      "secondary",
      "info",
      "success",
      "warning",
      "error",
      "light",
      "dark",
      "white",
    ]).isRequired,
    label: PropTypes.string.isRequired,
  }).isRequired,
  // icon: PropTypes.node.isRequired,

};

export default Datareport;
