import * as React from 'react';
import Box from '@mui/material/Box';
import Rating from '@mui/material/Rating';


export default function BasicRating() {
  const [value, setValue] = React.useState(1);

  return (
    <Box
      sx={{
        '& > legend': { mt: 1 },
      }}
    >
      <Rating
        size="large"
        name="simple-controlled"
        max={1}
        value={value}
        onChange={(event, newValue) => {
          setValue(newValue);
        }}
      />
    </Box>
  );
}
