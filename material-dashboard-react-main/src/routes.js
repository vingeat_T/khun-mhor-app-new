/**
=========================================================
* Material Dashboard 2 React - v2.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

/** 
  All of the routes for the Soft UI Dashboard React are added here,
  You can add a new route, customize the routes and delete the routes here.

  Once you add a new route on this file it will be visible automatically on
  the Sidenav.

  For adding a new route you can follow the existing routes in the routes array.
  1. The `type` key with the `collapse` value is used for a route.
  2. The `type` key with the `title` value is used for a title inside the Sidenav. 
  3. The `type` key with the `divider` value is used for a divider between Sidenav items.
  4. The `name` key is used for the name of the route on the Sidenav.
  5. The `key` key is used for the key of the route (It will help you with the key prop inside a loop).
  6. The `icon` key is used for the icon of the route on the Sidenav, you have to add a node.
  7. The `collapse` key is used for making a collapsible item on the Sidenav that has other routes
  inside (nested routes), you need to pass the nested routes inside an array as a value for the `collapse` key.
  8. The `route` key is used to store the route location which is used for the react router.
  9. The `href` key is used to store the external links location.
  10. The `title` key is only for the item with the type of `title` and its used for the title text on the Sidenav.
  10. The `component` key is used to store the component of its route.
*/

// Material Dashboard 2 React layouts
import Dashboard from "layouts/dashboard";
import Tables from "layouts/tables";
// import Billing from "layouts/billing";
// import RTL from "layouts/rtl";
// import Notifications from "layouts/notifications";
// import Profile from "layouts/profile";
import SignIn from "layouts/authentication/sign-in";
import SignUp from "layouts/authentication/sign-up";
import UserTest from "layouts/usertest";
import EditData from "layouts/tables/editdata";
// import Test from "layouts/tables/data/test";
import Banner from "layouts/banner";
import Mangeadmind from "layouts/mangeadmind";
import Editadmin from "layouts/mangeadmind/editadmin"
import WaitAsks from "layouts/waitAsk/waitAsk";
import NowAsk from "layouts/nowAsk/nowAsk";
import FreeAsk from "layouts/freeAsk/freeAsk";
import StatusQueue from "layouts/statusQueue/statusQueue";
// import Notifications from "layouts/notifications";
// import Rating  from "layouts/rating";
import ManageMoney  from "layouts/managemoney";
import NotificationMessage from "layouts/notificationMessage/notiMassage";
import Admin from "layouts/admin";
import AdminEdit from "layouts/admin/editDataAdmin"
import Dashboard2 from "layouts/dashboard2";
import Coin from "layouts/coin";
import CoinEdit from "layouts/coin/coinEdit";
import DocumentEdit from "layouts/documentreport/createdoc";
import DocumentCreate from "layouts/documentreport";
import History from "layouts/historyTransaction";
import HistoryEdit from "layouts/historyTransaction/data/createDataInRow";
import TransetionCoin from "layouts/transetionCoin";
import SearchTable from "layouts/transetionCoin/edit"
// import CorpsTest from 'layouts/corpstest'


// @mui icons
import Icon from "@mui/material/Icon";

const routes = [
  // {
  //   type: "collapse",
  //   name: "corpstest",
  //   key: "corpstest",
  //   icon: <Icon fontSize="small">dashboard</Icon>,
  //   route: "/corpstest",
  //   component: <CorpsTest />,
  // },
  {
    type: "collapse",
    name: "Dashboard",
    key: "dashboard2",
    icon: <Icon fontSize="small">dashboard</Icon>,
    route: "/dashboard2",
    component: <Dashboard2 />,
  },
  {
    type: "collapse",
    name: "History",
    key: "history",
    icon: <Icon fontSize="small">dashboard</Icon>,
    route: "/history",
    component: <History />,
  },
  {
    type: "subcollapse",
    name: "คุณหมอ",
    key: "testmenu",
    icon: <Icon fontSize="small">person</Icon>,
    collapse: [
      {
        name: "จัดการหมอ",
        key: "usertest",
        icon: <Icon fontSize="small">assignment</Icon>,
        route: "/usertest",
        component: <UserTest />,
      },
      {
        name: "จัดการรีวิว",
        key: "dashboard",
        icon: <Icon fontSize="small">dashboard</Icon>,
        route: "/dashboard",
        component: <Dashboard />,
      },
      {
        name: "จัดการรายได้ของหมอ",
        key: "ManageMoney",
        icon: <Icon fontSize="small">managemoney</Icon>,
        route: "/managemoney",
        component: <ManageMoney />,
      }
    ]
  }, 
  {
    type: "subcollapse",
    name: "แอดมิน",
    key: "admindhead",
    icon: <Icon fontSize="small">person</Icon>,
    collapse: [
      {
        name: "จัดการผู้ดูแลระบบ",
        key: "admin",
        icon: <Icon fontSize="small">notifications</Icon>,
        route: "/admin",
        component: <Admin />,
      },
      {
        key: "mangeadmind",
        name: "จัดการสิทธิ",
        icon: <Icon fontSize="small">person</Icon>,
        route: "/mangeadmind",
        component: <Mangeadmind />,
      } 
    ]
  },
  {
    type: "subcollapse",
    name: "ผู้ใช้ทั่วไป",
    key: "admindhead",
    icon: <Icon fontSize="small">person</Icon>,
    collapse: [
      {
        name: "จัดการผู้ใช้ทั่วไป",
        key: "tables",
        icon: <Icon fontSize="small">table_view</Icon>,
        route: "/tables",
        component: <Tables />,
      }
    ]
  },
  {
    type: "subcollapse",
    name: "จัดการระบบทั่วไป",
    key: "ิbasicsystem",
    icon: <Icon fontSize="small">person</Icon>,
    collapse: [
      {
        name: "Banner",
        key: "banner",
        icon: <Icon fontSize="small">insert_photo</Icon>,
        route: "/banner",
        component: <Banner />,
      },
      {
        name: "จัดการคิว",
        key: "FreeAsk",
        icon: <Icon fontSize="small">receipt_long</Icon>,
        route: "/freeAsk",
        component: <FreeAsk />,
      },
      {
        name: "จัดการการแจ้งเตือน",
        key: "NotificationMessage",
        icon: <Icon fontSize="small">notifications</Icon>,
        route: "/notificationMessage",
        component: <NotificationMessage />,
      },
    ]
  },
  // {
  //   type: "collapse",
  //   name: "จัดการผู้ใช้ทั่วไป",
  //   key: "tables",
  //   icon: <Icon fontSize="small">table_view</Icon>,
  //   route: "/tables",
  //   component: <Tables />,
  // },
  // {
  //   type: "collapse",
  //   name: "จัดการหมอ",
  //   key: "usertest",
  //   icon: <Icon fontSize="small">assignment</Icon>,
  //   route: "/usertest",
  //   component: <UserTest />,
  // },
  // {
  //   type: "collapse",
  //   key: "mangeadmind",
  //   name: "จัดการสิทธิ",
  //   icon: <Icon fontSize="small">person</Icon>,
  //   route: "/mangeadmind",
  //   component: <Mangeadmind />,
  // },
  // {
  //   type: "collapse",
  //   name: "รีวิวคุณหมอ",
  //   key: "Rating",
  //   icon: <Icon fontSize="small">star</Icon>,
  //   route: "/rating",
  //   component: <Rating />,
  // },
  // {
  //   type: "collapse",
  //   name: "จัดการรายได้ของหมอ",
  //   key: "ManageMoney",
  //   icon: <Icon fontSize="small">managemoney</Icon>,
  //   route: "/managemoney",
  //   component: <ManageMoney />,
  // },
  // {
  //   type: "collapse",
  //   name: "Banner",
  //   key: "banner",
  //   icon: <Icon fontSize="small">insert_photo</Icon>,
  //   route: "/banner",
  //   component: <Banner />,
  // },
    
  // {
  //   type: "collapse",
  //   name: "จัดการคิว",
  //   key: "FreeAsk",
  //   icon: <Icon fontSize="small">receipt_long</Icon>,
  //   route: "/freeAsk",
  //   component: <FreeAsk />,
  // },
  {
    name: "ถามรอ",
    key: "waitAsk",
    icon: <Icon fontSize="small">receipt_long</Icon>,
    route: "/waitAsk",
    component: <WaitAsks />,
  },
  {
    name: "ถามทันที",
    key: "NowAsk",
    icon: <Icon fontSize="small">receipt_long</Icon>,
    route: "/nowAsk",
    component: <NowAsk />,
  },
  {
    name: "จัดการคิว",
    key: "SatusQueue",
    icon: <Icon fontSize="small">receipt_long</Icon>,
    route: "/statusQueue",
    component: <StatusQueue />,
  },
  // {
  //   type: "collapse",
  //   name: "จัดการการแจ้งเตือน",
  //   key: "NotificationMessage",
  //   icon: <Icon fontSize="small">notifications</Icon>,
  //   route: "/notificationMessage",
  //   component: <NotificationMessage />,
  // },
  // {
  //   type: "collapse",
  //   name: "จัดการผู้ดูแลระบบ",
  //   key: "admin",
  //   icon: <Icon fontSize="small">notifications</Icon>,
  //   route: "/admin",
  //   component: <Admin />,
  // },
  {
    type: "collapse",
    name: "CoinHistory",
    key: "coinhistory",
    icon: <Icon fontSize="small">$</Icon>,
    route: "/transetioncoin",
    component: <TransetionCoin />,
  },
  {
    type: "collapse",
    name: "Coin",
    key: "coin",
    icon: <Icon fontSize="small">$</Icon>,
    route: "/coin",
    component: <Coin />,
  },
  {
    type: "collapse",
    name: "จัดการเอกสาร",
    key: "documentcreate",
    icon: <Icon fontSize="small">dashboard</Icon>,
    route: "/documentcreate",
    component: <DocumentCreate />,
  },
  {
    type: "collapse",
    name: "Sign In",
    key: "sign-in",
    icon: <Icon fontSize="small">login</Icon>,
    route: "/authentication/sign-in",
    component: <SignIn />,
  },
  {
    type: "collapse",
    name: "Sign Up",
    key: "sign-up",
    icon: <Icon fontSize="small">assignment</Icon>,
    route: "/authentication/sign-up",
    component: <SignUp />,
  },     
  {
    key: "editdata",
    route: "/tables/editdata",
    component: <EditData />,
  },
  {
    key: "add",
    route: "/tables/editdata/create",
    component: <EditData />,
  },
  {
    key: "addadmin",
    route: "/mangeadmin/editdata/create",
    component: <Editadmin />,
  },
  {
    key: "editadmin",
    route: "/editadmin",
    component: <AdminEdit />,
  },
  {
    key: "coin",
    route: "/editcoin",
    component: <CoinEdit />,
  },
  {
    key: "editdatatransation",
    route: "/editdatatransation",
    component: <HistoryEdit />,
  },
  {
    key: "document",
    route: "/document",
    component: <DocumentEdit />,
  },
  {
    key: "search",
    route: "/search",
    component: <SearchTable />,
  },
 
];

export default routes;
